import { createSlice } from '@reduxjs/toolkit';
import Toast from 'react-native-root-toast';
// import R from 'reactotron-react-native';
import moment from 'moment';
import FontType from '@theme/fonts';
import { getPraySections, checkPray, checkSalah } from '@api';
import { RootState } from '../index';

interface prayState {
  loading: boolean;
  loadingFarad: boolean;
  loadingSunaa: boolean;
  loadingSalah: boolean;
  currentSection: any;
  currentStatus: string;
  tarawehNum: any;
  statusCheck: string;
  status: string;
  fardList: any;
  sunnahList: any;
  date: any;
  taraweeh: {
    id: Number;
    name: string;
    type: string;
    value: Number | any;
    prayAt: string;
  };
  valueUpdated: {
    id: number;
    name: string;
    value: number;
    selected: boolean;
  };
  valueSalahUpdated: {
    id: number;
    name: string;
    value: number;
    selected: boolean;
  };
  tarawehUpdated: {
    id: Number;
    name: string;
    type: string;
    value: Number | any;
    prayAt: string;
  };
  qiyam: { id: Number; name: string; type: string; value: number; prayAt: string };
  qiyamUpdated: { id: Number; name: string; type: string; value: number; prayAt: string };
}

const initialState: prayState = {
  currentSection: '1',
  loading: false,
  loadingFarad: false,
  loadingSalah: false,
  loadingSunaa: false,
  date: moment(),
  statusCheck: '',
  status: '',
  currentStatus: '',
  tarawehNum: null,
  fardList: [],
  sunnahList: [],
  taraweeh: {
    id: null,
    name: '',
    type: '',
    value: null,
    prayAt: '',
  },
  qiyam: {
    id: null,
    name: '',
    type: '',
    value: null,
    prayAt: '',
  },
  valueUpdated: {
    id: null,
    name: '',
    value: null,
    selected: undefined,
  },
  valueSalahUpdated: {
    id: null,
    name: '',
    value: null,
    selected: undefined,
  },
  tarawehUpdated: {
    id: null,
    name: '',
    type: '',
    value: null,
    prayAt: '',
  },
  qiyamUpdated: {
    id: null,
    name: '',
    type: '',
    value: null,
    prayAt: '',
  },
};

export const praySlice = createSlice({
  name: 'pray',
  initialState,
  reducers: {
    setSection: (state, action) => {
      state.currentSection = action.payload.section;
      state.currentStatus = 'success';
    },
    setFarad: (state, action) => {
      let newList = [...state.fardList];
      let index = newList.findIndex((i: any) => i.id === action.payload?.id);
      newList[index] = { ...newList[index], selected: action.payload?.value };
      state.fardList = newList;
    },
    setSunaa: (state, action) => {
      let newList = [...state.sunnahList];
      let index = newList.findIndex((i: any) => i.id === action.payload?.id);
      newList[index] = { ...newList[index], selected: action.payload?.value };
      state.sunnahList = newList;
    },
    setSectionReset: (state) => {
      state.currentSection = '1';
      state.currentStatus = 'success';
    },
    setTaraweh: (state, action) => {
      state.tarawehNum = action.payload.taraweh;
    },
    setDate: (state, action) => {
      state.date = action.payload;
    },
    setFardLists: (state, action) => {
      const { fared, sunnah, taraweeh, qiyam } = action.payload;
      state.fardList = fared;
      state.sunnahList = sunnah;
      state.taraweeh = taraweeh;
      state.qiyam = qiyam;
    },
    setLoading: (state, action) => {
      state.loading = action.payload;
    },
    setLoadingFarad: (state, action) => {
      state.loadingFarad = action.payload;
    },
    setLoadingSunaa: (state, action) => {
      state.loadingSunaa = action.payload;
    },
    setLoadingSalah: (state, action) => {
      state.loadingSalah = action.payload;
    },
    setStatusCheck: (state, action) => {
      state.statusCheck = action.payload;
    },
    setStatus: (state, action) => {
      state.status = action.payload;
    },
    updateFard: (state, action) => {
      let newList = [...state.fardList];
      newList = action.payload;
      state.fardList = newList;
    },
    updateSunna: (state, action) => {
      let newList = [...state.sunnahList];
      newList = action.payload;
      state.sunnahList = newList;
    },
    updateTaraweh: (state, action) => {
      state.taraweeh = action.payload[0];
    },
    updateTarawehL: (state, action) => {
      state.taraweeh = action.payload;
    },
    updateQiaym: (state, action) => {
      state.qiyam = action.payload[0];
    },
    updateQiaymLocal: (state, action) => {
      state.qiyam = action.payload;
    },
  },
});

export const {
  setSection,
  setTaraweh,
  setFardLists,
  setLoading,
  setLoadingFarad,
  setLoadingSalah,
  setLoadingSunaa,
  setStatusCheck,
  setDate,
  setSectionReset,
  updateFard,
  updateSunna,
  updateTaraweh,
  updateQiaym,
  setStatus,
  setFarad,
  setSunaa,
  updateTarawehL,
  updateQiaymLocal,
} = praySlice.actions;

export const GetFardList = (value: any): any => {
  return async (dispatch: any) => {
    dispatch(setLoading(true));
    try {
      const result: any = await getPraySections(value);
      dispatch(setLoading(false));
      dispatch(setFardLists(result));
    } catch (error) {
      dispatch(setLoading(false));
    }
  };
};

export const CheckFardList = (prayers: any, date: any, type: any): any => {
  return async (dispatch: any) => {
    if (type === 'FARD') {
      dispatch(setLoadingFarad(true));
    } else {
      dispatch(setLoadingSunaa(true));
    }
    try {
      const result: any = await checkPray(prayers, date);
      if (type === 'FARD') {
        dispatch(setStatus(result.success));
        dispatch(setLoadingFarad(false));
        dispatch(updateFard(result?.prayers));
      } else {
        dispatch(setLoadingSunaa(false));
        dispatch(updateSunna(result?.prayers));
      }
    } catch (error) {
      Toast.show(error, {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        textColor: '#FFFF',
        shadow: true,
        animation: true,
        textStyle: { fontFamily: FontType.Regular },
        hideOnPress: true,
        backgroundColor: '#656B8D',
        delay: 0,
        opacity: 1,
        containerStyle: {
          marginBottom: 18,
          borderRadius: 10,
        },
      });
      dispatch(setLoadingFarad(false));
      dispatch(setLoadingSunaa(false));
    }
  };
};

export const CheckFardSalah = (prayers: any, date: any, type: any): any => {
  return async (dispatch: any) => {
    dispatch(setLoadingSalah(true));
    try {
      const result: any = await checkSalah(prayers, date);
      dispatch(setStatusCheck(result.success));
      dispatch(setLoadingSalah(false));
      console.log(result)
      if (type === 'TARAWEEH') {
        dispatch(updateTaraweh(result.prayers));
      }
      // else {
      //   dispatch(updateQiaym(result.prayers));
      // }
      Toast.show('✓', {
        duration: Toast.durations.SHORT,
        position: Toast.positions.BOTTOM,
        textColor: '#FFFF',
        shadow: true,
        animation: true,
        textStyle: { fontFamily: FontType.Regular },
        hideOnPress: true,
        backgroundColor: '#47d424',
        delay: 0,
        opacity: 1,
        containerStyle: {
          marginBottom: 18,
          borderRadius: 10,
        },
      });
    } catch (error) {
      Toast.show(error, {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        textColor: '#FFFF',
        shadow: true,
        animation: true,
        textStyle: { fontFamily: FontType.Regular },
        hideOnPress: true,
        backgroundColor: '#656B8D',
        delay: 0,
        opacity: 1,
        containerStyle: {
          marginBottom: 18,
          borderRadius: 10,
        },
      });
      dispatch(setLoadingSalah(false));
    }
  };
};

export const sectionSelector = (state: RootState) => state.pray.currentSection;
export const statusSelector = (state: RootState) => state.pray.currentStatus;
export const loadingGetSelector = (state: RootState) => state.pray.loading;
export const loadingFaradSelector = (state: RootState) => state.pray.loadingFarad;
export const loadingSunnaSelector = (state: RootState) => state.pray.loadingSunaa;
export const loadingSalahSelector = (state: RootState) => state.pray.loadingSalah;
export const statusCheckFSelector = (state: RootState) => state.pray.statusCheck;
export const fardListSelector = (state: RootState) => state.pray.fardList;
export const sunnahListSelector = (state: RootState) => state.pray.sunnahList;
export const tarawehSelector = (state: RootState) => state.pray.taraweeh;
export const qiyamSelector = (state: RootState) => state.pray.qiyam;
export const tarawehNumSelector = (state: RootState) => state.pray.tarawehNum;
export const dateSelector = (state: RootState) => state.pray.date;
export const statusCheckSelector = (state: RootState) => state.pray.status;
export default praySlice.reducer;
