import {createSlice} from '@reduxjs/toolkit';
import Toast from 'react-native-root-toast';
import FontType from '@theme/fonts';
import {getQuranSection, getDailyQuran, updateQuran, updateTimeRead} from '@api';
import {RootState} from '../index';

interface quranState {
  loading: boolean;
  loadingDailyQuran: boolean;
  dailyQuran: {
    id: number;
    userId: number;
    value: boolean;
    readAt: any;
    readTime: number;
  };
  stateRead: string;
  status: string;
  tracker: {
    juz: number;
    surah: number;
    ayah: number;
  };
  minlimit: {
    juz: number;
    surah: number;
    ayah: number;
  };
  maxlimit: {
    juz: number;
    surah: number;
    ayah: number;
  };
  trackerUpdated: {
    juz: number;
    surah: number;
    ayah: number;
  };
}
const initialState: quranState = {
  loading: false,
  loadingDailyQuran: false,
  dailyQuran: {
    id: null,
    userId: null,
    value: false,
    readAt: '',
    readTime: 0,
  },
  stateRead: '',
  status: '',
  tracker: {
    juz: null,
    surah: null,
    ayah: null,
  },
  trackerUpdated: {
    juz: null,
    surah: null,
    ayah: null,
  },
  minlimit: {
    juz: null,
    surah: null,
    ayah: null,
  },
  maxlimit: {
    juz: null,
    surah: null,
    ayah: null,
  },
};

export const quranSlice = createSlice({
  name: 'quran',
  initialState,
  reducers: {
    setQuranGet: (state, action) => {
      const {tracker, minLimit, maxLimit} = action.payload;
      state.tracker = tracker;
      state.maxlimit = maxLimit;
      state.minlimit = minLimit;
    },
    setQuranDaily: (state, action) => {
      state.dailyQuran = action.payload;
    },
    setLoading: (state, action) => {
      state.loading = action.payload;
    },
    setStatusQuran: (state, action) => {
      state.status = action.payload;
    },
    setLoadingDailyQuran: (state, action) => {
      state.loadingDailyQuran = action.payload;
    },
    setStateRead: (state, action) => {
      state.stateRead = action.payload;
    },
    updateQuranLocal: (state, action) => {
      state.tracker.ayah = action?.payload?.ayah;
      state.tracker.juz = action?.payload?.juz;
      state.tracker.surah = action?.payload?.surah;
    },
    updateValues: (state, action) => {
      const {tracker, minLimit, maxLimit} = action.payload;
      state.trackerUpdated = tracker;
      state.minlimit = minLimit;
      state.maxlimit = maxLimit;
      const {juz, surah, ayah} = state.trackerUpdated;
      state.tracker.ayah = ayah;
      state.tracker.surah = surah;
      state.tracker.juz = juz;
    },
    updateRead: (state, action) => {
      state.dailyQuran = action.payload;
    },
  },
});
export const {
  setLoading,
  setQuranGet,
  setLoadingDailyQuran,
  setQuranDaily,
  setStateRead,
  updateValues,
  setStatusQuran,
  updateRead,
  updateQuranLocal,
} = quranSlice.actions;

export const GetQuran = (): any => {
  return async (dispatch: any) => {
    dispatch(setLoading(true));
    try {
      const result: any = await getQuranSection();
      dispatch(setLoading(false));
      dispatch(setQuranGet(result));
    } catch (error) {
      dispatch(setLoading(false));
    }
  };
};

export const GetQuranDaily = (value: any): any => {
  return async (dispatch: any) => {
    dispatch(setLoadingDailyQuran(true));
    try {
      const result: any = await getDailyQuran(value);
      dispatch(setLoadingDailyQuran(false));
      dispatch(setQuranDaily(result.dailyQuran));
    } catch (error) {
      dispatch(setLoadingDailyQuran(false));
    }
  };
};

export const UpdateQuran = (juz: number, surah: number, ayah: number): any => {
  return async (dispatch: any) => {
    try {
      const result: any = await updateQuran(juz, surah, ayah);
      dispatch(updateValues(result));
      dispatch(setStatusQuran(result));
    } catch (error) {
      Toast.show(error, {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        textColor: '#FFFF',
        shadow: true,
        animation: true,
        textStyle: {fontFamily: FontType.Regular},
        hideOnPress: true,
        backgroundColor: '#656B8D',
        delay: 0,
        opacity: 1,
        containerStyle: {
          marginBottom: 18,
          borderRadius: 10,
        },
      });
    }
  };
};

export const UpdateRead = (value: number, date: any): any => {
  return async (dispatch: any) => {
    try {
      const result: any = await updateTimeRead(value, date);
      dispatch(updateRead(result.dailyQuran));
      dispatch(setStateRead(result.dailyQuran.readTime));
    } catch (error) {
      Toast.show(error, {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        textColor: '#FFFF',
        shadow: true,
        animation: true,
        textStyle: {fontFamily: FontType.Regular},
        hideOnPress: true,
        backgroundColor: '#656B8D',
        delay: 0,
        opacity: 1,
        containerStyle: {
          marginBottom: 18,
          borderRadius: 10,
        },
      });
    }
  };
};

export const loadingSelector = (state: RootState) => state.quran.loading;
export const loadingDailyQuranSelector = (state: RootState) => state.quran.loadingDailyQuran;
export const quranSelector = (state: RootState) => state.quran.tracker;
export const minLimitSelector = (state: RootState) => state.quran.minlimit;
export const maxLimitSelector = (state: RootState) => state.quran.maxlimit;
export const stateReadSelector = (state: RootState) => state.quran.stateRead;
export const dailyQuranSelector = (state: RootState) => state.quran.dailyQuran;
export const statusSelector = (state: RootState) => state.quran.status;
export default quranSlice.reducer;
