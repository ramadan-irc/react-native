import { createSlice } from '@reduxjs/toolkit';
import Toast from 'react-native-root-toast';
import FontType from '@theme/fonts';
import { getDailyTasks, checkTask } from '@api';
import { RootState } from '../index';
// import R from 'reactotron-react-native';
interface dailyState {
  loadingTasksList: boolean;
  requestloading: boolean;
  tasksList: any;
  status: string;
  taskUpdated: {
    id: number;
    name: string;
    value: boolean;
  };
}

const initialState: dailyState = {
  loadingTasksList: false,
  requestloading: false,
  tasksList: [],
  status: '',
  taskUpdated: {
    id: null,
    name: '',
    value: false,
  },
};

export const dailySlice = createSlice({
  name: 'daily',
  initialState,
  reducers: {
    setTasksLists: (state, action) => {
      state.tasksList = action.payload;
    },
    setLoadingTasksList: (state, action) => {
      state.loadingTasksList = action.payload;
    },
    setRequestLoading: (state, action) => {
      state.requestloading = action.payload;
    },
    setStatus: (state, action) => {
      state.status = action.payload;
    },
    setTasks: (state, action) => {
      let newList = [...state.tasksList];
      let index = newList.findIndex((i: any) => i.id === action.payload?.id);
      newList[index] = { ...newList[index], selected: action.payload?.value };
      state.tasksList = newList;
    },
    updateTaskList: (state, action) => {
      let newList = [...state.tasksList];
      newList = action.payload;
      state.tasksList = newList;
    },
  },
});

export const { setLoadingTasksList, setRequestLoading, setTasksLists, updateTaskList, setStatus, setTasks } = dailySlice.actions;

export const GetTasksList = (value: any): any => {
  return async (dispatch: any) => {
    dispatch(setLoadingTasksList(true));
    try {
      const result: any = await getDailyTasks(value);
      dispatch(setLoadingTasksList(false));
      dispatch(setTasksLists(result.tasks));
    } catch (error) {
      dispatch(setLoadingTasksList(false));
    }
  };
};

export const CheckTask = (tasks: any, date: any): any => {
  return async (dispatch: any) => {
    dispatch(setRequestLoading(true));

    try {
      const result: any = await checkTask(tasks, date);
      dispatch(updateTaskList(result?.tasks));
      dispatch(setStatus(result.task));
      dispatch(setRequestLoading(false));
      Toast.show('✓', {
        duration: Toast.durations.SHORT,
        position: Toast.positions.BOTTOM,
        textColor: '#FFFF',
        shadow: true,
        animation: true,
        textStyle: { fontFamily: FontType.Regular },
        hideOnPress: true,
        backgroundColor: '#47d424',
        delay: 0,
        opacity: 1,
        containerStyle: {
          marginBottom: 18,
          borderRadius: 10,
        },
      });
    } catch (error) {
      dispatch(setRequestLoading(false));
      Toast.show(error, {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        textColor: '#FFFF',
        shadow: true,
        animation: true,
        textStyle: { fontFamily: FontType.Regular },
        hideOnPress: true,
        backgroundColor: '#656B8D',
        delay: 0,
        opacity: 1,
        containerStyle: {
          marginBottom: 18,
          borderRadius: 10,
        },
      });
    }
  };
};

export const loadingGetTasksSelector = (state: RootState) => state.daily.loadingTasksList;
export const requestloadingGetTasksSelector = (state: RootState) => state.daily.requestloading;
export const tasksListSelector = (state: RootState) => state.daily.tasksList;
export const statusDailySelector = (state: RootState) => state.daily.status;
export const taskUpdateSelector = (state: RootState) => state.daily.taskUpdated;
export default dailySlice.reducer;
