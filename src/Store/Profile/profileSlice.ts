import { createSlice } from '@reduxjs/toolkit';
import { editProfile, getProfile, postFeedback } from '@api';
import { RootState } from '../index';

import Toast from 'react-native-root-toast';
// import R from 'reactotron-react-native';
import FontType from '@theme/fonts';
import MyToast from '@components/Toast/component';
interface profileState {
  isOnEdit: boolean;
  city: string;
  error: string;
  loadingGet: boolean;
  loadingEdit: boolean;
  loadingFeedback: boolean;
  statusFeedback: string;
  feedback: {
    username: string;
    body: string;
    date: string;
    version: string;
    email: string;
    id: number;
  };
  status: string;
  user: {
    id: any;
    username: any;
    email: any;
    location: any;
    age: any;
    gender: any;
  };
}

const initialState: profileState = {
  isOnEdit: false,
  city: '',
  loadingGet: false,
  loadingEdit: false,
  statusFeedback: '',
  error: '',
  status: undefined,
  loadingFeedback: false,
  feedback: {
    username: '',
    body: '',
    date: '',
    version: '',
    email: '',
    id: null,
  },
  user: {
    id: undefined,
    username: undefined,
    email: undefined,
    location: undefined,
    age: undefined,
    gender: undefined,
  },
};

export const profileSlice = createSlice({
  name: 'profile',
  initialState,
  reducers: {
    setEdit: (state, action) => {
      state.isOnEdit = action.payload;
    },
    setStatusFeedback: (state, action) => {
      state.statusFeedback = action.payload;
    },
    setError: (state, action) => {
      state.error = action.payload;
    },
    setStatus: (state, action) => {
      state.status = action.payload;
    },
    setLoadingEdit: (state, action) => {
      state.loadingEdit = action.payload;
    },
    setLoadingGet: (state, action) => {
      state.loadingGet = action.payload;
    },
    setLoadingFeedback: (state, action) => {
      state.loadingFeedback = action.payload;
    },
    setCity: (state, action) => {
      state.city = action.payload.city;
    },
    setNewUser: (state, action) => {
      const { user } = action.payload;
      state.user = user;
    },
    setResponseFeedback: (state, action) => {
      state.feedback = action.payload;
    },
  },
});
export const {
  setEdit,
  setCity,
  setNewUser,
  setLoadingEdit,
  setLoadingGet,
  setError,
  setStatus,
  setLoadingFeedback,
  setResponseFeedback,
  setStatusFeedback,
} = profileSlice.actions;

export const EditProfile = (
  username: any,
  email: any,
  location: any,
  age: any,
  gender: any,
): any => {
  return async (dispatch: any) => {
    dispatch(setLoadingEdit(true));
    try {
      await editProfile(username, email, location, age, gender);
      dispatch(setLoadingEdit(false));
    } catch (error) {
      dispatch(setError(error));
      dispatch(setStatus('fail'));
      Toast.show(error, {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        textColor: '#FFFF',
        shadow: true,
        animation: true,
        textStyle: { fontFamily: FontType.Regular },
        hideOnPress: true,
        backgroundColor: '#656B8D',
        delay: 0,
        opacity: 1,
        containerStyle: {
          marginBottom: 18,
          borderRadius: 10,
        },
      });
      dispatch(setLoadingEdit(false));
    }
  };
};
export const GetProfile = () => {
  return async (dispatch: any) => {
    dispatch(setLoadingGet(true));
    try {
      const result: any = await getProfile();
      dispatch(setLoadingGet(false));
      dispatch(setNewUser(result));
    } catch (error) {
      dispatch(setError(error));
      MyToast('Sorry, network request failed')
      dispatch(setLoadingGet(false));
    }
  };
};
export const GiveFeedback = (version: string, body: string, value: any): any => {
  return async (dispatch: any) => {
    dispatch(setLoadingFeedback(true));
    try {
      const result: any = await postFeedback(version, body, value);
      dispatch(setResponseFeedback(result.feedback));
      dispatch(setStatusFeedback(result.success));
      dispatch(setLoadingFeedback(false));
    } catch (error) {
      Toast.show(error, {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        textColor: '#FFFF',
        shadow: true,
        animation: true,
        textStyle: { fontFamily: FontType.Regular },
        hideOnPress: true,
        backgroundColor: '#656B8D',
        delay: 0,
        opacity: 1,
        containerStyle: {
          marginBottom: 18,
          borderRadius: 10,
        },
      });
      dispatch(setLoadingFeedback(false));
    }
  };
};
export const editSelector = (state: RootState) => state.profile.isOnEdit;
export const citySelector = (state: RootState) => state.profile.city;
export const loadingGetSelector = (state: RootState) => state.profile.loadingGet;
export const loadingEditSelector = (state: RootState) => state.profile.loadingEdit;
export const loadingFeedbackSelector = (state: RootState) => state.profile.loadingFeedback;
export const newUserSelector = (state: RootState) => state.profile.user;
export const feedbackResponseSelector = (state: RootState) => state.profile.feedback;
export const ErrorSelector = (state: RootState) => state.profile.error;
export const StatusSelector = (state: RootState) => state.profile.status;
export const StatusFeedbackSelector = (state: RootState) => state.profile.statusFeedback;
export default profileSlice.reducer;
