import { userSlice } from './userSlice';
import { loginUser, registerUser, recoverPassword, setNotifyUser, setLang } from '@api';
import Toast from 'react-native-root-toast';
import FontType from '@theme/fonts';
import AsyncStorage from '@react-native-async-storage/async-storage';
import MyToast from '@components/Toast/component';

export const {
  setAuth,
  setIsLang,
  setError,
  setUser,
  logout,
  setLoading,
  setMessageRecover,
  resetMessageRecover,
  setRegisterToken,
  setNotify,
  setStatusNotify,
  setLoadingNotify,
  setLangRes,
  setLoadingLang,
  setCalendarType,
  setAdjType,
  setScheme,
} = userSlice.actions;

export const login = (
  id: any,
  email: any,
  username: any,
  password: any,
  registrationToken: any,
  date: any,
  language: any,
): any => {
  return async (dispatch: any) => {
    dispatch(setLoading(true));
    try {
      const result: any = await loginUser(
        id,
        email,
        username,
        password,
        registrationToken,
        date,
        language,
      );
      dispatch(setLoading(false));
      dispatch(setUser(result));
      dispatch(setAuth(true));
      await AsyncStorage.setItem('@expirs', 'true');
    } catch (error) {
      dispatch(setError(error));
      Toast.show(error, {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        textColor: '#FFFF',
        shadow: true,
        animation: true,
        textStyle: { fontFamily: FontType.Regular },
        hideOnPress: true,
        backgroundColor: '#656B8D',
        delay: 0,
        opacity: 1,
        containerStyle: {
          marginBottom: 18,
          borderRadius: 10,
        },
      });
      dispatch(setLoading(false));
    }
  };
};
export const register = (
  username: any,
  email: any,
  password: any,
  language: any,
  registrationToken: any,
  date: any,
): any => {
  return async (dispatch: any) => {
    dispatch(setLoading(true));
    try {
      const result: any = await registerUser(
        username,
        email,
        password,
        language,
        registrationToken,
        date,
      );
      dispatch(setLoading(false));
      dispatch(setUser(result));
      dispatch(setAuth(true));
      await AsyncStorage.setItem('@expirs', 'true');
    } catch (error) {
      // dispatch(setError(error));
      dispatch(setLoading(false));
      Toast.show(error, {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        textColor: '#FFFF',
        shadow: true,
        animation: true,
        textStyle: { fontFamily: FontType.Regular },
        hideOnPress: true,
        backgroundColor: '#656B8D',
        delay: 0,
        opacity: 1,
        containerStyle: {
          marginBottom: 18,
          borderRadius: 10,
        },
      });
    }
  };
};
export const recoverpassword = (email: any): any => {
  return async (dispatch: any) => {
    dispatch(setLoading(true));
    try {
      const result: any = await recoverPassword(email);
      dispatch(setLoading(false));
      dispatch(setMessageRecover(result.success));
      Toast.show(result.success, {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        textColor: '#FFFF',
        shadow: true,
        animation: true,
        textStyle: { fontFamily: FontType.Regular },
        hideOnPress: true,
        backgroundColor: '#656B8D',
        delay: 0,
        opacity: 1,
        containerStyle: {
          marginBottom: 18,
          borderRadius: 10,
        },
      });
    } catch (error) {
      dispatch(setError(error));
      dispatch(setLoading(false));
    }
  };
};
export const SetNotify = (value: any): any => {
  return async (dispatch: any) => {
    dispatch(setLoadingNotify(true));
    dispatch(setNotify(value));
    try {
      const result: any = await setNotifyUser(value);
      dispatch(setLoadingNotify(false));
      dispatch(setStatusNotify('success'));
      Toast.show(result.success, {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        textColor: '#FFFF',
        shadow: true,
        animation: true,
        textStyle: { fontFamily: FontType.Regular },
        hideOnPress: true,
        backgroundColor: '#656B8D',
        delay: 0,
        opacity: 1,
        containerStyle: {
          marginBottom: 18,
          borderRadius: 10,
        },
      });
    } catch (error) {
      dispatch(setStatusNotify('error'));
      dispatch(setNotify(!value));
      MyToast('Sorry, network request failed')
      dispatch(setLoadingNotify(false));
    }
  };
};

export const SetLang = (language: any): any => {
  return async (dispatch: any) => {
    dispatch(setLoadingLang(true));
    try {
      const result: any = await setLang(language);
      dispatch(setLoadingLang(false));
      dispatch(setLangRes(result.success));
    } catch (error) {
      Toast.show(error, {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        textColor: '#FFFF',
        shadow: true,
        animation: true,
        textStyle: { fontFamily: FontType.Regular },
        hideOnPress: true,
        backgroundColor: '#656B8D',
        delay: 0,
        opacity: 1,
        containerStyle: {
          marginBottom: 18,
          borderRadius: 10,
        },
      });
      dispatch(setLoadingLang(false));
    }
  };
};
