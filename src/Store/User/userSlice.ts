import {createSlice} from '@reduxjs/toolkit';
import {setAuthToken} from '@api';

interface userState {
  isAuthenticated: boolean;
  langSelected: boolean;
  loadingLang: boolean;
  error: string;
  resLang: string;
  accessToken: string;
  refreshToken: string;
  loading: boolean;
  loadingNotfiy: boolean;
  message: any;
  tokenRegister: string;
  statusNotify: string;
  adjType: string;
  calendar: string;
  notify: boolean;
  isDark: boolean;
  user: {
    id: any;
    username: any;
    email: any;
  };
}

const initialState: userState = {
  isAuthenticated: false,
  langSelected: false,
  loadingLang: false,
  isDark: false,
  error: '',
  resLang: '',
  adjType: '0',
  accessToken: undefined,
  refreshToken: undefined,
  loading: false,
  loadingNotfiy: false,
  message: undefined,
  notify: true,
  calendar: 'Gregorian',
  tokenRegister: '',
  statusNotify: '',

  user: {
    id: undefined,
    username: undefined,
    email: undefined,
  },
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setError: (state, action) => {
      state.error = action.payload;
    },
    setLoading: (state, action) => {
      state.loading = action.payload;
    },
    setLoadingNotify: (state, action) => {
      state.loadingNotfiy = action.payload;
    },
    setLoadingLang: (state, action) => {
      state.loadingLang = action.payload;
    },
    setLangRes: (state, action) => {
      state.resLang = action.payload;
    },
    setScheme: (state, action) => {
      state.isDark = action.payload;
    },
    setUser: (state, action) => {
      const {user, accessToken, refreshToken} = action.payload;
      setAuthToken(accessToken, refreshToken);
      state.user = user;
      state.accessToken = accessToken;
      state.refreshToken = refreshToken;
    },
    setAuth: (state, action) => {
      state.isAuthenticated = action.payload;
    },
    setIsLang: (state, action) => {
      state.langSelected = action.payload;
    },
    setMessageRecover: (state, action) => {
      state.message = action.payload;
    },
    resetMessageRecover: (state, action) => {
      state.message = action.payload;
    },
    setRegisterToken: (state, action) => {
      state.tokenRegister = action.payload;
    },
    setNotify: (state, action) => {
      state.notify = action.payload;
    },
    setStatusNotify: (state, action) => {
      state.statusNotify = action.payload;
    },
    setCalendarType: (state, action) => {
      state.calendar = action.payload;
    },
    setAdjType: (state, action) => {
      state.adjType = action.payload;
    },
    logout: () => initialState,
  },
});

export default userSlice.reducer;
