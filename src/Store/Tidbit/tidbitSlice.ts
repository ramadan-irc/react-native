import {createSlice} from '@reduxjs/toolkit';
import {getTidbit, getTidbitFav, addFavTidbit, removeFavTidbit} from '@api';
import {RootState} from '../index';
import Toast from 'react-native-root-toast';
// import R from 'reactotron-react-native';
import FontType from '@theme/fonts';
interface tidbitState {
  loadingGet: boolean;
  loadingGetFav: boolean;
  loadingAdd: boolean;
  loadingRemove: boolean;
  tidbits: any;
  tidbitsFav: any;
  error: string;
  statusAdd: string;
  statusRemove: string;
  tidbitUpdated: {
    id: number;
    text: string;
    deedOfTheDayDate: string;
  };
}

const initialState: tidbitState = {
  loadingGet: false,
  loadingGetFav: false,
  loadingAdd: false,
  loadingRemove: false,
  tidbits: [],
  tidbitsFav: [],
  error: '',
  statusAdd: '',
  statusRemove: '',
  tidbitUpdated: {
    id: null,
    text: '',
    deedOfTheDayDate: '',
  },
};

export const tidbitSlice = createSlice({
  name: 'tidbit',
  initialState,
  reducers: {
    setLoadingGet: (state, action) => {
      state.loadingGet = action.payload;
    },
    setLoadingGetFav: (state, action) => {
      state.loadingGetFav = action.payload;
    },
    setLoadingAdd: (state, action) => {
      state.loadingAdd = action.payload;
    },
    setLoadingRemove: (state, action) => {
      state.loadingRemove = action.payload;
    },
    setTidbitList: (state, action) => {
      state.tidbits = action.payload;
    },
    setTidbitListFav: (state, action) => {
      state.tidbitsFav = action.payload;
    },
    setError: (state, action) => {
      state.error = action.payload;
    },
    setStatusAdd: (state, action) => {
      state.statusAdd = action.payload;
    },
    setStatusRemove: (state, action) => {
      state.statusRemove = action.payload;
    },
    updateTidbit: (state, action) => {
      state.tidbitUpdated = action.payload.result;
      let newList = [...state.tidbits];
      let index = newList.findIndex((i) => i.id === state.tidbitUpdated.id);
      newList[index] = {...newList[index], isFavorite: action.payload.value};
      state.tidbits = newList;
    },
    popTidbit: (state, action) => {
      state.tidbitUpdated = action.payload.result;
      let List = [...state.tidbitsFav];
      let tidbitList = [...state.tidbits];
      let newList = List.filter((i) => i.id !== state.tidbitUpdated.id);
      let index = tidbitList.findIndex((i) => i.id === state.tidbitUpdated.id);
      tidbitList[index] = {...tidbitList[index], isFavorite: false};
      state.tidbitsFav = newList;
      state.tidbits = tidbitList;
    },
  },
});
export const {
  setLoadingGet,
  setError,
  setTidbitList,
  setLoadingGetFav,
  setTidbitListFav,
  setStatusAdd,
  setStatusRemove,
  setLoadingAdd,
  setLoadingRemove,
  updateTidbit,
  popTidbit,
} = tidbitSlice.actions;

export const GetTidbits = () => {
  return async (dispatch: any) => {
    dispatch(setLoadingGet(true));
    try {
      const result: any = await getTidbit();
      dispatch(setLoadingGet(false));
      dispatch(setTidbitList(result.tidbits));
    } catch (error) {
      dispatch(setError(error));
      Toast.show(error, {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        textColor: '#FFFF',
        shadow: true,
        animation: true,
        textStyle: {fontFamily: FontType.Regular},
        hideOnPress: true,
        backgroundColor: '#656B8D',
        delay: 0,
        opacity: 1,
        containerStyle: {
          marginBottom: 18,
          borderRadius: 10,
        },
      });
      dispatch(setLoadingGet(false));
    }
  };
};
export const GetTidbitsFav = () => {
  return async (dispatch: any) => {
    dispatch(setLoadingGetFav(true));
    try {
      const result: any = await getTidbitFav();
      dispatch(setLoadingGetFav(false));
      dispatch(setTidbitListFav(result.favorites));
    } catch (error) {
      dispatch(setError(error));
      dispatch(setLoadingGetFav(false));
    }
  };
};
export const addTidbitFav = (id: any): any => {
  return async (dispatch: any) => {
    try {
      const result: any = await addFavTidbit(id);
      dispatch(setStatusAdd(result.success));
      dispatch(
        updateTidbit({
          result: result.tidbit,
          value: true,
        }),
      );
    } catch (error) {
      dispatch(setStatusAdd(error));
      Toast.show(error, {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        textColor: '#FFFF',
        shadow: true,
        animation: true,
        textStyle: {fontFamily: FontType.Regular},
        hideOnPress: true,
        backgroundColor: '#656B8D',
        delay: 0,
        opacity: 1,
        containerStyle: {
          marginBottom: 18,
          borderRadius: 10,
        },
      });
    }
  };
};
export const removeTidbitFav = (id: any): any => {
  return async (dispatch: any) => {
    try {
      const result: any = await removeFavTidbit(id);

      dispatch(setStatusRemove(result.success));
      dispatch(
        updateTidbit({
          result: result.tidbit,
          value: false,
        }),
      );
    } catch (error) {
      dispatch(setStatusRemove(error));
      Toast.show(error, {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        textColor: '#FFFF',
        shadow: true,
        animation: true,
        textStyle: {fontFamily: FontType.Regular},
        hideOnPress: true,
        backgroundColor: '#656B8D',
        delay: 0,
        opacity: 1,
        containerStyle: {
          marginBottom: 18,
          borderRadius: 10,
        },
      });
    }
  };
};
export const popTidbitFav = (id: any): any => {
  return async (dispatch: any) => {
    try {
      const result: any = await removeFavTidbit(id);

      dispatch(setStatusRemove(result.success));
      dispatch(
        popTidbit({
          result: result.tidbit,
        }),
      );
    } catch (error) {
      dispatch(setStatusRemove(error));
      Toast.show(error, {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        textColor: '#FFFF',
        shadow: true,
        animation: true,
        textStyle: {fontFamily: FontType.Regular},
        hideOnPress: true,
        backgroundColor: '#656B8D',
        delay: 0,
        opacity: 1,
        containerStyle: {
          marginBottom: 18,
          borderRadius: 10,
        },
      });
    }
  };
};

export const loadingGetSelector = (state: RootState) => state.tidbit.loadingGet;
export const tidbitsSelector = (state: RootState) => state.tidbit.tidbits;
export const loadingGetFavSelector = (state: RootState) => state.tidbit.loadingGetFav;
export const tidbitsFavSelector = (state: RootState) => state.tidbit.tidbitsFav;
export const addFavStateSelector = (state: RootState) => state.tidbit.statusAdd;
export const removeFavStateSelector = (state: RootState) => state.tidbit.statusRemove;
export default tidbitSlice.reducer;
