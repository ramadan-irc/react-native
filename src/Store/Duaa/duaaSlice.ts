import {createSlice} from '@reduxjs/toolkit';
import {getDuaa, getDuaaFav, addFavDuaa, removeFavDuaa} from '@api';
import {RootState} from '../index';
import Toast from 'react-native-root-toast';
// import R from 'reactotron-react-native';
import FontType from '@theme/fonts';
interface duaaState {
  loadingGet: boolean;
  loadingGetFav: boolean;
  loadingAdd: boolean;
  loadingRemove: boolean;
  duaas: any;
  duaasFav: any;
  error: string;
  statusAdd: boolean;
  statusRemove: boolean;
  duaaUpdated: {
    id: number;
    textArabic: string;
    textInbetween: string;
    textEnglish: string;
    textFrench: string;
  };
}

const initialState: duaaState = {
  loadingGet: false,
  loadingGetFav: false,
  loadingAdd: false,
  loadingRemove: false,
  duaas: [],
  duaasFav: [],
  error: '',
  statusAdd: false,
  statusRemove: false,
  duaaUpdated: {
    id: null,
    textArabic: '',
    textEnglish: '',
    textInbetween: '',
    textFrench: '',
  },
};

export const duaaSlice = createSlice({
  name: 'duaa',
  initialState,
  reducers: {
    setLoadingGet: (state, action) => {
      state.loadingGet = action.payload;
    },
    setLoadingGetFav: (state, action) => {
      state.loadingGetFav = action.payload;
    },
    setLoadingAdd: (state, action) => {
      state.loadingAdd = action.payload;
    },
    setLoadingRemove: (state, action) => {
      state.loadingRemove = action.payload;
    },
    setDuaaList: (state, action) => {
      state.duaas = action.payload;
    },
    setDuaaListFav: (state, action) => {
      state.duaasFav = action.payload;
    },
    setError: (state, action) => {
      state.error = action.payload;
    },
    setStatusAdd: (state, action) => {
      state.statusAdd = action.payload;
    },
    setStatusRemove: (state, action) => {
      state.statusRemove = action.payload;
    },
    updateDuaa: (state, action) => {
      state.duaaUpdated = action.payload.result;
      let newList = [...state.duaas];
      let index = newList.findIndex((i) => i.id === state.duaaUpdated.id);
      newList[index] = {...newList[index], isFavorite: action.payload.value};
      state.duaas = newList;
    },
    popDuaa: (state, action) => {
      state.duaaUpdated = action.payload.result;
      let List = [...state.duaasFav];
      let duaaList = [...state.duaas];
      let newList = List.filter((i) => i.id !== state.duaaUpdated.id);
      let index = duaaList.findIndex((i) => i.id === state.duaaUpdated.id);
      duaaList[index] = {...duaaList[index], isFavorite: false};
      state.duaasFav = newList;
      state.duaas = duaaList;
    },
  },
});
export const {
  setLoadingGet,
  setError,
  setDuaaList,
  setLoadingGetFav,
  setDuaaListFav,
  setStatusAdd,
  setStatusRemove,
  updateDuaa,
  popDuaa,
} = duaaSlice.actions;

export const GetDuaas = () => {
  return async (dispatch: any) => {
    dispatch(setLoadingGet(true));
    try {
      const result: any = await getDuaa();
      dispatch(setLoadingGet(false));
      dispatch(setDuaaList(result.duas));
    } catch (error) {
      dispatch(setError(error));

      dispatch(setLoadingGet(false));
    }
  };
};
export const GetDuaasFav = () => {
  return async (dispatch: any) => {
    dispatch(setLoadingGetFav(true));
    try {
      const result: any = await getDuaaFav();
      dispatch(setLoadingGetFav(false));
      dispatch(setDuaaListFav(result.favorites));
    } catch (error) {
      dispatch(setError(error));

      dispatch(setLoadingGetFav(false));
    }
  };
};
export const addDuaaFav = (id: any): any => {
  return async (dispatch: any) => {
    try {
      const result: any = await addFavDuaa(id);
      dispatch(
        updateDuaa({
          result: result.dua,
          value: true,
        }),
      );
    } catch (error) {
      dispatch(setStatusAdd(false));
      Toast.show(error, {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        textColor: '#FFFF',
        shadow: true,
        animation: true,
        textStyle: {fontFamily: FontType.Regular},
        hideOnPress: true,
        backgroundColor: '#656B8D',
        delay: 0,
        opacity: 1,
        containerStyle: {
          marginBottom: 18,
          borderRadius: 10,
        },
      });
    }
  };
};
export const removeDuaaFav = (id: any): any => {
  return async (dispatch: any) => {
    try {
      const result: any = await removeFavDuaa(id);
      dispatch(setStatusRemove(true));
      dispatch(
        updateDuaa({
          result: result.dua,
          value: false,
        }),
      );
    } catch (error) {
      dispatch(setStatusRemove(false));
      Toast.show(error, {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        textColor: '#FFFF',
        shadow: true,
        animation: true,
        textStyle: {fontFamily: FontType.Regular},
        hideOnPress: true,
        backgroundColor: '#656B8D',
        delay: 0,
        opacity: 1,
        containerStyle: {
          marginBottom: 18,
          borderRadius: 10,
        },
      });
    }
  };
};
export const popDuaaFav = (id: any): any => {
  return async (dispatch: any) => {
    try {
      const result: any = await removeFavDuaa(id);
      dispatch(setStatusRemove(true));
      dispatch(
        popDuaa({
          result: result.dua,
        }),
      );
    } catch (error) {
      dispatch(setStatusRemove(false));
      Toast.show(error, {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        textColor: '#FFFF',
        shadow: true,
        animation: true,
        textStyle: {fontFamily: FontType.Regular},
        hideOnPress: true,
        backgroundColor: '#656B8D',
        delay: 0,
        opacity: 1,
        containerStyle: {
          marginBottom: 18,
          borderRadius: 10,
        },
      });
    }
  };
};

export const loadingGetSelector = (state: RootState) => state.duaa.loadingGet;
export const duaasSelector = (state: RootState) => state.duaa.duaas;
export const loadingDuaaFavSelector = (state: RootState) => state.duaa.loadingGetFav;
export const duaasFavSelector = (state: RootState) => state.duaa.duaasFav;
export const addFavStateSelector = (state: RootState) => state.duaa.statusAdd;
export const removeFavStateSelector = (state: RootState) => state.duaa.statusRemove;
export default duaaSlice.reducer;
