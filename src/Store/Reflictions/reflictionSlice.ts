import { createSlice } from '@reduxjs/toolkit';
import {
  getReflictions,
  addRefliction,
  editRefliction,
  deleteRefliction,
  Setrefliction,
  getTilte,
} from '@api';
import { RootState } from '../index';
import Toast from 'react-native-root-toast';
// import R from 'reactotron-react-native';
import FontType from '@theme/fonts';
interface reflictionState {
  loadingGet: boolean;
  loadingAdd: boolean;
  loadingEdit: boolean;
  loadingDelete: boolean;
  loadingSet: boolean;
  loadingTilte: boolean;
  edit: boolean;
  reflections: any;
  statusAdd: string;
  statusDelete: string;
  statusEdit: string;
  itemRefliction: any;
  reflictionItem: any;
  title: {
    id: number;
    textEnglish: string;
    textFrench: string;
    date: string;
  };
}

const initialState: reflictionState = {
  loadingGet: false,
  loadingAdd: false,
  loadingEdit: false,
  loadingDelete: false,
  loadingSet: false,
  loadingTilte: false,
  statusEdit: '',
  edit: false,
  reflections: [],
  statusAdd: '',
  statusDelete: '',
  itemRefliction: null,
  reflictionItem: null,
  title: {
    id: null,
    textEnglish: '',
    textFrench: '',
    date: '',
  },
};

export const reflicationSlice = createSlice({
  name: 'refliction',
  initialState,
  reducers: {
    setLoadingGet: (state, action) => {
      state.loadingGet = action.payload;
    },
    setLoadingAdd: (state, action) => {
      state.loadingAdd = action.payload;
    },
    setLoadingEdit: (state, action) => {
      state.loadingEdit = action.payload;
    },
    setLoadingDelete: (state, action) => {
      state.loadingDelete = action.payload;
    },
    setLoadingSet: (state, action) => {
      state.loadingSet = action.payload;
    },
    setLoadingTitle: (state, action) => {
      state.loadingTilte = action.payload;
    },
    setEdit: (state, action) => {
      state.edit = action.payload;
    },
    setReflictionList: (state, action) => {
      state.reflections = action.payload;
    },
    setStatusAdd: (state, action) => {
      state.statusAdd = action.payload;
    },
    setStatusEdit: (state, action) => {
      state.statusEdit = action.payload;
    },
    setStatusDelete: (state, action) => {
      state.statusDelete = action.payload;
    },
    setSelectedItem: (state, action) => {
      const { title } = action.payload;
      state.title = title;
    },
    setTilte: (state, action) => {
      state.itemRefliction = action.payload;
    },
    setReflctionItem: (state, action) => {
      state.reflictionItem = action.payload;
    },
  },
});
export const {
  setLoadingGet,
  setReflictionList,
  setStatusAdd,
  setLoadingAdd,
  setSelectedItem,
  setEdit,
  setLoadingEdit,
  setLoadingDelete,
  setStatusDelete,
  setLoadingSet,
  setReflctionItem,
  setStatusEdit,
  setLoadingTitle,
  setTilte,
} = reflicationSlice.actions;

export const GetReflictions = () => {
  return async (dispatch: any) => {
    dispatch(setLoadingGet(true));
    try {
      const result: any = await getReflictions();
      dispatch(setLoadingGet(false));
      dispatch(setReflictionList(result.reflections));
    } catch (error) {
      dispatch(setLoadingGet(false));
    }
  };
};
export const AddRefliction = (text: string): any => {
  return async (dispatch: any) => {
    dispatch(setLoadingAdd(true));
    try {
      const result: any = await addRefliction(text);
      dispatch(setStatusAdd(result.success));
      dispatch(setLoadingAdd(false));
    } catch (error) {
      dispatch(setStatusAdd(error));
      dispatch(setLoadingAdd(false));
      Toast.show(error, {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        textColor: '#FFFF',
        shadow: true,
        animation: true,
        textStyle: { fontFamily: FontType.Regular },
        hideOnPress: true,
        backgroundColor: '#656B8D',
        delay: 0,
        opacity: 1,
        containerStyle: {
          marginBottom: 18,
          borderRadius: 10,
        },
      });
    }
  };
};
export const EditRefliction = (id: any, text: string, date: any): any => {
  return async (dispatch: any) => {
    dispatch(setLoadingEdit(true));
    try {
      const result: any = await editRefliction(id, text, date);
      dispatch(setLoadingEdit(false));
      dispatch(setStatusEdit(result.success));
    } catch (error) {
      dispatch(setStatusEdit(error));
      Toast.show(error, {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        textColor: '#FFFF',
        shadow: true,
        animation: true,
        textStyle: { fontFamily: FontType.Regular },
        hideOnPress: true,
        backgroundColor: '#656B8D',
        delay: 0,
        opacity: 1,
        containerStyle: {
          marginBottom: 18,
          borderRadius: 10,
        },
      });
      dispatch(setLoadingEdit(false));
    }
  };
};
export const DeleteRefliction = (id: any): any => {
  return async (dispatch: any) => {
    dispatch(setLoadingDelete(true));
    try {
      const result: any = await deleteRefliction(id);
      dispatch(setLoadingDelete(false));
      dispatch(setStatusDelete(result.success));
    } catch (error) {
      dispatch(setStatusDelete(error));

      dispatch(setLoadingDelete(false));
    }
  };
};
export const setRefliction = (id: any): any => {
  return async (dispatch: any) => {
    dispatch(setLoadingSet(true));
    try {
      const result: any = await Setrefliction(id);
      dispatch(setLoadingSet(false));
      dispatch(setReflctionItem(result.reflections));
    } catch (error) {
      Toast.show(error, {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        textColor: '#FFFF',
        shadow: true,
        animation: true,
        textStyle: { fontFamily: FontType.Regular },
        hideOnPress: true,
        backgroundColor: '#656B8D',
        delay: 0,
        opacity: 1,
        containerStyle: {
          marginBottom: 18,
          borderRadius: 10,
        },
      });
      dispatch(setLoadingSet(false));
    }
  };
};

export const GetTilte = (date: any): any => {
  return async (dispatch: any) => {
    dispatch(setLoadingTitle(true));
    try {
      const result: any = await getTilte(date);
      dispatch(setLoadingTitle(false));
      dispatch(setTilte(result));
    } catch (error) {
      dispatch(setLoadingTitle(false));
    }
  };
};

export const loadingGetSelector = (state: RootState) => state.refliction.loadingGet;
export const loadingAddSelector = (state: RootState) => state.refliction.loadingAdd;
export const loadingEditSelector = (state: RootState) => state.refliction.loadingEdit;
export const loadingDeleteSelector = (state: RootState) => state.refliction.loadingDelete;
export const loadingSetSelector = (state: RootState) => state.refliction.loadingSet;
export const reflictionsSelector = (state: RootState) => state.refliction.reflections;
export const addStateSelector = (state: RootState) => state.refliction.statusAdd;
export const deleteStateSelector = (state: RootState) => state.refliction.statusDelete;
export const itemSelectedSelector = (state: RootState) => state.refliction.itemRefliction;
export const itemSelector = (state: RootState) => state.refliction.reflictionItem;
export const isEditSelector = (state: RootState) => state.refliction.edit;
export const stautsEditSelector = (state: RootState) => state.refliction.statusEdit;
export const loadingTitleSelector = (state: RootState) => state.refliction.loadingTilte;
export const titleSelector = (state: RootState) => state.refliction.title;
export default reflicationSlice.reducer;
