import {createSlice} from '@reduxjs/toolkit';
import {getDeed, getMonthProgress, getIndicatiorsData} from '@api';
import {RootState} from '../index';

interface landingState {
  loadingGetDeed: boolean;
  loadingGetMonth: boolean;
  loading: boolean;
  indicatiorData: any;
  status: string;
  deed: {
    text: string;
  };
  monthProgress: any;
}

const initialState: landingState = {
  loadingGetDeed: false,
  loadingGetMonth: false,
  loading: false,
  status: '',
  indicatiorData: [],
  deed: {
    text: '',
  },
  monthProgress: undefined,
};

export const landingSlice = createSlice({
  name: 'landing',
  initialState,
  reducers: {
    setLoadingDeed: (state, action) => {
      state.loadingGetDeed = action.payload;
    },
    setLoadingMonth: (state, action) => {
      state.loadingGetMonth = action.payload;
    },
    setLoading: (state, action) => {
      state.loading = action.payload;
    },
    setIndicatior: (state, action) => {
      state.indicatiorData = action.payload;
    },
    setDeed: (state, action) => {
      state.deed = action.payload;
    },
    setMonthProgress: (state, action) => {
      state.monthProgress = action.payload;
    },
    setStatus: (state, action) => {
      state.status = action.payload;
    },
  },
});
export const {
  setLoadingDeed,
  setLoadingMonth,
  setDeed,
  setMonthProgress,
  setIndicatior,
  setLoading,
  setStatus,
} = landingSlice.actions;

export const GetDeed = (date: any) => {
  return async (dispatch: any) => {
    dispatch(setLoadingDeed(true));
    try {
      const result: any = await getDeed(date);
      dispatch(setLoadingDeed(false));
      dispatch(setDeed(result.deedOfTheDay));
    } catch (error) {
      dispatch(setLoadingDeed(false));
    }
  };
};
export const GetMonthProgress = () => {
  return async (dispatch: any) => {
    dispatch(setLoadingMonth(true));
    try {
      const result: any = await getMonthProgress();

      dispatch(setLoadingMonth(false));
      dispatch(setMonthProgress(result));
    } catch (error) {
      dispatch(setLoadingMonth(false));
    }
  };
};

export const GetDataIndicatior = (date: any): any => {
  return async (dispatch: any) => {
    dispatch(setLoading(true));
    try {
      const result: any = await getIndicatiorsData(date);
      dispatch(setLoading(false));
      dispatch(setIndicatior(result.data));
      dispatch(setStatus(result));
    } catch (error) {
      dispatch(setLoading(false));
    }
  };
};

export const loadingDeedSelector = (state: RootState) => state.landing.loadingGetDeed;
export const loadingMonthSelector = (state: RootState) => state.landing.loadingGetMonth;
export const loadingSelector = (state: RootState) => state.landing.loading;
export const deedSelector = (state: RootState) => state.landing.deed;
export const monthSelector = (state: RootState) => state.landing.monthProgress;
export const dataIndicatiorSelector = (state: RootState) => state.landing.indicatiorData;
export const statusSelector = (state: RootState) => state.landing.status;
export default landingSlice.reducer;
