import { applyMiddleware, createStore, combineReducers, compose } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import Thunk from 'redux-thunk';
import AsyncStorage from '@react-native-async-storage/async-storage';

import userReducer from './User/userSlice';
import profileReducer from './Profile/profileSlice';
import tidbitsReducer from './Tidbit/tidbitSlice';
import landingReducer from './landingData/landingSlice';
import duaaReducer from './Duaa/duaaSlice';
import reflictionReducer from './Reflictions/reflictionSlice';
import prayReducer from './Sections/praySlice';
import quranReducer from './Sections/quranSlice';
import dailyReducer from './Sections/dailySlice';
const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['user',],
};
const rootReducer = combineReducers({
  user: userReducer,
  profile: profileReducer,
  tidbit: tidbitsReducer,
  landing: landingReducer,
  duaa: duaaReducer,
  refliction: reflictionReducer,
  pray: prayReducer,
  quran: quranReducer,
  daily: dailyReducer,
});
export type RootState = ReturnType<typeof rootReducer>;
// const reactotron = require('../Config/reactotron').default;
// const reactotronMiddleware = reactotron.createEnhancer();
let persistedReducer = persistReducer(persistConfig, rootReducer);

const store = createStore(persistedReducer, compose(applyMiddleware(Thunk)));

const persistor = persistStore(store);

export { store, persistor };
