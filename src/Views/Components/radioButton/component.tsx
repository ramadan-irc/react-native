import React from 'react';
import {Radio, RadioGroup} from '@ui-kitten/components';
interface Props {
  children: any;
  onPress: () => void;
  value: string;
  checked?: boolean;
}

export const RadioButton: React.FC<Props> = ({children, onPress, value, checked}) => {
  const [selectedIndex, setSelectedIndex] = React.useState(0);
  return (
    <React.Fragment>
      <RadioGroup selectedIndex={selectedIndex} onChange={(index) => setSelectedIndex(index)}>
        <Radio onPress={onPress} key={value} checked={checked}>
          {children}
        </Radio>
      </RadioGroup>
    </React.Fragment>
  );
};
