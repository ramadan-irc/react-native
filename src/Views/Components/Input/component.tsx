import React from 'react';
import {Input} from '@ui-kitten/components';
import {TextStyle, ViewStyle} from 'react-native';
import {theme} from '@theme/style';
interface Props {
  children?: any;
  placeHolder?: any;
  placeHolderColor?: any;
  keyboard?: any;
  value: string;
  onChangeText: (value: string) => void;
  disabled?: boolean;
  multiline?: boolean;
  label?: React.ReactText;
  caption?: React.ReactText;
  iconRight?: (ImageProps: any) => React.ReactElement;
  iconLeft?: (ImageProps: any) => React.ReactElement;
  captionIcon?: (ImageProps: any) => React.ReactElement;
  Status?: 'basic' | 'primary' | 'success' | 'info' | 'warning' | 'danger' | 'control';
  onFocus?: () => void;
  onBlur?: () => void;
  onSubmit?: () => void;
  Size?: 'small' | 'medium' | 'large';
  StyleText?: TextStyle;
  secureText?: boolean;
  style?: ViewStyle;
  ref?: any;
  returnKeyType?: any;
  autoFocus?: boolean;
}

const getStatus: {[key: string]: string} = theme.statusInput;

const checkStatus = (size: string): string => {
  return getStatus[size] || 'basic';
};
const getSize: {[key: string]: string} = theme.sizeInput;

const checkSize = (size: string): string => {
  return getSize[size] || 'medium';
};
export const RnInput: React.FC<Props> = ({
  children,
  placeHolder,
  value,
  onChangeText,
  disabled,
  label,
  caption,
  iconRight,
  captionIcon,
  Status,
  onBlur,
  onFocus,
  Size,
  StyleText,
  secureText,
  style,
  onSubmit,
  multiline,
  iconLeft,
  placeHolderColor,
  keyboard,
  ref,
  autoFocus,
  returnKeyType,
}) => {
  return (
    <Input
      placeholder={placeHolder}
      children={children}
      value={value}
      onBlur={onBlur}
      onFocus={onFocus}
      onChangeText={onChangeText}
      disabled={disabled}
      label={label}
      ref={ref}
      autoFocus={autoFocus}
      keyboardType={keyboard}
      onSubmitEditing={onSubmit}
      size={checkSize(Size)}
      textStyle={{...StyleText}}
      style={{...style}}
      secureTextEntry={secureText}
      status={checkStatus(Status)}
      caption={caption}
      returnKeyType={returnKeyType}
      multiline={multiline}
      placeholderTextColor={placeHolderColor}
      captionIcon={captionIcon}
      accessoryRight={iconRight}
      accessoryLeft={iconLeft}
    />
  );
};
