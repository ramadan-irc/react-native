import React from 'react';
import {TouchableOpacity} from 'react-native';

interface TProps {
  activeOpacity?: number;
  children: React.ReactNode;
  onPress: () => void;
  disabled?: boolean;
}

export const Touchable: React.FC<TProps> = ({
  activeOpacity,
  children,
  disabled,
  onPress,
  ...rest
}) => {
  return (
    <TouchableOpacity activeOpacity={activeOpacity} onPress={onPress} disabled={disabled} {...rest}>
      {children}
    </TouchableOpacity>
  );
};
