import React from 'react';
import Toast from 'react-native-root-toast';
import FontType from '@theme/fonts';


const MyToast = (message: string) => {
  return (
    Toast.show(message, {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      textColor: '#FFFF',
      shadow: true,
      animation: true,
      textStyle: { fontFamily: FontType.Regular },
      hideOnPress: true,
      backgroundColor: '#656B8D',
      delay: 0,
      opacity: 1,
      containerStyle: {
        marginBottom: 18,
        borderRadius: 10,
      },
    })
  );
};
export default MyToast;