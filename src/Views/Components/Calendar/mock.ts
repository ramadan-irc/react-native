export const mock = [
  {
    key: 1,
    amount: 50,
    svg: {fill: 'transparent'},
  },
  {
    key: 2,
    amount: 50,
    svg: {fill: 'transparent'},
  },
  {
    key: 3,
    amount: 10,
    svg: {fill: 'transparent'},
  },
  {
    key: 5,
    amount: 10,
    svg: {fill: 'transparent'},
  },
  {
    key: 6,
    amount: 10,
    svg: {fill: 'transparent'},
  },
  {
    key: 7,
    amount: 10,
    svg: {fill: 'transparent'},
  },
  {
    key: 8,
    amount: 10,
    svg: {fill: 'transparent'},
  },
];
