const beforeToday = (day: any) => {
  return day.isBefore(new Date(), 'day');
};
const isToday = (day: any) => {
  return day.isSame(new Date(), 'day');
};
const isMonth = (day: any, value: any) => {
  return value.isSame(day, 'month');
};

const dayStyles = (day: any, value: any) => {
  if (beforeToday(day)) {
    return 'before';
  }
  if (isToday(day)) {
    return 'today';
  }

  if (isMonth(day, value)) {
    return 'beforeMonth';
  }

  return '';
};

export default dayStyles;
