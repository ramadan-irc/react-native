import {StyleSheet} from 'react-native';
import {width, wp} from '@theme/style';
import Fonts from '@theme/fonts';
var styles = StyleSheet.create({
  day: {
    position: 'absolute',
    width: width / 7,
    height: 44,
    backgroundColor: 'white',
    padding: 0,
    margin: 0,
    zIndex: 1,
    textAlign: 'center',
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingHorizontal: 10,
    paddingVertical: 20,
  },
  backIcon: {
    alignItems: 'center',
    width: 40,
    height: 40,
    marginRight: 10,
    backgroundColor: '#F2F2F2',
    borderRadius: 10,
  },
  callenderShape: {
    width: wp(75),
    height: 40,

    borderRadius: 20,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  backCalendarIcon: {
    alignItems: 'center',
    marginLeft: 10,
  },
  lineRead: {
    borderBottomColor: '#76AECC',
    borderBottomWidth: 1.5,
    width: width / 1.1,
    paddingTop: 5,
    marginHorizontal: 20,
  },
  text: {
    textAlign: 'center',
    fontFamily: Fonts.Bold,
    fontSize: 14,
  },
  text1: {
    position: 'absolute',
    left: width / 10.5,
    bottom: wp(10.5),
    fontFamily: Fonts.Bold,
    fontSize: 14,
  },
  ref: {
    bottom: 10,
    paddingRight: 5,
    fontSize: 10,
    paddingTop: 2,
    textAlign: 'center',
    textTransform: 'uppercase',
    fontFamily: Fonts.Bold,
    width: 63,
  },
});

export default styles;
