import React, { useState, useEffect, useCallback } from 'react';
import { View, Text as TextR, Platform, Dimensions } from 'react-native';
import { Icon } from '@ui-kitten/components';
import { PieChartComponent } from '../pieProgress';
import { BackIcon, CalendarIcon, HijriIcon } from '@constants';
import { dataIndicatiorSelector } from '@store/landingData/landingSlice';
import { useTheme } from '@theme/ThemeProvider';
import moment from 'moment';
import momentHijri from 'moment-hijri';
import { setCalendarType } from '@store/User/userAction';
import { callndarTypeSelector, adjTypeSelector } from '@store/User/userState';
import { useSelector, useDispatch } from 'react-redux';
import dayStyles from './StyleDays';
import { Text } from '../Text';
import { Touchable } from '../Touchable';
import { useTranslation } from 'react-i18next';
import FontType from '@theme/fonts';
import styles from './style';
import { wp } from '@theme/style';
import { buildCalendar, buildHijriCalendar } from './buildCalendar';
import { PieChart } from 'react-native-svg-charts';
import { mock } from './mock';
import { StackNavigationProp } from '@react-navigation/stack';
import { HomeParamsList } from 'src/Views/Navigation/paramsList';
import { CommonActions } from '@react-navigation/routers';
interface Props {
  onBack: () => void;
  navigation: StackNavigationProp<HomeParamsList, 'Calendar'>;
}
export const Calendar: React.FC<Props> = ({ onBack, navigation }) => {
  const dispatch = useDispatch();
  const [calendar, setCalendar] = useState([]);


  const { i18n } = useTranslation();
  const selectedLngCode = i18n.language;
  const typeCalendar = useSelector(callndarTypeSelector);
  const data = useSelector(dataIndicatiorSelector);
  const [value, setValue] = useState<any>(typeCalendar === 'Hijri' ? momentHijri() : moment());
  const adjType = useSelector(adjTypeSelector);
  const weekDay = moment.weekdaysShort();
  const { colors } = useTheme();
  const deviceWidth = Dimensions.get('window').width;

  useEffect(() => {
    typeCalendar === 'Gregorian' ?
      setCalendar(buildCalendar(moment())) :
      setCalendar(buildHijriCalendar(momentHijri()))
  }, [value]);
  const currMonthName = () => {
    return value.format('MMM');
  };
  const currMonthNameC = () => {
    return value.format('MMMM');
  };
  const nextMonthName = () => {
    return moment(value).add(1, 'months').format('MMM');
  };
  const prevMonth = () => {
    return value.clone().subtract(1, 'M');
  };
  const nextMonth = () => {
    return value.clone().add(1, 'M');
  };
  const currMonthNameHijri = () => {
    if (momentHijri(value).format('iMMMM') === 'Thul-Qi’dah') {
      return "Dhul-Qi'dah";
    } else if (momentHijri(value).format('iMMMM') === 'Thul-Hijjah') {
      return 'Dhul-Hijjah';
    } else if (momentHijri(value).format('iMMMM') === "Rabi' al-Thani") {
      return 'Rabi’ 2';
    } else if (momentHijri(value).format('iMMMM') === 'Jumada al-Ula') {
      return 'Jumad 1';
    } else if (momentHijri(value).format('iMMMM') === 'Jumada al-Alkhirah') {
      return 'Jumad 2';
    } else if (momentHijri(value).format('iMMMM') === "Rabi' al-Awwal") {
      return 'Rabi’ 1';
    } else {
      return momentHijri(value).format('iMMMM');
    }
  };
  const currMonthNameHijriC = () => {
    return momentHijri(value).format('iMMM');
  };

  const nextMonthNameHijriC = () => {
    return momentHijri(value).add(1, 'iMonth').format('iMMM');
  };
  const makeid = (length: any) => {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  };
  console.log(navigation)
  const renderCalendar = () => <>
    {calendar.map((week, weekk) => (
      <View
        key={weekk + 'week' + makeid(10)}
        style={{ flexDirection: 'row', marginRight: 15, marginTop: 10 }}>
        {week.map((Day: any, dayk: any) => {
          // console.log('dayk', typeCalendar === 'Hijri')
          if ((typeCalendar === 'Hijri' && Day.format('iMMM') === value.format('iMMM')) || (typeCalendar === 'Gregorian' && Day.format('MMMM') === value.format('MMMM'))) {
            return (
              <View
                key={makeid(8) + dayk}
                style={{
                  padding: 5,
                  flex: 1,
                  margin: 1,
                }}>
                <TextR
                  key={Day + 'id' + makeid(8)}
                  style={{
                    color:
                      dayStyles(Day, value) === 'before'
                        ? '#D3D3D3'
                        : dayStyles(Day, value) === 'today'
                          ? colors.daysbefore
                          : '#EAE8E8',
                    ...styles.ref,
                  }}>
                  {typeCalendar === 'Hijri'
                    ? Day.format('MMM D').toString()
                    : adjType === '0'
                      ? momentHijri(Day).format('iMMM iD').toString()
                      : adjType === '+1'
                        ? momentHijri(Day).subtract(1, 'd').format('iMMM iD').toString()
                        : momentHijri(Day).add(1, 'd').format('iMMM iD').toString()}
                </TextR>
                <PieChartComponent
                  data={data !== undefined ? data[Day.format('YYYY-MM-DD')] : null}
                  style={{ height: 48, width: 50, left: 3 }}
                />
                <View
                  style={{
                    position: 'absolute',
                    top: 15,
                    left: 0,
                    right: deviceWidth <= 360 ? 0 : 10,
                    bottom: 0,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <TextR
                    key={Day + 'id' + makeid(8)}
                    style={{
                      color:
                        dayStyles(Day, value) === 'before'
                          ? colors.daysbefore
                          : dayStyles(Day, value) === 'today'
                            ? colors.daysbefore
                            : '#EAE8E8',
                      ...styles.text,
                      paddingLeft: deviceWidth >= 540 ? 4 : wp(5),
                    }}>
                    {typeCalendar === 'Gregorian'
                      ? Day.format('D').toString()
                      : adjType === '0'
                        ? momentHijri(Day).format('iD').toString()
                        : adjType === '+1'
                          ? momentHijri(Day).subtract(1, 'd').format('iD').toString()
                          : momentHijri(Day).add(1, 'd').format('iD').toString()}
                  </TextR>
                </View>
              </View>
            );
          } else {
            return (
              <View
                key={makeid(8) + dayk}
                style={{
                  padding: 5,
                  flex: 1,
                  margin: 1,
                }}>
                <TextR
                  key={Day + 'id' + makeid(8)}
                  style={{
                    color: 'transparent',
                    ...styles.ref,
                  }}>
                  {typeCalendar === 'Hijri'
                    ? Day.format('MMM D').toString()
                    : adjType === '0'
                      ? momentHijri(Day).format('iMMM iD').toString()
                      : adjType === '+1'
                        ? momentHijri(Day).subtract(1, 'd').format('iMMM iD').toString()
                        : momentHijri(Day).add(1, 'd').format('iMMM iD').toString()}
                </TextR>
                <View style={{ justifyContent: 'space-around', flex: 1 }}>
                  <PieChart
                    style={{ height: 48, width: 50, left: 3 }}
                    data={mock}
                    innerRadius={'55%'}
                    valueAccessor={({ item }: any) => item.amount}
                  />
                </View>
                <TextR
                  key={Day + 'id' + makeid(8)}
                  style={{
                    color:
                      dayStyles(Day, value) === 'before'
                        ? 'transparent'
                        : dayStyles(Day, value) === 'today'
                          ? 'transparent'
                          : 'transparent',
                    paddingLeft: Number(Day.format('D')) >= 10 ? 4 : 5,
                    ...styles.text,
                  }}>
                  {typeCalendar === 'Gregorian'
                    ? Day.format('D').toString()
                    : adjType === '0'
                      ? momentHijri(Day).format('iD').toString()
                      : adjType === '+1'
                        ? momentHijri(Day).subtract(1, 'd').format('iD').toString()
                        : momentHijri(Day).add(1, 'd').format('iD').toString()}
                </TextR>
              </View>
            );
          }
        })}
      </View>
    ))}
  </>

  const navigateAgain = () => {
    navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [
          { name: 'Home' },
          { name: 'Calendar' },

        ],
      })
    );
  }
  return (
    <View>
      <View style={styles.headerContainer}>
        <Touchable onPress={onBack}>
          <View style={styles.backIcon}>
            <BackIcon height={25} width={25} style={{ top: 8, right: 2 }} />
          </View>
        </Touchable>
        <View style={{ ...styles.callenderShape, backgroundColor: colors.barCalendar }}>
          <Touchable onPress={() => setValue(prevMonth())}>
            <Icon style={{ width: 24, height: 24 }} fill={colors.arrowIcon} name="arrow-left" />
          </Touchable>
          <Text
            size="xl"
            style={{
              textAlign: 'center',
              color: colors.calendarText,
              fontFamily: FontType.Lie,

              textTransform: 'capitalize',
            }}>
            {typeCalendar === 'Gregorian' ? currMonthNameC() : currMonthNameHijri()}
          </Text>

          <Text
            size="m"
            style={{
              color: colors.barC,
              fontFamily: FontType.Medium,
              textTransform: 'uppercase',
              left: 15,
            }}>
            {typeCalendar === 'Gregorian' ? currMonthNameHijriC() : currMonthName()}
          </Text>
          <Text size="m" style={{ color: colors.barC, fontFamily: FontType.Medium, left: 10 }}>
            /
          </Text>
          <Text
            size="m"
            style={{
              color: colors.barC,
              fontFamily: FontType.Medium,
              textTransform: 'uppercase',
              left: 5,
            }}>
            {typeCalendar === 'Gregorian' ? nextMonthNameHijriC() : nextMonthName()}
          </Text>

          <Touchable onPress={() => setValue(nextMonth())}>
            <Icon style={{ width: 24, height: 24 }} fill={colors.arrowIcon} name="arrow-right" />
          </Touchable>
        </View>
        {typeCalendar === 'Gregorian' ? (
          <Touchable onPress={() => {
            dispatch(setCalendarType('Hijri'))
            navigateAgain()
          }}>
            <View style={styles.backCalendarIcon}>
              <HijriIcon width={28} height={28} style={{ top: 7 }} />
              <Text
                size="xs"
                style={{
                  color: '#656B8D',
                  fontFamily: FontType.Bold,
                  paddingTop: Platform.OS === 'ios' ? 12 : 7,
                }}>
                Hijri
              </Text>
            </View>
          </Touchable>
        ) : (
          <Touchable onPress={() => {
            dispatch(setCalendarType('Gregorian'))
            navigateAgain()
          }}>
            <View style={styles.backCalendarIcon}>
              <CalendarIcon width={28} height={28} style={{ top: 7 }} />
              <Text
                size="xs"
                style={{
                  color: '#656B8D',
                  fontFamily: FontType.Bold,
                  paddingTop: Platform.OS === 'ios' ? 12 : 7,
                }}>
                {selectedLngCode === 'en' ? 'Gregorian' : 'Grégorien'}
              </Text>
            </View>
          </Touchable>
        )}
      </View>
      <View
        style={{
          flexDirection: 'row',
          paddingHorizontal: 3,
        }}>
        {weekDay.map((day, key) => {
          return (
            <View style={{ flex: 1 }} key={key}>
              <Text
                style={{
                  fontFamily: FontType.Bold,
                  color: '#75AECC',
                  textAlign: 'center',
                  textTransform: 'uppercase',
                }}
                size="s"
                key={day + 'daa' + makeid(5)}>
                {selectedLngCode === 'fr' ? day.substring(0, day.length - 1) : day}
              </Text>
            </View>
          );
        })}
      </View>
      <View style={styles.lineRead} />

      {renderCalendar()}
    </View>
  );
};
