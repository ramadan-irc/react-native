import {StyleSheet} from 'react-native';

var styles = StyleSheet.create({
  item: {
    paddingVertical: 10,
    paddingHorizontal: 28,
  },
  selectedItem: {
    width: 65,
    height: 65,
    top: 8,
    marginHorizontal: 20,
    alignItems: 'center',
    borderRadius: 90,
  },
});

export default styles;
