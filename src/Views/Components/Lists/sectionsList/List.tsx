import React from 'react';
import {StyleSheet, FlatList} from 'react-native';
import sections from '../../../../Mock/sections';
import RenderItem from './renderItem';
import {sectionSelector} from '@store/Sections/praySlice';
import {useSelector} from 'react-redux';
export const SectionsList: React.FC = () => {
  const currentSection = useSelector(sectionSelector);
  const Sectionss = ({item}: any) => {
    return (
      <RenderItem
        item={item}
        selected={
          currentSection === '1'
            ? item.id === 1
            : (currentSection && currentSection.id === item.id) || currentSection === item.id
        }
      />
    );
  };
  return (
    <FlatList
      showsHorizontalScrollIndicator={false}
      data={sections}
      numColumns={3}
      removeClippedSubviews={true}
      contentContainerStyle={styles.row}
      keyExtractor={(item) => item.id.toString()}
      renderItem={Sectionss}
    />
  );
};
const styles = StyleSheet.create({
  row: {
    alignItems: 'center',
    paddingVertical: 28,
  },
});
