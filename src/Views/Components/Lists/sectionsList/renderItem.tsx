import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import styles from './style';
import { SvgUri } from 'react-native-svg';
import { useTheme } from '@theme/ThemeProvider';
import { setSection } from '@store/Sections/praySlice';
import { useDispatch } from 'react-redux';
const ItemCat = ({ item, selected }: any) => {
  const dispatch = useDispatch();
  const { colors } = useTheme();
  const _SelectSection = (section: any) => {
    dispatch(
      setSection({
        section: section,
      }),
    );
  };

  return (
    <TouchableOpacity onPress={() => _SelectSection(item)}>
      <View
        style={[
          selected
            ? { ...styles.selectedItem, backgroundColor: colors.section }
            : { ...styles.item, backgroundColor: colors.section, borderColor: colors.borderSection },
        ]}>
        <SvgUri width="60" height="60" uri={item?.image} />
      </View>
    </TouchableOpacity>
  );
};
export default ItemCat;
