import React from 'react';
import {View} from 'react-native';
import {Text} from '@components/Text';
import {FavFill} from '@constants';
import {useDispatch} from 'react-redux';
import {popDuaaFav} from '@store/Duaa/duaaSlice';
import {useTheme} from '@theme/ThemeProvider';
import styles from './style';
import {useTranslation} from 'react-i18next';
interface Props {
  item: any;
  index: any;
}
export const RenderItem: React.FC<Props> = ({item, index}: any) => {
  const dispatch = useDispatch();
  const {i18n} = useTranslation();
  const selectedLngCode = i18n.language;
  const {colors} = useTheme();
  const _fetchRemove = (id: any) => {
    dispatch(popDuaaFav(id));
  };
  return (
    <View style={styles.itemsContanier} key={index}>
      <Text style={{...styles.arText, color: colors.duaaAr, textAlign: 'center'}} size="m">
        {item?.textArabic}
      </Text>
      <Text style={{...styles.enArText, color: colors.duaaMid, textAlign: 'center'}} size="m">
        {item?.textInbetween}
      </Text>
      <Text style={{...styles.enArText, color: colors.duaaEn, textAlign: 'center'}} size="s">
        {selectedLngCode === 'en' ? item?.textEnglish : item?.textFrench}
      </Text>
      <View style={{alignSelf: 'flex-end', paddingVertical: 10, paddingRight: 10}}>
        <FavFill width={25} height={25} onPress={() => _fetchRemove(item.id)} />
      </View>
      <View style={styles.Line} />
    </View>
  );
};
