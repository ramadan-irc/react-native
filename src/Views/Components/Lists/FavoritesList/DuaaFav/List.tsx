import React from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import {RenderItem} from './renderItem';
import Styles from './style';
import {Text} from '@components';
import {useSelector} from 'react-redux';
import {duaasFavSelector} from '@store/Duaa/duaaSlice';
import {useTranslation} from 'react-i18next';
import '@i18n';

interface Props {
  duaas: any;
}

export const DuasListFav: React.FC<Props> = ({duaas}) => {
  const favDuaa = useSelector(duaasFavSelector);
  const {t} = useTranslation();
  const Items = ({item, index}: any) => {
    return <RenderItem item={item} index={index} />;
  };
  const renderEmpty = () => {
    return favDuaa.length === 0 ? (
      <View style={{justifyContent: 'center', alignItems: 'center', paddingTop: 20}}>
        <Text size="l" style={Styles.textEmpty}>
          {t('homePage:nFD')}
        </Text>
      </View>
    ) : null;
  };
  return (
    <>
      <FlatList
        showsHorizontalScrollIndicator={false}
        data={duaas}
        removeClippedSubviews={true}
        contentContainerStyle={styles.row}
        keyExtractor={(item) => item.id.toString()}
        ListEmptyComponent={renderEmpty}
        renderItem={Items}
      />
    </>
  );
};
const styles = StyleSheet.create({
  row: {
    alignItems: 'center',
    paddingTop: 20,
  },
});
