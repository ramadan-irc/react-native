import {StyleSheet} from 'react-native';
import {width, height} from '@theme/style';
import FontType from '@theme/fonts';

var styles = StyleSheet.create({
  text: {
    fontFamily: FontType.Lie,
    color: '#74AFCD',
  },
  itemsContanier: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  arText: {
    paddingBottom: 10,
    fontFamily: FontType.RegularArabic,
  },
  enArText: {
    paddingBottom: 20,
    fontFamily: FontType.Medium,
  },
  Line: {
    borderBottomColor: '#E3E7EF',
    borderBottomWidth: 1,
    alignSelf: 'center',
    width: width / 1.1,
  },
  loader: {
    position: 'absolute',
    height: height,
    backgroundColor: 'transparent',
    width: width,
    flex: 1,
  },
  loaderContainer: {
    flex: 1,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textEmpty: {
    fontFamily: FontType.Bold,
    color: '#656B8D',
    textAlign: 'center',
    paddingStart: 15,
    paddingEnd: 10,
  },
});

export default styles;
