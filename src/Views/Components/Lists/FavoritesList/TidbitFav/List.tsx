import React from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import RenderItem from './renderItem';
import {Text} from '@components';
import {useSelector} from 'react-redux';
import {tidbitsFavSelector} from '@store/Tidbit/tidbitSlice';
import Styles from './style';
import {useTranslation} from 'react-i18next';
import '@i18n';

interface Props {
  data: any;
}
export const TidbitFavList: React.FC<Props> = ({data}) => {
  const favtidbit = useSelector(tidbitsFavSelector);
  const {t} = useTranslation();
  const Items = ({item, index}: any) => {
    return <RenderItem item={item} index={index} />;
  };
  const renderEmpty = () => {
    return favtidbit.length === 0 ? (
      <View style={{justifyContent: 'center', alignItems: 'center', paddingTop: 20}}>
        <Text size="l" style={Styles.textEmpty}>
          {t('homePage:nFT')}
        </Text>
      </View>
    ) : null;
  };
  return (
    <>
      <FlatList
        showsHorizontalScrollIndicator={false}
        data={data}
        ListEmptyComponent={renderEmpty}
        removeClippedSubviews={true}
        contentContainerStyle={styles.row}
        keyExtractor={(item) => item.id.toString()}
        renderItem={Items}
      />
    </>
  );
};
const styles = StyleSheet.create({
  row: {
    alignItems: 'center',
    paddingTop: 20,
  },
});
