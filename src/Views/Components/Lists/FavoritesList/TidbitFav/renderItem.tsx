import React from 'react';
import {View} from 'react-native';
import {Text, Touchable} from '@components';
import {FavFill} from '@constants';
import styles from './style';
import {useDispatch} from 'react-redux';
import {useTranslation} from 'react-i18next';
import {useTheme} from '@theme/ThemeProvider';
import {popTidbitFav} from '@store/Tidbit/tidbitSlice';

interface Props {
  item: any;
  index: any;
}
const RenderItem: React.FC<Props> = ({item, index}) => {
  const dispatch = useDispatch();
  const {colors} = useTheme();
  const {i18n} = useTranslation();
  const [visible, setVisible] = React.useState(false);
  const selectedLngCode = i18n.language;
  const _fetchRemove = (id: any) => {
    dispatch(popTidbitFav(id));
  };
  return (
    <>
      {!visible ? (
        <Touchable onPress={() => setVisible(!visible)}>
          <View style={styles.Contanier} key={index}>
            <View>
              <Text style={{...styles.enArText, color: colors.tidbitT, textAlign: 'left'}} size="m">
                {selectedLngCode === 'en'
                  ? item?.textEnglish.substring(0, 70) + '...'
                  : item?.textFrench.substring(0, 70) + '...'}
              </Text>
            </View>
            <FavFill
              width={25}
              height={25}
              style={{marginRight: 10}}
              onPress={() => _fetchRemove(item.id)}
            />
          </View>
        </Touchable>
      ) : (
        <Touchable onPress={() => setVisible(!visible)}>
          <View style={styles.Contanier} key={index}>
            <View>
              <Text style={{...styles.enArText, color: colors.tidbitT, textAlign: 'left'}} size="m">
                {selectedLngCode === 'en' ? item?.textEnglish : item?.textFrench}
              </Text>
            </View>
            <FavFill
              width={25}
              height={25}
              style={{marginRight: 10}}
              onPress={() => _fetchRemove(item.id)}
            />
          </View>
        </Touchable>
      )}

      <View style={styles.Line} />
    </>
  );
};
export default RenderItem;
