import {StyleSheet} from 'react-native';
import {width, wp, height} from '@theme/style';
import FontType from '@theme/fonts';

var styles = StyleSheet.create({
  Contanier: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 20,
  },
  enArText: {
    fontFamily: FontType.Medium,
    width: wp(80),
  },
  Line: {
    borderBottomColor: '#E3E7EF',
    borderBottomWidth: 1,
    alignSelf: 'center',
    width: width / 1.1,
  },
  loader: {
    position: 'absolute',
    height: height,
    backgroundColor: 'transparent',
    width: width,
    flex: 1,
  },
  loaderContainer: {
    flex: 1,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textEmpty: {
    fontFamily: FontType.Bold,
    color: '#656B8D',
    textAlign: 'center',
    paddingStart: 15,
    paddingEnd: 10,
  },
});

export default styles;
