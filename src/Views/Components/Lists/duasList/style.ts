import {StyleSheet} from 'react-native';
import {width, height} from '@theme/style';
import FontType from '@theme/fonts';

var styles = StyleSheet.create({
  listHeader: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingVertical: 20,
    alignItems: 'center',
  },
  text: {
    color: '#74AFCD',
    paddingRight: 5,
    top: 2,
  },
  text1: {
    color: '#74AFCD',
    paddingRight: 10,
    paddingLeft: 5,
  },
  itemsContanier: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  arText: {
    paddingBottom: 10,
    fontFamily: FontType.RegularArabic,
  },
  enText: {
    paddingBottom: 20,
    fontFamily: FontType.Medium,
  },
  enArText: {
    paddingBottom: 20,
    fontFamily: FontType.Regular,
  },
  Line: {
    borderBottomColor: '#E3E7EF',
    borderBottomWidth: 1,
    alignSelf: 'center',
    width: width / 1.1,
  },
  loader: {
    position: 'absolute',
    height: height,
    backgroundColor: 'transparent',
    width: width,
    flex: 1,
  },
  loaderContainer: {
    flex: 1,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default styles;
