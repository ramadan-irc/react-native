import React from 'react';
import {View} from 'react-native';
import {Text} from '@components/Text';
import {StarsIcon, FlibStars} from '@constants';
import styles from './style';
import {useTranslation} from 'react-i18next';
import FontType from '@theme/fonts';

interface Props {}
const ListHeader: React.FC<Props> = () => {
  const {t, i18n} = useTranslation();
  const selectedLngCode = i18n.language;
  return (
    <View style={styles.listHeader}>
      <StarsIcon width={80} height={80} />
      <Text
        style={{
          ...styles.text1,
          fontFamily: selectedLngCode === 'en' ? FontType.Lie : FontType.Bold,
        }}
        size="xl">
        {selectedLngCode === 'en' ? t('homePage:daily') : t('homePage:duaa')}
      </Text>
      <Text
        style={{
          ...styles.text,
          fontFamily: selectedLngCode === 'en' ? FontType.Bold : FontType.Lie,
        }}
        size="l">
        {selectedLngCode === 'en' ? t('homePage:duaa') : t('homePage:daily')}
      </Text>
      <FlibStars width={80} height={80} />
    </View>
  );
};
export default ListHeader;
