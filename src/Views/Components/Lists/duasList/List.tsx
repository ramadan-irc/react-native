import React from 'react';
import {FlatList, StyleSheet} from 'react-native';
import ListHeader from './header';
import {RenderItem} from './renderItem';
interface Props {
  data: any;
}

export const DuasList: React.FC<Props> = ({data}) => {
  const Items = ({item, index}: any) => {
    return <RenderItem item={item} index={index} />;
  };
  const RenderHeader = () => {
    return <ListHeader />;
  };
  return (
    <>
      <FlatList
        showsHorizontalScrollIndicator={false}
        data={data}
        removeClippedSubviews={true}
        ListHeaderComponent={RenderHeader}
        contentContainerStyle={styles.row}
        keyExtractor={(item) => item.id.toString()}
        renderItem={Items}
      />
    </>
  );
};
const styles = StyleSheet.create({
  row: {
    alignItems: 'center',
  },
});
