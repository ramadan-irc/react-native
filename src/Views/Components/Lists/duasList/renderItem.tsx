import React from 'react';
import {View} from 'react-native';
import {Text} from '@components/Text';
import {FavFill, FavUnFill} from '@constants';
// import R from 'reactotron-react-native';
import {useTheme} from '@theme/ThemeProvider';
import {useTranslation} from 'react-i18next';
import {useDispatch} from 'react-redux';
import styles from './style';
import {addDuaaFav, removeDuaaFav} from '@store/Duaa/duaaSlice';
interface Props {
  item: any;
  index: any;
}
export const RenderItem: React.FC<Props> = ({item, index}: any) => {
  const dispatch = useDispatch();
  const {i18n} = useTranslation();
  const selectedLngCode = i18n.language;
  const {colors} = useTheme();
  const _fetchAdd = (id: any) => {
    dispatch(addDuaaFav(id));
  };
  const _fetchRemove = (id: any) => {
    dispatch(removeDuaaFav(id));
  };
  return (
    <View style={styles.itemsContanier} key={index}>
      <Text style={{...styles.arText, color: colors.duaaAr, textAlign: 'center'}} size="m">
        {item?.textArabic}
      </Text>
      <Text style={{...styles.enArText, color: colors.duaaMid, textAlign: 'center'}} size="m">
        {item?.textInbetween}
      </Text>
      <Text style={{...styles.enText, color: colors.duaaEn, textAlign: 'center'}} size="s">
        {selectedLngCode === 'en' ? item?.textEnglish : item?.textFrench}
      </Text>
      <View style={{alignSelf: 'flex-end', paddingVertical: 10, paddingRight: 10}}>
        {!item.isFavorite ? (
          <FavUnFill width={25} height={25} style={{right: 5}} onPress={() => _fetchAdd(item.id)} />
        ) : (
          <FavFill
            width={25}
            height={25}
            style={{right: 5}}
            onPress={() => _fetchRemove(item.id)}
          />
        )}
      </View>
      <View style={styles.Line} />
    </View>
  );
};
