import {StyleSheet} from 'react-native';
import {width} from '@theme/style';
import FontType from '@theme/fonts';

var styles = StyleSheet.create({
  Contanier: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
  },

  arText: {
    paddingBottom: 10,
    fontFamily: FontType.Regular,
  },
  enArText: {
    paddingBottom: 6,
    fontFamily: FontType.Medium,
  },
  Line: {
    borderBottomColor: '#E3E7EF',
    borderBottomWidth: 1,
    alignSelf: 'center',
    width: width / 1.1,
  },
});

export default styles;
