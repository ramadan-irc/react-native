import React, {useCallback} from 'react';
import {View} from 'react-native';
import {Text} from '@components/Text';
import {ArrowRef} from '@constants';
import {Touchable} from '@components/Touchable';
import styles from './style';
import {setSelectedItem, setRefliction} from '@store/Reflictions/reflictionSlice';
import {useDispatch} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import {useTheme} from '@theme/ThemeProvider';
interface Props {
  item: any;
  index: any;
}
const RenderItem: React.FC<Props> = ({item, index}) => {
  const dispatch = useDispatch();
  const {colors} = useTheme();
  const navigation = useNavigation();
  const _onPressItem = useCallback(
    async (ref: any) => {
      dispatch(setSelectedItem(ref));
      await dispatch(setRefliction(ref?.id));
      navigation.navigate('Deatails');
    },
    [dispatch, navigation],
  );
  return (
    <>
      <Touchable onPress={() => _onPressItem(item)}>
        <View style={styles.Contanier} key={index}>
          <View>
            <Text style={{...styles.arText, color: colors.duaaAr, textAlign: 'left'}} size="m">
              {item?.date}
            </Text>
            <Text style={{...styles.enArText, color: colors.duaaMid, textAlign: 'left'}} size="m">
              {item?.preview.substring(0, 15) + '...'}
            </Text>
          </View>
          <ArrowRef width={15} height={15} style={{marginRight: 10}} />
        </View>
      </Touchable>
      <View style={styles.Line} />
    </>
  );
};
export default RenderItem;
