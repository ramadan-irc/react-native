import React, {useCallback} from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {useTheme} from '@theme/ThemeProvider';
import Styles from './style';
import {CheckFardList, fardListSelector, dateSelector} from '@store/Sections/praySlice';
import {useSelector, useDispatch} from 'react-redux';
import {Text} from '@components';
import FontType from '@theme/fonts';
import debounce from 'lodash.debounce';
import tron from 'reactotron-react-native';
interface Props {}
export const PraysList: React.FC<Props> = () => {
  const {colors} = useTheme();
  const value = useSelector(dateSelector);
  const dispatch = useDispatch();
  const fardList = useSelector(fardListSelector);
  const [list, setList] = React.useState([]);
  let lists: any = [];
  const debouncedTrue = useCallback(
    debounce((listss: any, items: any) => {
      tron.logImportant('dsds', listss);
      dispatch(CheckFardList(listss, value.format(), items?.type));
    }, 2000),

    [],
  );

  React.useEffect(() => {
    setList(fardList);
  }, [fardList]);

  const _fetchCheck = (item: any) => {
    let newList = [...list];
    let index = newList.findIndex((i: any) => i.id === item.id);
    newList[index] = {...newList[index], selected: !item?.selected};
    setList(newList);
    lists = newList;
    debouncedTrue(lists, item);
  };

  return (
    <View style={styles.row}>
      {list?.map((item: any, key: any) => {
        return (
          <TouchableOpacity key={key} onPress={() => _fetchCheck(item)}>
            <View
              style={[
                Styles.item,
                item?.selected
                  ? {backgroundColor: '#DFBE84'}
                  : {
                      backgroundColor: 'transparent',
                      borderRadius: 14,
                      borderColor: '#DFBE84',
                      borderWidth: 1,
                    },
              ]}
              key={key}>
              <Text
                size="xs"
                style={{
                  color: item?.selected ? colors.textSelctedpray : '#DFBE84',
                  fontFamily: FontType.Bold,
                }}>
                {item?.name}
              </Text>
            </View>
          </TouchableOpacity>
        );
      })}
    </View>
  );
};
const styles = StyleSheet.create({
  row: {
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection: 'row',
    paddingBottom: 20,
  },
});
