import React, { useCallback } from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { Text, RnSpinner } from '@components';
import { SvgCssUri } from 'react-native-svg';
import { dateSelector } from '@store/Sections/praySlice';
import { useTranslation } from 'react-i18next';
import Styles from './style';
import { loadingGetTasksSelector, requestloadingGetTasksSelector, CheckTask, tasksListSelector } from '@store/Sections/dailySlice';
import debounce from 'lodash.debounce';
import tron from 'reactotron-react-native';

export const DailyList: React.FC = () => {
  const dispatch = useDispatch();
  const value = useSelector(dateSelector);
  const loading = useSelector(loadingGetTasksSelector);
  const requestloading = useSelector(requestloadingGetTasksSelector);
  const tasks = useSelector(tasksListSelector);
  const { i18n } = useTranslation();
  const selectedLngCode = i18n.language;
  const [list, setList] = React.useState([]);
  let lists: any = [];
  const debouncedTrue = useCallback(
    debounce((listss: any) => {
      dispatch(CheckTask(listss, value.format()));
    }, 2000),

    [],
  );
  React.useEffect(() => {
    setList(tasks);
  }, [tasks]);
  const _fetchCheck = (item: any) => {
    let newList = [...list];
    let index = newList.findIndex((i: any) => i.id === item.id);
    newList[index] = { ...newList[index], value: !item?.value };
    setList(newList);
    lists = newList;
    debouncedTrue(lists);
  };

  return !loading ? (
    <>
      <View style={styles.firstRow}>
        {requestloading && <View style={{
          ...Styles.indicatorButton,
          position: 'absolute',
          right: '50%',
          top: -10

        }}>
          <RnSpinner size="xl" color="#75AECC" />
        </View>}
        {list?.slice(0, 4).map((item: any, key: any) => {
          tron.logImportant('hay', item);
          return (
            <TouchableOpacity onPress={() => _fetchCheck(item)} key={key + '2'}>
              <View
                key={key + '1'}
                style={[
                  Styles.item,
                  item?.value === true
                    ? { backgroundColor: '#75AECC' }
                    : {
                      backgroundColor: 'transparent',
                      borderRadius: 14,
                      borderColor: '#75AECC',
                      borderWidth: 1,
                    },
                ]}>
                <SvgCssUri
                  width="90"
                  height="90"
                  key={key + '5'}
                  uri={item?.value === true ? item?.selectedIcon : item?.notSelectedIcon}
                />
              </View>
              <Text size="xs" style={Styles.text} key={key + '9'}>
                {selectedLngCode === 'en' ? item?.name : item?.nameFrench}
              </Text>
            </TouchableOpacity>
          );
        })}
      </View>
      <View style={styles.firstRow}>
        {list?.slice(4, 8).map((item: any, index: any) => {
          return (
            <TouchableOpacity onPress={() => _fetchCheck(item)} key={index + '22'}>
              <View
                key={index + '99'}
                style={[
                  Styles.item,
                  item?.value === true
                    ? { backgroundColor: '#75AECC' }
                    : {
                      backgroundColor: 'transparent',
                      borderRadius: 14,
                      borderColor: '#75AECC',
                      borderWidth: 1,
                    },
                ]}>
                <SvgCssUri
                  width="90"
                  height="90"
                  key={index + '98549'}
                  uri={item?.value === true ? item?.selectedIcon : item?.notSelectedIcon}
                />
              </View>
              <Text size="xs" style={Styles.text} key={index + '985dsdsds49'}>
                {selectedLngCode === 'en' ? item?.name : item?.nameFrench}
              </Text>
            </TouchableOpacity>
          );
        })}
      </View>
    </>
  ) : (
    <View style={[Styles.indicatorButton]}>
      <RnSpinner size="xl" color="#75AECC" />
    </View>
  );
};
const styles = StyleSheet.create({
  firstRow: {
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection: 'row',
    paddingBottom: 24,
    marginHorizontal: 20,
  },
  seceondRow: {
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection: 'row',
    paddingHorizontal: 20,
  },
});
