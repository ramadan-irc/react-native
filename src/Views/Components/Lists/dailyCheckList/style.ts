import {StyleSheet} from 'react-native';
import FontType from '@theme/fonts';
import {wp, height, width} from '@theme/style';
var styles = StyleSheet.create({
  item: {
    width: 65,
    height: 65,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 19,
    marginHorizontal: 10,
    marginVertical: 15,
  },
  selectedItem: {
    width: 65,
    height: 65,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 19,
    marginHorizontal: 10,
  },
  indicatorButton: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    width: wp(21),
    textAlign: 'center',
    color: '#75AECC',
    position: 'absolute',
    bottom: -16,
    fontFamily: FontType.Medium,
  },
  loader: {
    position: 'absolute',
    height: height,
    backgroundColor: 'transparent',
    width: width,
    flex: 1,
  },
  loaderContainer: {
    flex: 1,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default styles;
