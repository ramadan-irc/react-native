import React from 'react';
import {FlatList, StyleSheet} from 'react-native';
import {RenderItem} from './renderItem';

interface Props {
  data: any;
}
export const TidbitList: React.FC<Props> = ({data}) => {
  const Items = ({item, index}: any) => {
    return <RenderItem item={item} index={index} />;
  };
  return (
    <>
      <FlatList
        showsHorizontalScrollIndicator={false}
        data={data}
        removeClippedSubviews={true}
        contentContainerStyle={styles.row}
        keyExtractor={(item) => item.id.toString()}
        renderItem={Items}
      />
    </>
  );
};
const styles = StyleSheet.create({
  row: {
    alignItems: 'center',
  },
});
