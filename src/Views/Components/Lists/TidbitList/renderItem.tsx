import React from 'react';
import {View} from 'react-native';
import {Text, Touchable} from '@components';
import {FavFill, FavUnFill} from '@constants';
import styles from './style';
import {useTranslation} from 'react-i18next';
import {useDispatch} from 'react-redux';
import {removeTidbitFav, addTidbitFav} from '@store/Tidbit/tidbitSlice';

interface Props {
  item: any;
  index: any;
}

export const RenderItem: React.FC<Props> = ({item, index}) => {
  const dispatch = useDispatch();
  const {i18n} = useTranslation();
  const [visible, setVisible] = React.useState(false);
  const selectedLngCode = i18n.language;
  const _fetchAdd = (id: any) => {
    dispatch(addTidbitFav(id));
  };
  const _fetchRemove = (id: any) => {
    dispatch(removeTidbitFav(id));
  };
  return (
    <View style={{justifyContent: 'center', alignItems: 'center'}}>
      {!visible ? (
        <Touchable onPress={() => setVisible(!visible)}>
          <View style={styles.rectangle} key={index}>
            <Text size="s" style={styles.Text}>
              {selectedLngCode === 'en'
                ? item?.textEnglish.substring(0, 80) + '...'
                : item?.textFrench.substring(0, 80) + '...'}
            </Text>
            {!item?.isFavorite ? (
              <FavUnFill
                width={30}
                height={30}
                style={{right: 5}}
                onPress={() => _fetchAdd(item.id)}
              />
            ) : (
              <FavFill
                width={30}
                height={30}
                style={{right: 5}}
                onPress={() => _fetchRemove(item.id)}
              />
            )}
          </View>
        </Touchable>
      ) : (
        <Touchable onPress={() => setVisible(!visible)}>
          <View style={styles.rectangleExtended} key={index}>
            <Text size="s" style={styles.Text}>
              {selectedLngCode === 'en' ? item?.textEnglish : item?.textFrench}
            </Text>
            {!item.isFavorite ? (
              <FavUnFill
                width={25}
                height={25}
                style={{right: 5}}
                onPress={() => _fetchAdd(item.id)}
              />
            ) : (
              <FavFill
                width={25}
                height={25}
                style={{right: 5}}
                onPress={() => _fetchRemove(item.id)}
              />
            )}
          </View>
        </Touchable>
      )}
    </View>
  );
};
