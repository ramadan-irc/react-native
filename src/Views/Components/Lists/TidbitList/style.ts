import {StyleSheet} from 'react-native';
import {wp, height, width, hp} from '@theme/style';
import FontType from '@theme/fonts';
var styles = StyleSheet.create({
  rectangle: {
    flexDirection: 'row',
    marginVertical: 15,
    alignItems: 'center',
    justifyContent: 'space-around',
    width: wp(87),
    height: 65,
    borderRadius: 20,
    backgroundColor: '#FFFFFF',
  },
  rectangleExtended: {
    flexDirection: 'row',
    marginVertical: 15,
    alignItems: 'center',
    justifyContent: 'space-around',
    width: wp(87),
    height: hp(20),
    borderRadius: 20,
    backgroundColor: '#FFFFFF',
  },
  Text: {
    color: '#656B8D',
    textAlign: 'left',
    fontFamily: FontType.Medium,
    width: wp(75),

    paddingLeft: wp(4),
  },
  loader: {
    position: 'absolute',
    height: height,
    backgroundColor: 'transparent',
    width: width,
    flex: 1,
  },
  loaderContainer: {
    flex: 1,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default styles;
