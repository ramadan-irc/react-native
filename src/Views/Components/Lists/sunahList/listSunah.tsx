import React, {useCallback} from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {Text} from '@components';
import Styles from './style';
import FontType from '@theme/fonts';
import {useTheme} from '@theme/ThemeProvider';
import {CheckFardList, sunnahListSelector, dateSelector} from '@store/Sections/praySlice';
import debounce from 'lodash.debounce';
import {useDispatch, useSelector} from 'react-redux';
interface Props {}
export const SunahList: React.FC<Props> = () => {
  const {colors} = useTheme();
  const dispatch = useDispatch();
  const value = useSelector(dateSelector);
  const sunnaList = useSelector(sunnahListSelector);
  const [list, setList] = React.useState([]);
  let lists: any = [];
  const debouncedTrue = useCallback(
    debounce((listss: any, items: any) => {
      dispatch(CheckFardList(listss, value.format(), items?.type));
    }, 2000),

    [],
  );

  React.useEffect(() => {
    setList(sunnaList);
  }, [sunnaList]);

  const _fetchCheck = (item: any) => {
    let newList = [...list];
    let index = newList.findIndex((i: any) => i.id === item.id);
    newList[index] = {...newList[index], selected: !item?.selected};
    setList(newList);
    lists = newList;
    debouncedTrue(lists, item);
  };
  return (
    <View style={styles.row}>
      {list?.map((item: any, key: any) => {
        return (
          <TouchableOpacity key={key} onPress={() => _fetchCheck(item)}>
            <View
              key={key}
              style={[
                Styles.item,
                item?.selected
                  ? {backgroundColor: '#D3D3D3'}
                  : {
                      backgroundColor: 'transparent',
                      borderRadius: 14,
                      borderColor: '#D3D3D3',
                      borderWidth: 1,
                    },
              ]}>
              <Text
                size="xs"
                style={{
                  color: item?.selected ? colors.textSelctedpray : '#D3D3D3',
                  fontFamily: FontType.Bold,
                }}>
                {item?.name}
              </Text>
            </View>
          </TouchableOpacity>
        );
      })}
    </View>
  );
};
const styles = StyleSheet.create({
  row: {
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection: 'row',
    paddingBottom: 20,
  },
});
