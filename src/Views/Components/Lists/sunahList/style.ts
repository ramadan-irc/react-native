import {StyleSheet} from 'react-native';
import {width, hp} from '@theme/style';
var styles = StyleSheet.create({
  item: {
    width: 55,
    height: 55,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 19,
    marginHorizontal: 10,
  },
  selectedItem: {
    width: 55,
    height: 55,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 19,
    marginHorizontal: 10,
  },
  indicatorButton: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  loaderContainer: {
    flex: 1,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
  },
  loader: {
    position: 'absolute',
    height: hp(50),
    top: 0.5,
    left: -17,
    backgroundColor: '#0000001f',
    width: width,
    borderRadius: 10,
  },
});

export default styles;
