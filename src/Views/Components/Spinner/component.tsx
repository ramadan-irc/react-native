import React from 'react';
import { ActivityIndicator, View } from 'react-native';
import { theme } from '@theme/style';
import Spinner from 'react-native-spinkit';

interface Props {
  size: 'xs' | 's' | 'm' | 'l' | 'xl' | 'xxl';
  color: any;
}

const getSize: { [key: string]: number } = theme.sizeActivity;

const checkSize = (size: string): number => {
  return getSize[size];
};

export const RnSpinner = ({ size = 'm', color }: Props) => (
  // <ActivityIndicator
  //   size={checkSize(size)} color={color}
  // />
  <View style={{
    flex: 1, alignItems: 'center', justifyContent: 'center',
  }}>


    <Spinner
      style={{
        // position: 'absolute',
        flex: 1,
        transform: [{ scaleX: 1 }],
        alignSelf: 'center',
        zIndex: 100,
      }}
      isVisible={true}
      size={checkSize(size)}
      type={'Circle'}
      color={color}
    />
  </View>
); //Pulse /  Circle  /FadingCircleAlt

