import React from 'react';
import FontType from '@theme/fonts';
import { Text } from '@components';
import { Button, Icon } from '@ui-kitten/components';
interface TProps {
  onPress: () => void;
}

export const ReloadButton: React.FC<TProps> = ({
  onPress,
}) => {
  const IconRnder = () => (
    <Icon style={{ width: 20, height: 20 }} fill={'#656B8D'} name="flip-2-outline" />
  );
  return (
    <Button
      accessoryRight={IconRnder}
      style={{
        width: 50 * 2,
        height: 30,
        backgroundColor: '#F2F2F2',
        borderRadius: 20,
        borderWidth: 0,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        alignSelf: 'center',
        marginVertical: 15,
      }}
      children={() => (
        <Text size="s" style={{ color: '#656B8D', fontFamily: FontType.Bold, paddingRight: 5 }}>
          {'Reload'}
        </Text>
      )}
      onPress={onPress}
    />
  );
};
