import React from 'react';
import {PieChart} from 'react-native-svg-charts';
import {View} from 'react-native';
// import R from 'reactotron-react-native';
interface Props {
  data: {
    quran: Boolean;
    task: number;
    prayers: {
      fajr: boolean;
      duhur: boolean;
      asr: boolean;
      maghrib: boolean;
      isha: boolean;
    };
  };
  style: any;
}
export const PieChartComponent: React.FC<Props> = ({data, style}) => {
  const d = [
    {
      key: 1,
      amount: 50,
      svg: {fill: data?.quran ? '#96A782' : '#D6DCD0'},
    },
    {
      key: 2,
      amount: data?.task,
      svg: {fill: '#75AECC'},
    },
    {
      key: 3,
      amount: data?.task === undefined ? 50 : 50 - data?.task,
      svg: {fill: '#CDDEE7'},
    },
    {
      key: 4,
      amount: 10,
      svg: {fill: data?.prayers.fajr ? '#DFBE84' : '#ECE2D1'},
    },
    {
      key: 5,
      amount: 10,
      svg: {fill: data?.prayers.duhur ? '#DFBE84' : '#ECE2D1'},
    },
    {
      key: 6,
      amount: 10,
      svg: {fill: data?.prayers.asr ? '#DFBE84' : '#ECE2D1'},
    },
    {
      key: 7,
      amount: 10,
      svg: {fill: data?.prayers.maghrib ? '#DFBE84' : '#ECE2D1'},
    },
    {
      key: 8,
      amount: 10,
      svg: {fill: data?.prayers.isha ? '#DFBE84' : '#ECE2D1'},
    },
  ];
  return (
    <View style={{justifyContent: 'center', flex: 1}}>
      <PieChart
        style={style}
        data={d}
        innerRadius={'55%'}
        valueAccessor={({item}: any) => item.amount}
      />
    </View>
  );
};
