import React from 'react';
import {Text as RNText, TextStyle} from 'react-native';
import {theme} from '@theme/style';

export interface TextProps {
  size?: 'xs' | 's' | 'm' | 'l' | 'xl' | 'xxl';
  style?: TextStyle;
  bold?: boolean;
  children?: React.ReactNode;
}

const getSize: {[key: string]: number} = theme.sizes;

const checkSize = (size: string): number => {
  return getSize[size] || 0;
};
export const Text = ({size = 'm', children, style, ...rest}: TextProps) => (
  <RNText
    {...rest}
    style={{
      ...style,
      fontSize: checkSize(size),
    }}>
    {children}
  </RNText>
);
