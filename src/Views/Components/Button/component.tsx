import React from 'react';
import {Button} from '@ui-kitten/components';
import {ViewStyle} from 'react-native';
import {theme} from '@theme/style';
interface Props {
  children?: any;
  iconRight?: (ImageProps: any) => React.ReactElement;
  iconLeft?: (ImageProps: any) => React.ReactElement;
  appearance: 'filled' | 'outline' | 'ghost';
  status?: 'basic' | 'primary' | 'success' | 'info' | 'warning' | 'danger' | 'control';
  size: 'tiny' | 'small' | 'medium' | 'large' | 'giant';
  activeOpacity?: number;
  disabled?: boolean;
  style?: ViewStyle;
  onPress: () => void;
}

const getStatus: {[key: string]: string} = theme.statusButton;

const checkStatus = (status: string): string => {
  return getStatus[status] || 'basic';
};
const getSize: {[key: string]: string} = theme.sizeButton;

const checkSize = (size: string): string => {
  return getSize[size] || 'medium';
};
const getappearance: {[key: string]: string} = theme.appearanceButton;

const checkappearance = (appearance: string): string => {
  return getappearance[appearance] || 'outline';
};
export const RnButton: React.FC<Props> = ({
  children,
  iconRight,
  appearance,
  activeOpacity,
  size,
  status,
  style,
  disabled,
  iconLeft,
  onPress,
}) => {
  return (
    <Button
      onPress={onPress}
      size={checkSize(size)}
      status={checkStatus(status)}
      disabled={disabled}
      style={{...style}}
      children={children}
      accessoryRight={iconRight}
      accessoryLeft={iconLeft}
      appearance={checkappearance(appearance)}
      activeOpacity={activeOpacity}
    />
  );
};
RnButton.defaultProps = {
  activeOpacity: 0.8,
};
