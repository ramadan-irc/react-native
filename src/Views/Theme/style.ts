import {Dimensions} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const theme = {
  sizes: {
    xs: 10,
    s: 12,
    m: 16,
    l: 18,
    xl: 25,
    xxl: 32,
  },
  sizeActivity: {
    xs: 10,
    s: 14,
    m: 18,
    l: 24,
    xl: 28,
    xxl: 32,
  },
  spacing: {
    s: 8,
    m: 13,
    l: 20,
    xl: 30,
    xxl: 40,
  },
  radius: {
    max: 50,
    mid: 30,
    min: 15,
    xmin: 10,
  },
  border: {
    thin: 1,
    thick: 3,
  },
  statusInput: {
    basic: 'basic',
    primary: 'primary',
    success: 'success',
    info: 'info',
    warning: 'warning',
    danger: 'danger',
    control: 'control',
  },
  sizeInput: {
    small: 'small',
    medium: 'medium',
    large: 'large',
  },
  sizeButton: {
    tiny: 'tiny',
    small: 'small',
    medium: 'medium',
    large: 'large',
    giant: 'giant',
  },

  appearanceButton: {
    filled: 'filled',
    outline: 'outline',
    ghost: 'ghost',
  },
  statusButton: {
    basic: 'basic',
    primary: 'primary',
    success: 'success',
    info: 'info',
    warning: 'warning',
    danger: 'danger',
    control: 'control',
  },
};
const {width, height} = Dimensions.get('window');
export {width, height, theme, wp, hp};
