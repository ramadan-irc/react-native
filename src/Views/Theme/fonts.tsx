import {Platform} from 'react-native';

const Fonts = {
  RegularArabic: Platform.OS === 'ios' ? 'DINNextLTArabic-Regular' : 'DINNextLTArabic-Regular',
  Bold: Platform.OS === 'ios' ? 'DINNextLTPro-Bold' : 'DINNextLTPro-Bold',
  Medium: Platform.OS === 'ios' ? 'DINNextLTPro-Medium' : 'DINNextLTPro-Medium',
  Regular: Platform.OS === 'ios' ? 'DINNextLTPro-Regular' : 'DINNextLTPro-Regular',
  Lie: Platform.OS === 'ios' ? 'LietoMe' : 'LietoMe',
};
export default Fonts;
