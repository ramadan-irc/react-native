import * as React from 'react';
import {lightColors, darkColors} from './colorThemes';
import {setScheme} from '@store/User/userAction';
import {isDarkSelector} from '@store/User/userState';
import {useDispatch, useSelector} from 'react-redux';
export const ThemeContext = React.createContext({
  isDarkState: false,
  colors: lightColors,
  setScheme: ({}) => {},
});
export const ThemeProvider = (props: any) => {
  const isDarkState = useSelector(isDarkSelector);
  const dispatch = useDispatch();
  const defaultTheme: any = {
    isDarkState,
    colors: isDarkState ? darkColors : lightColors,
    setScheme: (scheme: any) => dispatch(setScheme(scheme === 'dark')),
  };
  return <ThemeContext.Provider value={defaultTheme}>{props.children}</ThemeContext.Provider>;
};

export const useTheme = () => React.useContext(ThemeContext);
