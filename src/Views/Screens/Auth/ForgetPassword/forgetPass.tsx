import React, { useState, useCallback } from 'react';
import { View, ImageBackground, ScrollView, SafeAreaView } from 'react-native';
import { MyDeeds, NewIslamic, MyDeedsFr } from '@constants';
import styles from './style';
import { ISLAMIC } from '@constants/images';
import { useDispatch, useSelector } from 'react-redux';
import { resetMessageRecover, recoverpassword } from '@store/User/userAction';
import { loadingSelector, messageSelector } from '@store/User/userState';
import { useTheme } from '@theme/ThemeProvider';
import { RnButton, Text, RnInput, RnSpinner } from '@components';
import { AuthParamsList } from '../../../Navigation/paramsList';
import { StackNavigationProp } from '@react-navigation/stack';
import { useTranslation } from 'react-i18next';
import { BackIcon, } from '@constants';

import '@i18n';
import { wp } from '@theme/style';
interface Props {
  navigation: StackNavigationProp<AuthParamsList, 'ForgetPassword'>;
}

export const ForgetPassword: React.FC<Props> = ({ navigation }) => {
  const [email, setEmail] = useState('');
  const dispatch = useDispatch();
  const { colors } = useTheme();
  const { t, i18n } = useTranslation();
  const selectedLngCode = i18n.language;
  const loading = useSelector(loadingSelector);
  const message = useSelector(messageSelector);
  const _fetchResetPassword = useCallback(async () => {
    dispatch(recoverpassword(email));
  }, [email, dispatch]);
  React.useEffect(() => {
    if (message === 'A link to your email has been sent') {
      navigation.navigate('Login');
    }
    return () => {
      dispatch(resetMessageRecover(''));
    };
  }, [message, navigation, dispatch]);
  const _goBackPress = () => navigation.goBack()
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <ImageBackground style={styles.image} source={ISLAMIC}>
        <ScrollView contentContainerStyle={styles.containerScroll}>
          <View style={styles.imageContainer}>
            <RnButton
              appearance="ghost"
              children={() =>
                <View style={styles.backIcon}>
                  <BackIcon height={25} width={25} style={{ top: 8, right: 2 }} />
                </View>
              }
              style={{ ...styles.backContainer, }}
              onPress={_goBackPress}
            />
            <NewIslamic style={styles.logo} />

            {selectedLngCode === 'en' ? (
              <MyDeeds style={styles.logohead} />
            ) : (
              <MyDeedsFr style={styles.logohead} />
            )}
          </View>



          <View style={{
            ...styles.inputContainer
          }}>
            <Text size="s" style={{ ...styles.userText, color: colors.text }}>
              {t('loginPage:Email')}
            </Text>
            <RnInput
              value={email}
              StyleText={styles.textInput}
              style={styles.input}
              onChangeText={(nextValue) => setEmail(nextValue)}
              Status="basic"
              Size="large"
            />
            <RnButton
              appearance="filled"
              children={() =>
                !loading ? (
                  <View style={{ flex: 1 }}>
                    <Text size="m" style={{ ...styles.loginText, color: colors.text }}>
                      {t('loginPage:recover')}
                    </Text>
                  </View>
                ) : (
                  <View style={{
                    ...styles.indicator,
                    position: 'absolute',
                  }}>
                    <RnSpinner size="l" color="#75AECC" />
                  </View>
                )
              }
              style={styles.button}
              disabled={loading ? true : false}
              size="large"
              onPress={_fetchResetPassword}
            />


          </View>
        </ScrollView>
      </ImageBackground>
    </SafeAreaView>
  );
};
