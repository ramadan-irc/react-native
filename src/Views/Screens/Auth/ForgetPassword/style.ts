import { StyleSheet, Platform } from 'react-native';
import { width, height, theme, wp } from '@theme/style';
import FontType from '@theme/fonts';
export default StyleSheet.create({
  containerScroll: {
    paddingBottom: theme.spacing.xxl,
    justifyContent: 'flex-start',
    // alignItems: 'center',
  },
  image: {
    height: height,
    width: width,
  },
  imageContainer: {
    justifyContent: 'flex-start',//'space-around'
    alignItems: 'center',
    alignContent: 'center',
    flexDirection: 'row',
  },
  logohead: {
    maxHeight: 100,
    maxWidth: 195,
    top: 24,
  },
  logo: {
    maxHeight: 70,
    maxWidth: 80,
    top: 34,
  },
  button: {
    borderRadius: theme.radius.mid,
    backgroundColor: '#C5E2ED',
    width: wp(55),
    marginVertical: theme.sizes.xl,
  },
  buttonFacebook: {
    borderRadius: theme.radius.mid,
    backgroundColor: '#80B1DC',
    width: wp(70),
    marginVertical: theme.sizes.xl,
  },
  buttonGoogle: {
    borderRadius: theme.radius.mid,
    backgroundColor: '#E68D89',
    width: wp(70),
    marginVertical: theme.sizes.xl,
  },
  inputContainer: {
    alignItems: 'center',
    paddingTop: theme.spacing.m,
    paddingHorizontal: theme.spacing.xxl,
  },
  backContainer: {
    marginTop: '15%',
    // alignSelf: 'flex-start',
    // marginStart: theme.spacing.xl,

  },
  userText: {
    marginTop: 28,
    top: 10,
    fontFamily: FontType.Bold,
  },
  textInput: {
    color: '#75AECC',
    fontFamily: FontType.Regular,
  },
  input: {
    marginTop: 26,
    borderRadius: 30,
    backgroundColor: '#FFFF',
    borderColor: '#75AECC',
  },
  passwordText: {
    marginTop: 6,
    top: 10,
    fontFamily: FontType.Bold,
  },
  loginText: {
    textAlign: 'center',
    fontFamily: FontType.Bold,
  },
  googleText: {
    color: '#FFFF',
    textAlign: 'center',
    fontFamily: FontType.Medium,
  },
  signUpText: {
    paddingTop: 10,
    fontFamily: FontType.Bold,
  },
  forgetpasswordText: {
    bottom: 2,
    fontFamily: FontType.Bold,
  },
  containerToast: {
    marginBottom: Platform.OS === 'ios' ? wp(8) : wp(8),
    borderRadius: theme.radius.xmin,
  },
  indicator: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  backIcon: {
    alignItems: 'center',
    width: 40,
    height: 40,
    // marginRight: 10,
    backgroundColor: '#F2F2F2',
    borderRadius: 10,
  },
});
