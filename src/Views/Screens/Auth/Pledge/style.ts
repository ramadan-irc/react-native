import {StyleSheet} from 'react-native';
import {width, height, theme, wp} from '@theme/style';
import FontType from '@theme/fonts';
export default StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    flex: 1,
    height: height,

    width: width,
  },
  button: {
    borderRadius: theme.radius.mid,
    backgroundColor: '#C5E2ED',
    width: wp(55),
    marginVertical: theme.sizes.xl,
  },
  containerScroll: {
    paddingBottom: 20,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  logoHeadContainer: {
    justifyContent: 'flex-start',
    alignContent: 'center',
    marginBottom: 30,
  },
  logoHead: {
    height: 100,
    width: 50,
    top: 24,
    alignSelf: 'center',
  },
  basmalhContainer: {
    justifyContent: 'flex-start',
    alignContent: 'center',
    paddingTop: 20,
  },
  basmalhImage: {
    height: 50,
    width: 310,
  },
  firstText: {
    textAlign: 'center',
    paddingVertical: 20,

    fontFamily: FontType.Medium,
  },
  trackerContainer: {
    justifyContent: 'flex-start',
    alignContent: 'center',
    paddingBottom: 24,
  },
  secondText: {
    textAlign: 'center',
    paddingTop: 20,
    fontFamily: FontType.Lie,
  },
  trackerImage: {
    height: 100,
    width: 270,
    top: 24,
    alignSelf: 'center',
  },
  border: {
    borderBottomWidth: 1,
    top: 10,
    width: width / 1.2,
  },
  textfirstContainer: {
    flexDirection: 'row',
    paddingTop: 36,
  },
});
