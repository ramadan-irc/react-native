import React from 'react';
import {IslamicPledge, LOGOHEAD, BAMALAH} from '@constants/images';
import styles from './style';
import {ImageBackground, View, Image, ScrollView, SafeAreaView} from 'react-native';
import {useTheme} from '@theme/ThemeProvider';
import {useTranslation} from 'react-i18next';
import {RnButton, Text} from '@components';
import {width, wp} from '@theme/style';
import {setAuth} from '@store/User/userAction';
import {userSelector} from '@store/User/userState';
import {useDispatch, useSelector} from 'react-redux';
import '../../../../I18n';
import {DailyCheckListIcon, QuranIcon, PraysIcon} from '@constants';
import FontType from '@theme/fonts';
import {AuthParamsList} from '../../../Navigation/paramsList';
import {StackNavigationProp} from '@react-navigation/stack';
interface Props {
  navigation: StackNavigationProp<AuthParamsList, 'Pledge'>;
  user: {username: String; email: String};
}

export const Pledge: React.FC<Props> = () => {
  const {colors} = useTheme();
  const {t, i18n} = useTranslation();
  const dispatch = useDispatch();
  const user = useSelector(userSelector);
  const selectedLngCode = i18n.language;
  return (
    <SafeAreaView style={{flex: 1}}>
      <ImageBackground
        style={styles.image}
        resizeMethod={'auto'}
        imageStyle={{resizeMode: 'cover', alignSelf: 'flex-end'}}
        source={IslamicPledge}>
        <ScrollView contentContainerStyle={styles.containerScroll}>
          <View style={styles.logoHeadContainer}>
            <Image source={LOGOHEAD} style={styles.logoHead} />
          </View>
          <View style={styles.basmalhContainer}>
            <Image source={BAMALAH} style={styles.basmalhImage} />
          </View>
          <Text
            size="s"
            style={{
              ...styles.firstText,
              color: colors.textpledge,
            }}>
            {t('Pledge:first')}
          </Text>
          <View style={styles.trackerContainer}>
            <Text
              size="xxl"
              style={{
                color: colors.textpledge,
                ...styles.secondText,
              }}>
              {t('Pledge:second')}
            </Text>
            <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
              <View style={{alignItems: 'center'}}>
                <PraysIcon width={80} height={80} />
                <Text size="m" style={{fontFamily: FontType.Bold, color: '#656B8D', paddingTop: 8}}>
                  {t('Pledge:Pr')}
                </Text>
              </View>
              <View style={{alignItems: 'center'}}>
                <QuranIcon width={80} height={80} style={{marginLeft: 20}} />
                <Text
                  size="m"
                  style={{
                    fontFamily: FontType.Bold,
                    color: '#656B8D',
                    paddingTop: 8,
                    marginLeft: 20,
                  }}>
                  {t('Pledge:Qr')}
                </Text>
              </View>
              <View style={{alignItems: 'center'}}>
                <DailyCheckListIcon width={80} height={80} />
                <Text
                  size="m"
                  style={{
                    fontFamily: FontType.Bold,
                    color: '#656B8D',
                    paddingTop: 8,
                    width: selectedLngCode === 'fr' ? wp(24) : wp(28),
                    textAlign: 'center',
                  }}>
                  {t('Pledge:gH')}
                </Text>
              </View>
            </View>
          </View>
          <Text
            size="s"
            style={{
              color: colors.textpledge,
              ...styles.firstText,
            }}>
            {t('Pledge:goals')}
          </Text>
          <View
            style={{
              borderBottomColor: colors.bordercolor,
              ...styles.border,
            }}
          />
          <View style={styles.textfirstContainer}>
            {selectedLngCode === 'en' ? (
              <>
                <Text
                  size="m"
                  style={{
                    textAlign: 'left',
                    color: colors.text,
                    fontFamily: FontType.Bold,
                  }}>
                  {t('Pledge:third')}
                </Text>
                <Text
                  size="xl"
                  style={{
                    textAlign: 'left',
                    color: colors.text,
                    bottom: 10,
                    paddingHorizontal: 5,
                    fontFamily: FontType.Lie,
                  }}>
                  {t('Pledge:R')}
                </Text>
                <Text
                  size="m"
                  style={{
                    textAlign: 'left',
                    color: colors.text,
                    fontFamily: FontType.Bold,
                  }}>
                  {t('Pledge:sc')}
                </Text>
              </>
            ) : (
              <>
                <Text
                  size="m"
                  style={{
                    textAlign: 'left',
                    color: colors.text,
                    fontFamily: FontType.Bold,
                  }}>
                  {t('Pledge:v')}
                </Text>
                <Text
                  size="xl"
                  style={{
                    textAlign: 'left',
                    color: colors.text,
                    bottom: 10,
                    paddingLeft: 5,
                    fontFamily: FontType.Lie,
                  }}>
                  {t('Pledge:R')}
                </Text>
              </>
            )}
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingTop: 5}}>
            <Text
              size="s"
              style={{
                textAlign: 'left',
                color: colors.textpledge,
                paddingRight: 10,
                top: 8,
                fontFamily: FontType.Bold,
              }}>
              {t('Pledge:I')}
            </Text>
            <View
              style={{
                borderBottomColor: colors.textpledge,
                borderBottomWidth: 1,
                bottom: 7,
                top: 2,
                width: width / 1.2,
              }}>
              <Text
                size="l"
                style={{
                  textAlign: 'left',
                  color: colors.textpledge,
                  fontFamily: FontType.Medium,
                }}>
                {user?.username}
              </Text>
            </View>
          </View>
          <View
            style={{
              paddingStart: wp(8),
              paddingEnd: wp(7),
              paddingVertical: wp(5),
            }}>
            <Text
              size="m"
              style={{
                textAlign: 'justify',
                lineHeight: 28,
                color: colors.textpledge,
                fontFamily: FontType.Medium,
              }}>
              {t('Pledge:fourth')}
            </Text>
            <Text
              size="m"
              style={{
                textAlign: 'justify',
                lineHeight: 28,
                color: colors.textpledge,
                fontFamily: FontType.Medium,
                paddingVertical: wp(3),
              }}>
              {t('Pledge:five')}
            </Text>
            <Text
              size="m"
              style={{
                textAlign: 'justify',
                lineHeight: 28,
                color: colors.textpledge,
                fontFamily: FontType.Medium,
                paddingVertical: wp(2),
              }}>
              {t('Pledge:six')}
            </Text>
            <Text
              size="m"
              style={{
                textAlign: 'justify',
                lineHeight: 25,
                color: colors.textpledge,
                fontFamily: FontType.Medium,
                paddingVertical: wp(2),
              }}>
              {t('Pledge:seven')}
            </Text>
            <Text
              size="m"
              style={{
                textAlign: 'left',
                color: colors.textpledge,
                fontFamily: FontType.Medium,
                paddingVertical: wp(1),
              }}>
              {t('Pledge:Ameen')}
            </Text>
          </View>
          <RnButton
            appearance="filled"
            children={() => (
              <View style={{flex: 1}}>
                <Text
                  size="m"
                  style={{color: colors.text, fontFamily: FontType.Bold, textAlign: 'center'}}>
                  {t('Pledge:CONTINUE')}
                </Text>
              </View>
            )}
            style={styles.button}
            size="large"
            onPress={() => dispatch(setAuth(true))}
          />
        </ScrollView>
      </ImageBackground>
    </SafeAreaView>
  );
};
