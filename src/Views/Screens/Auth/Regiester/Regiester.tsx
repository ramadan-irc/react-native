import React from 'react';
import {ISLAMIC} from '@constants/images';
import styles from './style';
import {View, ImageBackground, ScrollView, SafeAreaView} from 'react-native';
import {MyDeeds, NewIslamic, MyDeedsFr} from '@constants';
import {useTranslation} from 'react-i18next';
import {Inputs} from './lib';
interface Props {}

export const Register: React.FC<Props> = () => {
  const {i18n} = useTranslation();
  const selectedLngCode = i18n.language;
  return (
    <SafeAreaView style={{flex: 1}}>
      <ImageBackground style={styles.image} source={ISLAMIC}>
        <ScrollView contentContainerStyle={styles.containerScroll}>
          <View style={styles.imageContainer}>
            <NewIslamic style={styles.logo} />
            {selectedLngCode === 'en' ? (
              <MyDeeds style={styles.logohead} />
            ) : (
              <MyDeedsFr style={styles.logohead} />
            )}
          </View>
          <Inputs />
        </ScrollView>
      </ImageBackground>
    </SafeAreaView>
  );
};
