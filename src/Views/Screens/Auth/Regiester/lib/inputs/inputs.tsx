import React, { useCallback } from 'react';
import styles from '../../style';
import { View, KeyboardAvoidingView } from 'react-native';
import '../../../../../../I18n';
import { useForm, Controller } from 'react-hook-form';
import { useNavigation } from '@react-navigation/native';
import { RnButton, Text, RnInput, Touchable, RnSpinner } from '@components';
import { useTheme } from '@theme/ThemeProvider';
import { register as Reg } from '@store/User/userAction';
import { useTranslation } from 'react-i18next';
import { loadingSelector } from '@store/User/userState';
import { useDispatch, useSelector } from 'react-redux';
import { AuthParamsList } from '../../../../../Navigation/paramsList';
import { StackNavigationProp } from '@react-navigation/stack';
import AsyncStorage from '@react-native-async-storage/async-storage';
import moment from 'moment';
import { wp } from '@theme/style';
const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

interface Props { }
export const Inputs: React.FC<Props> = () => {
  const { colors } = useTheme();
  const navigation: StackNavigationProp<AuthParamsList, 'Register'> = useNavigation();
  const dispatch = useDispatch();
  const { i18n, t } = useTranslation();
  const selectedLngCode = i18n.language;
  const [username, setUsername] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [error, setError] = React.useState('');
  const [confirmP, setConfirmP] = React.useState('');
  const loading = useSelector(loadingSelector);
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const onSubmit = useCallback(
    async (data: any) => {
      if (password !== confirmP) {
        setError(
          selectedLngCode === 'en'
            ? 'Password does not match'
            : 'Le mot de passe ne correspond pas',
        );
      } else {
        const fcm = await AsyncStorage.getItem('@fcmToken');
        dispatch(
          Reg(
            username,
            data?.email,
            password,
            selectedLngCode === 'en' ? 'English' : 'French',
            fcm,
            moment().format(),
          ),
        );
        setError('');
      }
    },
    [password, confirmP, dispatch, username, selectedLngCode],
  );

  return (
    <View style={styles.inputContainer}>
      <KeyboardAvoidingView
        style={{
          alignItems: 'center',
          justifyContent: 'center', paddingVertical: 20,
        }}
        behavior="padding"
      >
        <Text
          size="m"
          style={{
            ...styles.userText,
            color: colors.text,
          }}>
          {t('signUp:Name')}
        </Text>
        <RnInput
          value={username}
          style={styles.input}
          StyleText={styles.textInput}
          onChangeText={(nextValue) => setUsername(nextValue)}
          Status="basic"
          Size="large"
        />
        <Text
          size="m"
          style={{
            color: colors.text,
            ...styles.Text,
          }}>
          {t('signUp:Email')}
        </Text>
        <Controller
          control={control}
          render={({ field: { onChange, value } }) => (
            <RnInput
              value={value}
              style={styles.input}
              keyboard="email-address"
              StyleText={styles.textInput}
              // eslint-disable-next-line no-shadow
              onChangeText={(value) => onChange(value)}
              Status={errors.email?.message ? 'danger' : 'basic'}
              Size="large"
              caption={errors.email?.message}
            />
          )}
          name="email"
          rules={{
            required: {
              value: true,
              message: t('signUp:emailReq'),
            },
            pattern: {
              value: EMAIL_REGEX,
              message: t('signUp:emailVal'),
            },
          }}
          defaultValue=""
        />
        <Text
          size="m"
          style={{
            color: colors.text,
            ...styles.Text,
          }}>
          {t('signUp:Password')}
        </Text>
        <RnInput
          value={password}
          style={styles.input}
          StyleText={styles.textInput}
          secureText={true}
          onChangeText={(nextValue) => setPassword(nextValue)}
          Status="basic"
          Size="large"
        />
        <Text
          size="m"
          style={{
            color: colors.text,
            ...styles.Text,
          }}>
          {t('signUp:conf')}
        </Text>
        <RnInput
          value={confirmP}
          style={styles.input}
          StyleText={styles.textInput}
          secureText={true}
          onChangeText={(nextValue) => setConfirmP(nextValue)}
          Status="basic"
          Size="large"
        />

        <RnButton
          appearance="filled"
          children={() =>
            !loading ? (
              <View style={{ flex: 1 }}>
                <Text size="xl" style={{ ...styles.regiesterText, color: colors.text }}>
                  {t('signUp:Sign')}
                </Text>
              </View>
            ) : (
              <View style={[styles.indicator]}>
                <RnSpinner size="l" color="#75AECC" />
              </View>
            )
          }
          style={styles.button}
          size="medium"
          disabled={errors.email?.message ? true : false}
          status={error === 'not same' ? 'danger' : 'basic'}
          onPress={handleSubmit(onSubmit)}
        />
        <Text
          size="l"
          style={{
            color: colors.error,
            paddingVertical: 10,
            ...styles.error,
          }}>
          {error}
        </Text>
        <Touchable activeOpacity={0.3} onPress={() => navigation.goBack()}>
          <Text>
            <Text size="l" style={{ color: colors.text, ...styles.logInText, }}>
              {`${t('loginPage:or')} `}
            </Text>
            <Text size="l" style={{ color: colors.text, ...styles.logInText, textDecorationLine: 'underline' }}>
              {t('signUp:Login')}
            </Text>
          </Text>
        </Touchable>

      </KeyboardAvoidingView>
    </View>
  );
};
