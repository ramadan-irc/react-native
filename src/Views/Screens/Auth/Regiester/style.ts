import { StyleSheet } from 'react-native';
import { width, height, theme, wp } from '@theme/style';
import FontType from '@theme/fonts';
export default StyleSheet.create({
  containerScroll: {
    paddingBottom: theme.spacing.xxl,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  image: {
    height: height,
    width: width,
  },
  userText: {
    marginTop: 26,
    top: 10,
    fontFamily: FontType.Bold,
    textAlign: 'left',
  },
  imageContainer: {
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
  },
  inputContainer: {
    alignItems: 'center',
    paddingTop: theme.spacing.m,
    paddingHorizontal: theme.spacing.xxl,
  },
  logohead: {
    maxHeight: 100,
    maxWidth: 195,
    top: 24,
  },
  logo: {
    maxHeight: 70,
    maxWidth: 80,
    top: 34,
  },
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  button: {
    borderRadius: theme.radius.mid,
    backgroundColor: '#C5E2ED',
    width: wp(65),
    marginVertical: theme.sizes.xl,
  },
  logInText: {
    fontFamily: FontType.Bold,
    bottom: 20,
  },
  input: {
    marginTop: 26,
    borderRadius: 30,
    backgroundColor: '#FFFF',
    borderColor: '#75AECC',
  },
  textInput: {
    color: '#75AECC',
    fontFamily: FontType.Regular,
  },
  Text: {
    marginTop: 6,
    top: 10,
    fontFamily: FontType.Bold,
    textAlign: 'left',

  },
  error: {
    marginTop: 2,
    marginBottom: 10,
    bottom: 10,
    fontFamily: FontType.Bold,
  },
  regiesterText: {
    textAlign: 'center',
    fontFamily: FontType.Bold,
  },
  indicator: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
