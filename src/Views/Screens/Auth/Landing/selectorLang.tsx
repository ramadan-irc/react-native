import React from 'react';
import { StyleSheet } from 'react-native';
import { MenuItem, OverflowMenu, Button } from '@ui-kitten/components';
import FontType from '@theme/fonts';
import { useTranslation } from 'react-i18next';
import { Text } from '@components';
import { Icon } from '@ui-kitten/components';
const LANGS = [
  { lngCode: 'en', label: 'ENGLISH' },
  { lngCode: 'fr', label: 'FRANÇAIS' },
];
interface Props {
  label: string;
  setLabel: (l: string) => void;
}
export const LanguageSelector: React.FC<Props> = ({ label, setLabel }) => {
  const { i18n } = useTranslation();
  const [visible, setVisible] = React.useState(false);
  const setLng = (lngCode: string) => i18n.changeLanguage(lngCode);

  const IconRnder = () => (
    <Icon style={{ width: 20, height: 20 }} fill={'#74AFCD'} name="arrow-down" />
  );
  const renderToggleButton = () => (
    <Button
      accessoryRight={IconRnder}
      style={styles.container}
      children={() => (
        <Text size="s" style={{ color: '#74AFCD', fontFamily: FontType.Bold, textAlign: 'center' }}>
          {label}
        </Text>
      )}
      onPress={() => setVisible(true)}
    />
  );
  return (
    <OverflowMenu
      visible={visible}
      anchor={renderToggleButton}
      placement="bottom end"
      style={{ backgroundColor: '#74AFCD', width: 70 * 2, alignItems: 'center' }}
      onBackdropPress={() => setVisible(false)}>
      {LANGS.map((l) => {
        return (
          <MenuItem
            title={() => (
              <Text size="s" style={{ color: '#FFFF', fontFamily: FontType.Bold, }}>
                {l.label}
              </Text>
            )}
            key={l.lngCode}
            style={{ backgroundColor: '#74AFCD', }}
            onPress={() => {
              setLng(l.lngCode);
              setLabel(l.label);
              setVisible(false);
            }}
          />
        );
      })}
    </OverflowMenu>
  );
};

export default LanguageSelector;

const styles = StyleSheet.create({
  container: {
    width: 70 * 2,
    height: 5,
    marginVertical: 10,
    backgroundColor: '#FFFF',
    borderRadius: 20,
    borderWidth: 2,
    borderColor: '#75AECC',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
});
