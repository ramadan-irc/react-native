import React from 'react';
import {SafeAreaView, ImageBackground} from 'react-native';
import {useDispatch} from 'react-redux';
import {setIsLang} from '@store/User/userAction';
import {RnButton, Text} from '@components';
import {LanguageSelector} from './selectorLang';
import {LANDINGIMAGE} from '@constants/images';
import {MyDeeds, NewIslamic, MyDeedsFr} from '@constants';
import Toast from 'react-native-root-toast';
import {useTheme} from '@theme/ThemeProvider';
import FontType from '@theme/fonts';
import AsyncStorage from '@react-native-async-storage/async-storage';
import styles from './style';
import '../../../../I18n';
import {useTranslation} from 'react-i18next';

export const LandingPage = () => {
  const dispatch = useDispatch();
  const {colors} = useTheme();
  const {i18n, t} = useTranslation();
  const [label, setLabel] = React.useState('Select Langauge');
  const selectedLngCode = i18n.language;
  const setLang = async () => {
    try {
      if (label !== 'Select Langauge') {
        await AsyncStorage.setItem('@lang', 'true');
        dispatch(setIsLang(true));
      } else {
        Toast.show('Choose Langauge', {
          duration: Toast.durations.SHORT,
          position: Toast.positions.BOTTOM,
          textColor: '#FFFF',
          shadow: true,
          animation: true,
          textStyle: {fontFamily: FontType.Regular},
          hideOnPress: true,
          backgroundColor: colors.toast,
          delay: 0,
          opacity: 1,
          containerStyle: styles.containerToast,
        });
      }
    } catch (e) {
      console.log('rerr', e);
    }
  };
  return (
    <SafeAreaView style={{flex: 1}}>
      <ImageBackground style={styles.image} source={LANDINGIMAGE}>
        <NewIslamic style={{maxHeight: 100, maxWidth: 100, marginLeft: 5, marginTop: 40}} />
        {selectedLngCode === 'en' ? (
          <MyDeeds
            style={{
              maxWidth: 250,
              maxHeight: 100,
              marginRight: 28,
              marginBottom: 30,
              marginTop: 25,
            }}
          />
        ) : (
          <MyDeedsFr
            style={{
              maxWidth: 250,
              maxHeight: 100,
              marginLeft: 25,
              marginBottom: 30,
              marginTop: 25,
            }}
          />
        )}

        <RnButton
          appearance="outline"
          size="small"
          style={styles.button}
          children={() => (
            <Text size="xl" style={styles.text}>
              {t('Landing:Start')}
            </Text>
          )}
          onPress={setLang}
        />
        <Text size="m" style={styles.text1}>
          {t('Landing:LNG')}
        </Text>
        <LanguageSelector label={label} setLabel={setLabel} />
      </ImageBackground>
    </SafeAreaView>
  );
};
