import {StyleSheet, Platform} from 'react-native';
import {theme, wp, width, height} from '@theme/style';
import FontType from '@theme/fonts';
export default StyleSheet.create({
  header: {
    justifyContent: 'space-around',
    alignItems: 'flex-start',
    flexDirection: 'row',
    paddingVertical: 24,
  },
  containerScroll: {
    paddingVertical: theme.spacing.xxl,
    justifyContent: 'flex-start',
  },
  container: {
    flex: 1,
  },
  image: {
    height: height,
    width: width,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonSignout: {
    borderRadius: theme.radius.mid,
    backgroundColor: '#E29591',
    width: wp(40),
    marginVertical: theme.sizes.xl,
  },
  FEEDBACKbutton: {
    borderRadius: theme.radius.mid,
    backgroundColor: '#C5E2ED',
    width: wp(40),
    marginVertical: theme.sizes.xl,
  },
  containerItems: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'flex-start',
    paddingBottom: 20,
  },
  containerItem: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  Line: {
    borderBottomColor: '#E3E7EF',
    borderBottomWidth: 1,
    alignSelf: 'center',
    width: width / 1.1,
  },
  containerButton: {
    alignItems: 'flex-end',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  loader: {
    position: 'absolute',
    height: height,
    backgroundColor: 'transparent',
    width: width,
    flex: 1,
  },
  loaderContainer: {
    flex: 1,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    borderRadius: theme.radius.mid,
    backgroundColor: '#C5E2ED',
    width: wp(80),
    borderColor: '#C5E2ED',
    marginVertical: theme.sizes.xl,
  },
  Buttoncontainer: {
    width: 45 * 2,
    height: 20,
    backgroundColor: '#F2F2F2',
    borderRadius: 20,
    borderWidth: 1,
    bottom: 6,
    marginLeft: 20,
    borderColor: '#F2F2F2',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  text: {
    fontFamily: FontType.Bold,
    color: '#74AFCD',
  },
  containerToast: {
    marginBottom: Platform.OS === 'ios' ? wp(8) : wp(8),
    borderRadius: theme.radius.xmin,
  },
  text1: {
    fontFamily: FontType.Bold,
    color: '#656B8D',
    paddingTop: 8,
  },
});
