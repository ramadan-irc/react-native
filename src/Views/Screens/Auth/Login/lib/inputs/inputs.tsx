import React, { useCallback } from 'react';
import styles from '../../style';
import { View, Alert, Platform } from 'react-native';
import { WEB_CLIENT_ID } from '@constants';
import { useDispatch, useSelector, batch } from 'react-redux';
import { Icon } from '@ui-kitten/components';
import moment from 'moment';
import Toast from 'react-native-root-toast';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { AccessToken, GraphRequest, GraphRequestManager, LoginManager } from 'react-native-fbsdk';
import { GoogleSignin, statusCodes } from '@react-native-community/google-signin';
import FontType from '@theme/fonts';
import { login } from '@store/User/userAction';
import { loadingSelector } from '@store/User/userState';
import { useTranslation } from 'react-i18next';
import { resetMessageRecover } from '@store/User/userAction';
import { RnButton, Text, RnInput, Touchable, RnSpinner } from '@components';
import { useTheme } from '@theme/ThemeProvider';
import IconIonicons from 'react-native-vector-icons/Ionicons';

import appleAuth, {
  AppleAuthCredentialState,
  AppleAuthError,
  AppleAuthRealUserStatus,
  AppleAuthRequestOperation,
  AppleAuthRequestScope,
} from '@invertase/react-native-apple-authentication';

import '../../../../../../I18n';
import { wp } from '@theme/style';
interface Props {
  onNavigate: () => void;
  onNavigatePassword: () => void;
}
const faceIcon = (props: any) => <Icon {...props} name="facebook" fill="#FFFF" />;
const googleIcon = (props: any) => <Icon {...props} name="google" fill="#FFFF" />;
const appleIcon = (props: any) => (
  <IconIonicons {...props} name="logo-apple" color="#FFFF" size={20} />
);

export const Inputs: React.FC<Props> = ({ onNavigate, onNavigatePassword }) => {
  const { colors } = useTheme();
  const { i18n, t } = useTranslation();
  const selectedLngCode = i18n.language;
  const dispatch = useDispatch();
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const loading = useSelector(loadingSelector);
  const _fetchLogin = useCallback(async () => {
    const fcm = await AsyncStorage.getItem('@fcmToken');
    await dispatch(
      login(
        '',
        email,
        '',
        password,
        fcm,
        moment().format(),
        selectedLngCode === 'en' ? 'English' : 'French',
      ),
    );
  }, [dispatch, email, password, selectedLngCode]);
  const getInfoFromToken = (token: any) => {
    const PROFILE_REQUEST_PARAMS = {
      fields: {
        string: 'id,name,email',
      },
    };
    const profileRequest = new GraphRequest(
      '/me',
      { token, parameters: PROFILE_REQUEST_PARAMS },
      (error: any, user: any) => {
        if (error) {
          Toast.show(error, {
            duration: Toast.durations.SHORT,
            position: Toast.positions.BOTTOM,
            textColor: '#FFFF',
            shadow: true,
            animation: true,
            textStyle: { fontFamily: FontType.Regular },
            hideOnPress: true,
            backgroundColor: colors.toast,
            delay: 0,
            opacity: 1,
            containerStyle: styles.containerToast,
          });
        } else {
          // R.logImportant('user', user);
          batch(async () => {
            const fcm = await AsyncStorage.getItem('@fcmToken');
            await dispatch(
              login(
                user.id,
                user.email,
                user.name,
                '',
                fcm,
                moment().format(),
                selectedLngCode === 'en' ? 'English' : 'French',
              ),
            );
          });
        }
      },
    );
    new GraphRequestManager().addRequest(profileRequest).start();
  };

  const _loginWithFacebook = async () => {
    LoginManager.logInWithPermissions(['public_profile,email']).then(
      (loginface: any) => {
        if (loginface.isCancelled) {
          Toast.show('Login cancelled', {
            duration: Toast.durations.SHORT,
            position: Toast.positions.BOTTOM,
            textColor: '#FFFF',
            shadow: true,
            animation: true,
            textStyle: { fontFamily: FontType.Regular },
            hideOnPress: true,
            backgroundColor: colors.toast,
            delay: 0,
            opacity: 1,
            containerStyle: styles.containerToast,
          });
        } else {
          AccessToken.getCurrentAccessToken().then((data: any) => {
            const accessToken = data.accessToken.toString();
            getInfoFromToken(accessToken);
          });
        }
      },
      (error: any) => {
        Toast.show(error, {
          duration: Toast.durations.SHORT,
          position: Toast.positions.BOTTOM,
          shadow: true,
          animation: true,
          hideOnPress: true,
          textColor: '#FFFF',
          textStyle: { fontFamily: FontType.Regular },
          backgroundColor: colors.toast,
          delay: 0,
          opacity: 1,
          containerStyle: styles.containerToast,
        });
      },
    );
  };

  const _loginWithApple = async () => {
    const appleAuthRequestResponse: any = appleAuth
      .performRequest({
        requestedOperation: AppleAuthRequestOperation.LOGIN,
        requestedScopes: [AppleAuthRequestScope.EMAIL, AppleAuthRequestScope.FULL_NAME],
      })
      .then(
        (result) => {
          console.log(result);
          // get current authentication state for user
          // /!\ This method must be tested on a real device. On the iOS simulator it always throws an error.
          const credentialState: any = appleAuth.getCredentialStateForUser(result.user).then(
            async (state) => {
              console.log(state);
              // use credentialState response to ensure the user is authenticated
              if (state === AppleAuthCredentialState.AUTHORIZED) {
                // user is authenticated
                let body = {};
                body.token_id = result.identityToken;
                body.email = result.email;
                let name = null;
                if (result.fullName && result.fullName.givenName) {
                  name = result.fullName.givenName;
                }
                if (result.fullName && result.fullName.familyName) {
                  if (name) {
                    name = `${name} ${result.fullName.familyName}`;
                  } else {
                    name = result.fullName.familyName;
                  }
                }
                body.name = name;
                const fcm = await AsyncStorage.getItem('@fcmToken');

                await dispatch(
                  login(
                    body.token_id,
                    body.email,
                    body.name,
                    '',
                    fcm,
                    moment().format(),
                    selectedLngCode === 'en' ? 'English' : 'French',
                  ),
                );
              } else { 
                Toast.show(error, {
                  duration: Toast.durations.SHORT,
                  position: Toast.positions.BOTTOM,
                  shadow: true,
                  animation: true,
                  hideOnPress: true,
                  textColor: '#FFFF',
                  textStyle: { fontFamily: FontType.Regular },
                  backgroundColor: colors.toast,
                  delay: 0,
                  opacity: 1,
                  containerStyle: styles.containerToast,
                });
              }
            },
            (error) => {
              console.log('Login failed with error 1: ' + error);
              Toast.show(error, {
                duration: Toast.durations.SHORT,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true,
                textColor: '#FFFF',
                textStyle: { fontFamily: FontType.Regular },
                backgroundColor: colors.toast,
                delay: 0,
                opacity: 1,
                containerStyle: styles.containerToast,
              });
            },
          );
        },
        (error) => {
          console.log(error.code);
          console.log('Login failed with error 3: ' + error);
        },
      );
  };
  const _configureGoogleSign = () => {
    GoogleSignin.configure({
      webClientId: WEB_CLIENT_ID,
      offlineAccess: false,
    });
  };
  const _signInGoogle = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfos = await GoogleSignin.signIn();
      const fcm = await AsyncStorage.getItem('@fcmToken');

      await dispatch(
        login(
          userInfos.user.id,
          userInfos.user.email,
          userInfos.user.name,
          '',
          fcm,
          moment().format(),
          selectedLngCode === 'en' ? 'English' : 'French',
        ),
      );

      //  await dispatch(login(user.name, user.id));
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        Alert.alert('Process Cancelled');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        Alert.alert('Process in progress');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        Alert.alert('Play services are not available');
      } else {
        Alert.alert('Something else went wrong... ', error.toString());
      }
    }
  };
  React.useEffect(() => {
    _configureGoogleSign();
    dispatch(resetMessageRecover(''));
  }, [dispatch]);
  return (
    <View style={styles.inputContainer}>
      <Text size="m" style={{ ...styles.userText, color: colors.text }}>
        {t('loginPage:Email')}
      </Text>
      <RnInput
        value={email}
        StyleText={styles.textInput}
        style={styles.input}
        onChangeText={(nextValue) => setEmail(nextValue)}
        Status="basic"
        Size="large"
      />
      <Text
        size="m"
        style={{
          ...styles.passwordText,
          color: colors.text,
        }}>
        {t('loginPage:Password')}
      </Text>
      <RnInput
        value={password}
        StyleText={styles.textInput}
        style={styles.input}
        secureText={true}
        onChangeText={(nextValue) => setPassword(nextValue)}
        Status="basic"
        Size="large"
      />
      <RnButton
        appearance="filled"
        children={() =>
          !loading ? (
            <View style={{ flex: 1 }}>
              <Text size="xl" style={{ ...styles.loginText, color: colors.text }}>
                {t('loginPage:Login')}
              </Text>
            </View>
          ) : (
            <View style={{
              ...styles.indicator,

            }}>
              <RnSpinner size="l" color="#75AECC" />
            </View>
          )
        }
        style={styles.button}
        disabled={!loading ? false : true}
        size="medium"
        onPress={_fetchLogin}
      />
      <Touchable activeOpacity={0.2} onPress={onNavigatePassword}>
        <Text
          size="l"
          style={{ color: colors.text, ...styles.forgetpasswordText, paddingVertical: 12, textDecorationLine: 'underline' }}>
          {t('loginPage:recover')}
        </Text>
      </Touchable>

      <View
        style={styles.separatorContainer}
        animation={'zoomIn'}
        delay={700}
        duration={400}>
        <View style={styles.separatorLine} />
        <Text
          size="l"
          style={{
            ...styles.googleText,
            color: '#9B9FA4',// colors.text,
            marginHorizontal: wp(1),
          }}>
          {t('loginPage:Signinwith')}
        </Text>
        <View style={styles.separatorLine} />
      </View>
      <View style={{ flexDirection: 'row' }}>

        <RnButton
          appearance="filled"
          iconLeft={googleIcon}
          style={{
            ...styles.buttonSocial,
            ...styles.buttonGoogle
          }}
          size="large"
          onPress={() => _signInGoogle()}
        />

        <RnButton
          appearance="filled"
          iconLeft={faceIcon}
          style={{
            ...styles.buttonSocial,
            ...styles.buttonFacebook
          }}
          size="large"
          onPress={_loginWithFacebook}
        />

        {
          Platform.OS === 'ios' && appleAuth.isSupported &&
          <RnButton
            appearance="filled"
            iconLeft={appleIcon}
            style={{
              ...styles.buttonSocial,
              ...styles.buttonApple
            }}
            size="large"
            onPress={_loginWithApple}
          />}
      </View>


      <Touchable activeOpacity={0.2} onPress={onNavigate}>
        <Text style={{ marginTop: 40 }}>
          <Text size="l" style={{ color: colors.text, ...styles.signUpText, paddingTop: 8, }}>
            {`${t('loginPage:or')} `}
          </Text>
          <Text size="l" style={{ color: colors.text, ...styles.signUpText, paddingTop: 8, textDecorationLine: 'underline' }}>
            {t('loginPage:Signup')}
          </Text>
        </Text>
      </Touchable>
    </View>
  );
};
