import { StyleSheet, Platform } from 'react-native';
import { width, height, theme, wp } from '@theme/style';
import FontType from '@theme/fonts';
export default StyleSheet.create({
  containerScroll: {
    paddingBottom: theme.spacing.xxl,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  image: {
    height: height,
    width: width,
  },
  imageContainer: {
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
  },
  logohead: {
    maxHeight: 100,
    maxWidth: 195,
    top: 24,
  },
  logo: {
    maxHeight: 70,
    maxWidth: 80,
    top: 34,
  },
  button: {
    borderRadius: theme.radius.mid,
    backgroundColor: '#C5E2ED',
    width: wp(65),
    marginVertical: theme.sizes.xl,
  },
  buttonSocial: {
    borderRadius: theme.radius.mid,
    // width: wp(30),
    paddingHorizontal: wp(7),
    marginVertical: theme.sizes.s,
    marginHorizontal: wp(3)
  },
  buttonFacebook: {
    backgroundColor: '#80B1DC',
  },
  buttonApple: {
    backgroundColor: '#A3AAAE',

  },
  buttonGoogle: {
    backgroundColor: '#E68D89',
  },
  inputContainer: {
    alignItems: 'center',
    paddingTop: theme.spacing.m,
    paddingHorizontal: theme.spacing.xxl,
  },
  userText: {
    marginTop: 28,
    top: 10,
    fontFamily: FontType.Bold,
  },
  textInput: {
    color: '#75AECC',
    fontFamily: FontType.Regular,
  },
  input: {
    marginTop: 26,
    borderRadius: 30,
    backgroundColor: '#FFFF',
    borderColor: '#75AECC',
  },
  passwordText: {
    marginTop: 6,
    top: 10,
    fontFamily: FontType.Bold,
  },
  loginText: {
    textAlign: 'center',
    fontFamily: FontType.Bold,
  },
  googleText: {
    color: '#FFFF',
    textAlign: 'center',
    fontFamily: FontType.Medium,
  },
  signUpText: {
    paddingTop: 10,
    fontFamily: FontType.Bold,
  },
  forgetpasswordText: {
    bottom: 2,
    fontFamily: FontType.Bold,
  },
  containerToast: {
    marginBottom: Platform.OS === 'ios' ? wp(8) : wp(8),
    borderRadius: theme.radius.xmin,
  },
  indicator: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  separatorContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    marginVertical: 20,
  },
  separatorLine: {
    flex: 1,
    borderWidth: StyleSheet.hairlineWidth,
    height: StyleSheet.hairlineWidth,
    borderColor: '#9B9FA4',
  },

});
