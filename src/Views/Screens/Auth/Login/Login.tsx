import React from 'react';
import {View, ImageBackground, ScrollView, SafeAreaView} from 'react-native';
import styles from './style';
import {Inputs} from './lib';
import {ISLAMIC} from '@constants/images';
import {MyDeeds, NewIslamic, MyDeedsFr} from '@constants';
import {AuthParamsList} from '../../../Navigation/paramsList';
import {StackNavigationProp} from '@react-navigation/stack';
import {useTranslation} from 'react-i18next';

interface Props {
  navigation: StackNavigationProp<AuthParamsList, 'Login'>;
}

export const Login: React.FC<Props> = ({navigation}) => {
  const {i18n} = useTranslation();
  const selectedLngCode = i18n.language;
  return (
    <SafeAreaView style={{flex: 1}}>
      <ImageBackground style={styles.image} source={ISLAMIC}>
        <ScrollView contentContainerStyle={styles.containerScroll}>
          <View style={styles.imageContainer}>
            <NewIslamic style={styles.logo} />
            {selectedLngCode === 'en' ? (
              <MyDeeds style={styles.logohead} />
            ) : (
              <MyDeedsFr style={styles.logohead} />
            )}
          </View>
          <Inputs
            onNavigate={() => navigation.navigate('Register')}
            onNavigatePassword={() => navigation.navigate('ForgetPassword')}
          />
        </ScrollView>
      </ImageBackground>
    </SafeAreaView>
  );
};
