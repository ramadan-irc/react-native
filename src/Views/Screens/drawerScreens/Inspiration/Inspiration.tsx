import React from 'react';
import {View, Button} from 'react-native';
import {Text} from '@components/Text';
import {useTheme} from '@theme/ThemeProvider';
interface Props {
  navigation: any;
}

export const Inspiration: React.FC<Props> = ({navigation}) => {
  const {colors} = useTheme();
  return (
    <View style={{flex: 1, justifyContent: 'flex-start', alignItems: 'center'}}>
      <Text size="l" style={{backgroundColor: colors.background}}>
        InspirationScreen
      </Text>
      <Button
        title="ToggleDrawer"
        onPress={() => {
          navigation.toggleDrawer();
        }}
      />
    </View>
  );
};
