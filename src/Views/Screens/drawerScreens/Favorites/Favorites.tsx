import React from 'react';
import {View} from 'react-native';
import {Text} from '@components/Text';
import {useTheme} from '@theme/ThemeProvider';
interface Props {}

export const Favorites: React.FC<Props> = () => {
  const {colors} = useTheme();
  return (
    <View style={{flex: 1, justifyContent: 'flex-start', alignItems: 'center'}}>
      <Text size="l" style={{backgroundColor: colors.background}}>
        FavScreen
      </Text>
    </View>
  );
};
