import {StyleSheet} from 'react-native';
import {theme, wp, width} from '@theme/style';
import FontType from '@theme/fonts';
export default StyleSheet.create({
  header: {
    justifyContent: 'space-around',
    alignItems: 'flex-start',
    flexDirection: 'row',
    paddingVertical: 24,
  },
  containerScroll: {
    paddingVertical: theme.spacing.xxl,
    justifyContent: 'flex-start',
  },
  container: {
    flex: 1,
  },
  buttonback: {
    borderRadius: theme.radius.mid,
    backgroundColor: '#C5E2ED',
    width: wp(40),

    marginTop: 28,
  },
  editButton: {
    borderRadius: theme.radius.mid,
    backgroundColor: '#C5E2ED',
    width: wp(40),
    marginTop: 28,
  },
  containerItems: {
    justifyContent: 'flex-start',
    flexDirection: 'row',
    alignItems: 'flex-start',
    paddingBottom: 20,
  },
  containerItem: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  Line1: {
    borderBottomColor: '#75AECC',
    borderBottomWidth: 1.5,
    width: width / 5,
    marginLeft: 10,
    top: 25,
  },
  Line2: {
    borderBottomColor: '#75AECC',
    borderBottomWidth: 1.5,
    width: width / 5,
    marginRight: 25,
    top: 25,
  },
  containerButton: {
    alignItems: 'flex-end',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  Buttoncontainer: {
    width: 50 * 2,
    marginBottom: 10,
    backgroundColor: '#F2F2F2',
    borderRadius: 20,
    borderWidth: 1,
    bottom: 6,
    marginLeft: 20,
    borderColor: '#F2F2F2',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  textView: {
    paddingStart: wp(8),
    paddingEnd: wp(7),
    paddingVertical: wp(1),
  },
  text1View: {
    paddingStart: wp(4),
    paddingEnd: wp(4),
    paddingVertical: wp(1),
  },
  text2View: {
    paddingStart: wp(12),
    paddingEnd: wp(3),
    paddingVertical: wp(1),
  },
  text3View: {
    paddingStart: wp(4),
    paddingEnd: wp(2),
    paddingVertical: wp(1),
  },
  textStyle: {
    textAlign: 'left',
    lineHeight: 28,

    fontFamily: FontType.Medium,
  },
  text1Style: {
    textAlign: 'left',
    lineHeight: 28,
    paddingLeft: 15,

    fontFamily: FontType.Bold,
  },
  text2Style: {
    textAlign: 'left',
    lineHeight: 28,
    fontFamily: FontType.Medium,
  },
  text3Style: {
    textAlign: 'left',
    lineHeight: 25,
    fontFamily: FontType.Medium,
  },
  text4Style: {
    textAlign: 'left',
    lineHeight: 25,
    paddingVertical: 10,
    fontFamily: FontType.Medium,
  },
  buttonText: {
    color: '#75AECC',
    textAlign: 'center',
    fontFamily: FontType.Bold,
  },
});
