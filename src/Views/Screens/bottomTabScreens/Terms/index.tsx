import React from 'react';
import {View, ScrollView, SafeAreaView} from 'react-native';
import {Header} from './Lib';
import {Text, RnButton} from '@components';
import FontType from '@theme/fonts';
import {SettingsParamsList} from '../../../Navigation/paramsList';
import {StackNavigationProp} from '@react-navigation/stack';
import {useTheme} from '@theme/ThemeProvider';
import styles from './style';
import {useTranslation} from 'react-i18next';
import '@i18n';

interface Props {
  navigation: StackNavigationProp<SettingsParamsList, 'termsConditions'>;
}
export const Terms: React.FC<Props> = ({navigation}) => {
  const {colors} = useTheme();
  const {t} = useTranslation();
  return (
    <SafeAreaView
      style={{
        ...styles.container,
        backgroundColor: colors.background,
      }}>
      <Header />
      <ScrollView contentContainerStyle={styles.containerScroll}>
        <View style={styles.containerItems}>
          <View style={styles.Line1} />
          <Text
            size="xl"
            style={{
              color: '#75AECC',
              fontFamily: FontType.Lie,
              textAlign: 'center',
              marginHorizontal: 5,
            }}>
            {t('Terms:TermsC')}
          </Text>
          <View style={styles.Line2} />
        </View>
        <View style={styles.textView}>
          <Text
            size="m"
            style={{
              ...styles.textStyle,
              color: colors.textpolicy,
            }}>
            {t('Terms:firstTT')}
          </Text>
        </View>
        <Text
          size="l"
          style={{
            ...styles.text1Style,
            color: colors.textpolicy,
          }}>
          {t('Terms:changesTerms')}
        </Text>
        <View style={styles.text1View}>
          <Text
            size="m"
            style={{
              ...styles.text2Style,
              color: colors.textpolicy,
            }}>
            {t('Terms:twoTerms')}
          </Text>
        </View>
        <Text
          size="l"
          style={{
            ...styles.text1Style,
            color: colors.textpolicy,
          }}>
          {t('Terms:contactUsT')}
        </Text>
        <View style={styles.text1View}>
          <Text
            size="m"
            style={{
              ...styles.text2Style,
              color: colors.textpolicy,
            }}>
            {t('Terms:threeTerms')}
          </Text>
        </View>

        <View style={{alignItems: 'center'}}>
          <RnButton
            appearance="filled"
            children={() => (
              <View style={{flex: 1}}>
                <Text size="m" style={{...styles.buttonText, color: colors.textpolicy}}>
                  {t('Terms:settingsTerms')}
                </Text>
              </View>
            )}
            style={styles.buttonback}
            size="small"
            onPress={() => navigation.navigate('Setting')}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};
