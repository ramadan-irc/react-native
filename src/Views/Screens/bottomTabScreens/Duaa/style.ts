import {StyleSheet} from 'react-native';
export default StyleSheet.create({
  header: {
    justifyContent: 'space-around',
    alignItems: 'flex-start',
    flexDirection: 'row',
    paddingVertical: 24,
  },
  indicator: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
});
