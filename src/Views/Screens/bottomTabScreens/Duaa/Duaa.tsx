import React, { useCallback } from 'react';
import { View, SafeAreaView } from 'react-native';
import { RnSpinner } from '@components';
import styles from './style';
import { useTheme } from '@theme/ThemeProvider';
import { Header } from './header';
import { DuasList } from '@components/Lists/duasList';
import { useSelector, useDispatch } from 'react-redux';
import { GetDuaas, loadingGetSelector, duaasSelector } from '@store/Duaa/duaaSlice';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { logout } from '@store/User/userAction';
import { ReloadButton } from '@components/reloadButton/component';
interface Props {
  navigation: any;
}

export const Duaa: React.FC<Props> = () => {
  const { colors } = useTheme();
  const dispatch = useDispatch();
  const loading = useSelector(loadingGetSelector);
  const duaas = useSelector(duaasSelector);
  const checkRemove = async () => {
    try {
      const v = await AsyncStorage.getItem('@expirs');
      if (v === null) {
        dispatch(logout());
      }
    } catch (e) {
      console.log('rerr');
    }
  };
  const _fetchDuaas = useCallback(async () => {
    dispatch(GetDuaas());
  }, [dispatch]);
  React.useEffect(() => {
    _fetchDuaas();
    checkRemove();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [_fetchDuaas]);
  const renderReload = () => <ReloadButton
    onPress={() => _fetchDuaas()}
  />
  const renderContent = () => <DuasList data={duaas} />

  console.log(!duaas || (duaas && duaas.length === 0), !duaas, (duaas && duaas.length === 0))
  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: colors.background,
      }}>
      <Header />

      {(loading) ? <View style={{
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        right: '50%',
        top: '70%'

      }}>
        <RnSpinner size="xl" color="#75AECC" />
      </View>
        : (!duaas || (duaas && duaas.length === 0)) ?
          renderReload() :
          renderContent()
      }
    </SafeAreaView>
  )
};
