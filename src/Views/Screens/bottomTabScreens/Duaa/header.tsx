import React from 'react';
import {View} from 'react-native';
import {useTheme} from '@theme/ThemeProvider';
import {ISLAMICIco, WhiteDeeds, MyDeedsFrWh} from '@constants';
import styles from './style';
import {useTranslation} from 'react-i18next';

interface Props {}

export const Header: React.FC<Props> = () => {
  const {colors} = useTheme();
  const {i18n} = useTranslation();
  const selectedLngCode = i18n.language;
  return (
    <View style={{...styles.header, backgroundColor: colors.header}}>
      <ISLAMICIco width="80" height="80" />
      {selectedLngCode === 'en' ? (
        <WhiteDeeds width="200" style={{right: 20}} height="80" />
      ) : (
        <MyDeedsFrWh width="200" style={{right: 20}} height="80" />
      )}
    </View>
  );
};
