import React from 'react';
import { Linking, ImageBackground, SafeAreaView } from 'react-native';
import { Text, RnButton } from '@components';
import { LANDINGIMAGE } from '@constants/images';
import { MyDeeds, NewIslamic, MyDeedsFr } from '@constants';
import styles from './style';
import '@i18n';
import { useTranslation } from 'react-i18next';
interface Props {
  navigation: any;
}

export const Donate: React.FC<Props> = ({ navigation }) => {
  const { i18n, t } = useTranslation();
  const selectedLngCode = i18n.language;
  // React.useEffect(() => {
  //   const unsubscribe = navigation.addListener('tabPress', (e: any) => {
  //     e.preventDefault();
  //     Linking.openURL('https://donate.islamicreliefcanada.org/');
  //   });

  //   return unsubscribe;
  // }, [navigation]);
  // React.useEffect(() => {
  //   const unsubscribe = navigation.addListener('focus', () => {
  //     Linking.openURL('https://donate.islamicreliefcanada.org/');
  //   });
  //   return unsubscribe;
  // }, [navigation]);
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <ImageBackground style={styles.image} source={LANDINGIMAGE}>
        <NewIslamic style={{ maxHeight: 100, maxWidth: 100, marginLeft: 5, marginTop: 40 }} />
        {selectedLngCode === 'en' ? (
          <MyDeeds
            style={{
              maxWidth: 250,
              maxHeight: 100,
              marginRight: 28,
              marginBottom: 30,
              marginTop: 25,
            }}
          />
        ) : (
          <MyDeedsFr
            style={{
              maxWidth: 250,
              maxHeight: 100,
              marginLeft: 25,
              marginBottom: 30,
              marginTop: 25,
            }}
          />
        )}
        <RnButton
          appearance="outline"
          size="small"
          style={styles.button}
          children={() => (
            <Text size="xl" style={styles.text}>
              {t('homePage:Donate')}
            </Text>
          )}
          onPress={() => Linking.openURL('https://donate.islamicreliefcanada.org/')}
        />
      </ImageBackground>
    </SafeAreaView>
  );
};
