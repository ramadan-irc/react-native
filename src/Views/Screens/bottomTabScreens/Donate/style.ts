import {StyleSheet} from 'react-native';
import FontType from '@theme/fonts';
import {width, height, theme, wp} from '@theme/style';
export default StyleSheet.create({
  button: {
    borderRadius: theme.radius.mid,
    backgroundColor: '#C5E2ED',
    width: wp(80),
    borderColor: '#C5E2ED',
    marginVertical: theme.sizes.xl,
  },

  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontFamily: FontType.Bold,
    color: '#74AFCD',
  },
  Text: {
    color: '#FFFF',
    textAlign: 'center',
    fontFamily: FontType.Bold,
  },
  image: {
    height: height,
    width: width,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
