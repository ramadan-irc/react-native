import React from 'react';
import {View} from 'react-native';
import {BackIcon, StarRef, PenIcon, SemiCIcon} from '@constants';
import {Text, Touchable} from '@components';
import {useTheme} from '@theme/ThemeProvider';
import styles from './style';
import {loadingTitleSelector} from '@store/Reflictions/reflictionSlice';
import {useSelector} from 'react-redux';
import {useTranslation} from 'react-i18next';
import '@i18n';
interface Props {
  onPress: () => void;
}

export const Header: React.FC<Props> = ({onPress}) => {
  const loading = useSelector(loadingTitleSelector);
  const {t} = useTranslation();
  const {colors} = useTheme();
  return (
    <>
      <View style={{...styles.header}}>
        <Touchable onPress={onPress}>
          <BackIcon width="40" height="40" style={{top: 5}} />
        </Touchable>
        <Text size="xl" style={{...styles.textHeader, color: colors.duaaAr}}>
          {t('homePage:TReflection')}
        </Text>
        <StarRef width="110" style={styles.starIcon} height="110" />
        <PenIcon width="58" height="58" />
      </View>
      <View style={styles.rectangle}>
        {!loading ? (
          <>
            <SemiCIcon width={30} height={30} style={{left: 20}} />
            <Text size="s" style={styles.rectangleText}>
              {t('homePage:title')}
            </Text>
          </>
        ) : (
          <SemiCIcon width={30} height={30} style={{alignSelf: 'center'}} />
        )}
      </View>
    </>
  );
};
