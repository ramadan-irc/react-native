import React, { useCallback } from 'react';
import { View, ScrollView, SafeAreaView } from 'react-native';
import { Header } from './header';
import { Body } from './bodyAdd';
import { useTheme } from '@theme/ThemeProvider';
import { useDispatch, useSelector } from 'react-redux';
import {
  addStateSelector,
  AddRefliction,
  loadingAddSelector,
  setStatusAdd,
} from '@store/Reflictions/reflictionSlice';
import styles from './style';
import { ReflicationsParamsList } from '../../../Navigation/paramsList';
import { StackNavigationProp } from '@react-navigation/stack';

interface Props {
  navigation: StackNavigationProp<ReflicationsParamsList, 'NewReflications'>;
}

export const NewReflections: React.FC<Props> = ({ navigation }) => {
  const { colors } = useTheme();
  const dispatch = useDispatch();
  const state = useSelector(addStateSelector);
  const loading = useSelector(loadingAddSelector);
  const [body, setBody] = React.useState('');
  const _fetchAdd = useCallback(async () => {
    dispatch(AddRefliction(body));
  }, [body, dispatch]);
  React.useEffect(() => {
    if (state === 'Reflection has been added') {
      navigation.navigate('Reflections');
    }
    return () => {
      dispatch(setStatusAdd(''));
    };
  }, [navigation, dispatch, state, loading]);

  console.log('Reflection has been added', loading)
  return (
    <SafeAreaView
      style={{
        ...styles.container,
        backgroundColor: colors.background,
      }}>
      <Header onPress={() => navigation.goBack()} />
      <ScrollView contentContainerStyle={styles.containerScroll}>
        <View style={{ paddingVertical: 30 }}>
          <Body body={body} setBody={setBody} loading={loading} onPress={_fetchAdd} />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};
