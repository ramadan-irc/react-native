import {StyleSheet} from 'react-native';
import FontType from '@theme/fonts';
import {wp, theme, width} from '@theme/style';
export default StyleSheet.create({
  header: {
    justifyContent: 'space-around',
    alignItems: 'flex-start',
    flexDirection: 'row',
    paddingVertical: 24,
  },
  textHeader: {
    fontFamily: FontType.Lie,
    color: '#656B8D',
  },
  container: {
    flex: 1,
  },
  starIcon: {
    position: 'absolute',
    right: wp(6),
  },
  buttonPlus: {
    borderRadius: theme.radius.mid,
    backgroundColor: 'transparent',
    width: wp(30),
    borderColor: '#75AECC',
    marginVertical: theme.sizes.xxl,
  },
  buttonText: {
    color: '#75AECC',
    textAlign: 'center',
    fontFamily: FontType.Bold,
  },
  buttonContainer: {
    alignSelf: 'flex-start',
    paddingBottom: 18,
    paddingHorizontal: 20,
  },
  title: {
    fontFamily: FontType.Lie,
    color: '#7FCAAF',
    textAlign: 'center',
  },
  Body: {
    fontFamily: FontType.Lie,
    color: '#7FCAAF',
    textAlign: 'center',
    paddingTop: 30,
  },
  textInput: {
    color: '#75AECC',
    fontFamily: FontType.Regular,
    textAlign: 'left',
  },
  input: {
    marginTop: 26,
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    textAlign: 'left',
  },
  inputContainer: {
    alignItems: 'center',
    flex: 1,
  },
  indicator: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    backgroundColor: '#FFFF',
  },
  containerScroll: {
    paddingBottom: theme.spacing.xxl,
  },
  rectangle: {
    width: 130 * 2.6,
    height: 80,
    borderRadius: 20,
    backgroundColor: '#E9F2EE',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    alignSelf: 'center',
    marginVertical: 10,
  },
  rectangleText: {
    color: '#7FCAAF',
    fontFamily: FontType.Medium,
    lineHeight: 18,
    textAlign: 'left',
    paddingLeft: 20,
    width: wp(70),
  },
  dateStyle: {
    color: '#74AFCD',
    fontFamily: FontType.Lie,
  },
  Line: {
    borderBottomColor: '#E3E7EF',
    borderBottomWidth: 1,
    alignSelf: 'center',
    width: width / 1.1,
  },
  placeHolderStyle: {
    color: '#E3E7EF',
    fontFamily: FontType.Medium,
    textAlign: 'left',
  },
});
