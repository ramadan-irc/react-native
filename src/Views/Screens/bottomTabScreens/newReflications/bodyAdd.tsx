import React from 'react';
import { View, KeyboardAvoidingView } from 'react-native';
import { RnInput, Text, RnButton, RnSpinner } from '@components';
import styles from './style';
import moment from 'moment';
import { useTheme } from '@theme/ThemeProvider';
import { useTranslation } from 'react-i18next';
import '@i18n';

interface Props {
  body: string;
  loading: boolean;
  setBody: (body: string) => void;
  onPress: () => void;
}
export const Body: React.FC<Props> = ({ body, loading, setBody, onPress }) => {
  const date = moment().format('MMMM DD,  YYYY');
  const { t } = useTranslation();
  const { colors } = useTheme();
  return (
    <View style={styles.inputContainer}>
      <Text size="xl" style={styles.dateStyle}>
        {date.toString()}
      </Text>
      <View style={styles.Line} />
      <KeyboardAvoidingView
        style={{ justifyContent: 'center', flex: 1, alignItems: 'center' }}
        behavior="padding">
        <RnInput
          placeHolder={t('homePage:tap')}
          placeHolderColor={colors.placeholder}
          value={body}
          multiline={true}
          StyleText={styles.textInput}
          style={{
            ...styles.input,
            borderColor: 'gray',
            marginHorizontal: 15,
            borderRadius: 10,
            height: 150


          }}
          onChangeText={(nextValue) => setBody(nextValue)}
          Status="basic"
          Size="large"
        />
        <View
          style={{
            paddingTop: 20,
          }}>
          <RnButton
            appearance="outline"
            activeOpacity={0.1}
            children={() =>
              !loading ? (
                <View style={{ flex: 1 }}>
                  <Text
                    size="s"
                    style={{
                      ...styles.buttonText,
                    }}>
                    {t('homePage:sub')}
                  </Text>
                </View>
              ) : (
                <View style={[styles.indicator]}>
                  <RnSpinner size="l" color="#75AECC" />
                </View>
              )
            }

            style={styles.buttonPlus}
            size="medium"
            onPress={onPress}
          />
        </View>
      </KeyboardAvoidingView>
    </View>
  );
};
