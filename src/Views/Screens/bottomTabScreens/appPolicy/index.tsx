import React from 'react';
import {View, ScrollView, SafeAreaView, Linking} from 'react-native';
import {Header} from './Lib';
import {Text, RnButton} from '@components';
import FontType from '@theme/fonts';
import {SettingsParamsList} from '../../../Navigation/paramsList';
import {StackNavigationProp} from '@react-navigation/stack';
import {useTheme} from '@theme/ThemeProvider';
import styles from './style';
import {useTranslation} from 'react-i18next';
import '@i18n';

interface Props {
  navigation: StackNavigationProp<SettingsParamsList, 'appPolicy'>;
}
export const AppPolicy: React.FC<Props> = ({navigation}) => {
  const {colors} = useTheme();
  const {t} = useTranslation();
  return (
    <SafeAreaView
      style={{
        ...styles.container,
        backgroundColor: colors.background,
      }}>
      <Header />
      <ScrollView contentContainerStyle={styles.containerScroll}>
        <Text size="xl" style={{color: '#75AECC', fontFamily: FontType.Lie, textAlign: 'center'}}>
          {t('Privacy:firstP')}
        </Text>

        <View style={styles.textView}>
          <Text
            size="m"
            style={{
              ...styles.textStyle,
              color: colors.textpolicy,
            }}>
            {t('Privacy:secondP')}
          </Text>
        </View>
        <Text
          size="l"
          style={{
            ...styles.text1Style,
            color: colors.textpolicy,
          }}>
          {t('Privacy:thirdP')}
        </Text>
        <View style={styles.text1View}>
          <Text
            size="m"
            style={{
              ...styles.text2Style,
              color: colors.textpolicy,
            }}>
            {t('Privacy:fourthP')}
          </Text>
        </View>
        <View style={styles.text2View}>
          <Text
            size="m"
            style={{
              ...styles.text3Style,
              color: colors.textpolicy,
            }}>
            {'\u2B24'} {t('Privacy:fiveP')}
          </Text>

          <Text
            size="m"
            style={{
              ...styles.text4Style,
              color: colors.textpolicy,
            }}>
            {'\u2B24'} {t('Privacy:sixP')}
          </Text>

          <Text
            size="m"
            style={{
              ...styles.text3Style,
              color: colors.textpolicy,
            }}>
            {'\u2B24'} {t('Privacy:sevenP')}
          </Text>
        </View>
        <View style={styles.text1View}>
          <Text
            size="m"
            style={{
              ...styles.text2Style,
              color: colors.textpolicy,
            }}>
            {t('Privacy:egihttP')}
          </Text>
        </View>
        <Text
          size="l"
          style={{
            ...styles.text1Style,
            color: colors.textpolicy,
          }}>
          {t('Privacy:PP')}
        </Text>
        <View style={styles.text1View}>
          <Text
            size="m"
            style={{
              ...styles.text2Style,
              color: colors.textpolicy,
            }}>
            {t('Privacy:goalsPP')}
          </Text>
        </View>
        <View style={styles.text2View}>
          <Text
            size="m"
            style={{
              ...styles.text3Style,
              color: colors.textpolicy,
            }}>
            {'\u2B24'} {t('Privacy:nineP')}
          </Text>

          <Text
            size="m"
            style={{
              ...styles.text3Style,
              color: colors.textpolicy,
            }}>
            {'\u2B24'} {t('Privacy:tenP')}
          </Text>
        </View>
        <View style={styles.text1View}>
          <Text
            size="m"
            style={{
              ...styles.text2Style,
              color: colors.textpolicy,
            }}>
            {t('Privacy:ddPP')}
          </Text>
        </View>
        <Text
          size="l"
          style={{
            ...styles.text1Style,
            color: colors.textpolicy,
          }}>
          {t('Privacy:elevenP')}
        </Text>
        <View style={styles.text1View}>
          <Text
            size="m"
            style={{
              ...styles.text2Style,
              color: colors.textpolicy,
            }}>
            {t('Privacy:twleP')}
          </Text>
        </View>
        <Text
          size="l"
          style={{
            ...styles.text1Style,
            color: colors.textpolicy,
          }}>
          {t('Privacy:thitrP')}
        </Text>
        <View style={styles.text3View}>
          <Text
            size="m"
            style={{
              ...styles.text2Style,
              color: colors.textpolicy,
            }}>
            {t('Privacy:foutyP')}
          </Text>
        </View>
        <Text
          size="l"
          style={{
            ...styles.text1Style,
            color: colors.textpolicy,
          }}>
          {t('Privacy:fivftyP')}
        </Text>
        <View style={styles.text1View}>
          <Text
            size="m"
            style={{
              ...styles.text2Style,
              color: colors.textpolicy,
            }}>
            {t('Privacy:sixtyP')}
          </Text>
        </View>
        <Text
          size="l"
          style={{
            ...styles.text1Style,
            color: colors.textpolicy,
          }}>
          {t('Privacy:sventyP')}
        </Text>
        <View style={styles.text3View}>
          <Text
            size="m"
            style={{
              ...styles.text2Style,
              color: colors.textpolicy,
            }}>
            {t('Privacy:eightyPPP')}
          </Text>
        </View>
        <Text
          size="l"
          style={{
            ...styles.text1Style,
            color: colors.textpolicy,
          }}>
          {t('Privacy:nineTYPP')}
        </Text>
        <View style={styles.text3View}>
          <Text
            size="m"
            style={{
              ...styles.text2Style,
              color: colors.textpolicy,
            }}>
            {t('Privacy:twintwPP')}
          </Text>
        </View>
        <View style={{alignItems: 'center'}}>
          <RnButton
            appearance="filled"
            onPress={() =>
              Linking.openURL('https://www.islamicreliefcanada.org/get-involved/mydeeds/')
            }
            children={() => (
              <View style={{flex: 1}}>
                <Text size="m" style={{...styles.buttonText, color: colors.textpolicy}}>
                  {t('Privacy:firstP')}
                </Text>
              </View>
            )}
            style={styles.buttonback}
            size="small"
          />
        </View>
        <View style={{alignItems: 'center'}}>
          <RnButton
            appearance="filled"
            children={() => (
              <View style={{flex: 1}}>
                <Text size="m" style={{...styles.buttonText, color: colors.textpolicy}}>
                  {t('Privacy:twintoneP')}
                </Text>
              </View>
            )}
            style={styles.buttonback}
            size="small"
            onPress={() => navigation.navigate('Setting')}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};
