import React from 'react';
import {Tab, TabBar} from '@ui-kitten/components';
import {View} from 'react-native';
import {DuasListFav, TidbitFavList} from '@components/Lists/FavoritesList';
import {DuasIcon, DEADIco} from '@constants';
import {useTheme} from '@theme/ThemeProvider';
import styles from './style';
interface Props {
  data: any;
  duaas: any;
}

export const TopTab: React.FC<Props> = ({data, duaas}) => {
  const [selectedIndex, setSelectedIndex] = React.useState(0);
  const {colors} = useTheme();
  return (
    <View style={styles.container}>
      <TabBar
        selectedIndex={selectedIndex}
        style={{backgroundColor: colors.background}}
        onSelect={(index) => setSelectedIndex(index)}
        indicatorStyle={{backgroundColor: colors.indicator, height: 2}}>
        <Tab icon={() => <DEADIco width={50} height={50} />} />
        <Tab icon={() => <DuasIcon width={50} height={50} />} />
      </TabBar>
      {selectedIndex === 0 ? (
        <TidbitFavList data={data} />
      ) : selectedIndex === 1 ? (
        <DuasListFav duaas={duaas} />
      ) : null}
    </View>
  );
};
