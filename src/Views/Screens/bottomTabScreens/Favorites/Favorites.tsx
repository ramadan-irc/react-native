import React, { useCallback } from 'react';
import { View, SafeAreaView } from 'react-native';
import { useTheme } from '@theme/ThemeProvider';
import { RnSpinner } from '@components';
import { Header } from './Header';
import { TopTab } from './topTab';
import styles from './style';
import { GetTidbitsFav, loadingGetFavSelector, tidbitsFavSelector } from '@store/Tidbit/tidbitSlice';
import { GetDuaasFav, loadingDuaaFavSelector, duaasFavSelector } from '@store/Duaa/duaaSlice';
import { HomeParamsList } from '../../../Navigation/paramsList';
import { StackNavigationProp } from '@react-navigation/stack';
import { useSelector, useDispatch, batch } from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { logout } from '@store/User/userAction';
interface Props {
  navigation: StackNavigationProp<HomeParamsList, 'Favorites'>;
}

export const Favorites: React.FC<Props> = ({ navigation }) => {
  const dispatch = useDispatch();
  const { colors } = useTheme();
  const loadingGet = useSelector(loadingGetFavSelector);
  const tidbitsFav = useSelector(tidbitsFavSelector);
  const loadingDuaa = useSelector(loadingDuaaFavSelector);
  const duaaFav = useSelector(duaasFavSelector);
  const checkRemove = async () => {
    try {
      const v = await AsyncStorage.getItem('@expirs');
      if (v === null) {
        dispatch(logout());
      }
    } catch (e) {
      console.log('rerr');
    }
  };
  const _fetchTidbitsFav = useCallback(async () => {
    dispatch(GetTidbitsFav());
  }, [dispatch]);
  const _fetchDuaasFav = useCallback(async () => {
    dispatch(GetDuaasFav());
  }, [dispatch]);
  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      batch(() => {
        _fetchTidbitsFav();
        _fetchDuaasFav();
        checkRemove();
      });
    });
    return unsubscribe;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [_fetchDuaasFav, _fetchTidbitsFav, navigation]);

  return <SafeAreaView style={{ ...styles.container, backgroundColor: colors.background }}>
    <Header onPress={() => navigation.goBack()} />
    {!loadingGet && !loadingDuaa ? (
      <TopTab data={tidbitsFav} duaas={duaaFav} />
    ) : (
      <View style={{ ...styles.indicator, backgroundColor: colors.background }}>
        <RnSpinner size="xl" color="#75AECC" />
      </View>
    )}
  </SafeAreaView>
};
