import React from 'react';
import {View} from 'react-native';
import {BackIcon} from '@constants';
import {Text} from '@components';
import styles from './style';
import {useTranslation} from 'react-i18next';
import '@i18n';
import {useTheme} from '@theme/ThemeProvider';
interface Props {
  onPress: () => void;
}

export const Header: React.FC<Props> = ({onPress}) => {
  const {t} = useTranslation();
  const {colors} = useTheme();
  return (
    <>
      <View style={{...styles.header}}>
        <BackIcon
          width="30"
          height="30"
          style={{position: 'absolute', left: 20}}
          onPress={onPress}
        />

        <Text size="xl" style={{...styles.textHeader, color: colors.tidbitT}}>
          {t('homePage:Favourites')}
        </Text>
      </View>
      <View style={styles.Line} />
    </>
  );
};
