import {StyleSheet} from 'react-native';
import FontType from '@theme/fonts';
import {wp, theme, width} from '@theme/style';
export default StyleSheet.create({
  header: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    paddingVertical: 24,
  },
  textHeader: {
    fontFamily: FontType.Lie,
    color: '#FFFF',
    textAlign: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: '#656B8D',
  },
  starIcon: {
    position: 'absolute',
    right: wp(6),
  },
  buttonPlus: {
    borderRadius: theme.radius.mid,
    backgroundColor: '#C5E2ED',
    width: wp(50),
  },
  buttonText: {
    color: '#75AECC',
    textAlign: 'center',
    fontFamily: FontType.Medium,
  },
  buttonContainer: {
    alignSelf: 'flex-start',
    paddingBottom: 18,
    paddingHorizontal: 20,
  },
  Line: {
    borderBottomColor: '#E3E7EF',
    borderBottomWidth: 1,
    alignSelf: 'center',
    marginBottom: 15,
    width: width / 1.1,
  },
  indicator: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
});
