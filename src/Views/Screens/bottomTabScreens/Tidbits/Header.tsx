import React from 'react';
import {View} from 'react-native';
import {BackWhite, DEADIco} from '@constants';
import {Text} from '@components';
import styles from './style';
import {useTranslation} from 'react-i18next';
import '@i18n';
interface Props {
  onPress: () => void;
}

export const Header: React.FC<Props> = ({onPress}) => {
  const {t} = useTranslation();
  return (
    <>
      <View style={{...styles.header}}>
        <BackWhite
          width="40"
          height="40"
          style={{position: 'absolute', left: 20}}
          onPress={onPress}
        />

        <DEADIco width="60" height="60" />
        <Text size="xl" style={styles.textHeader}>
          {t('homePage:Tidbits')}
        </Text>
      </View>
      <View style={styles.Line} />
    </>
  );
};
