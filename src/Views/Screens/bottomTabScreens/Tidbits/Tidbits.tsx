import React, {useCallback} from 'react';
import {View, SafeAreaView} from 'react-native';
import {Header} from './Header';
import {RnSpinner} from '@components';
import styles from './style';
import {useTheme} from '@theme/ThemeProvider';
import {GetTidbits, loadingGetSelector, tidbitsSelector} from '@store/Tidbit/tidbitSlice';
import {TidbitList} from '@components/Lists/TidbitList';
import {HomeParamsList} from '../../../Navigation/paramsList';
import {StackNavigationProp} from '@react-navigation/stack';
import {useSelector, useDispatch} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {logout} from '@store/User/userAction';
interface Props {
  navigation: StackNavigationProp<HomeParamsList, 'Tidbits'>;
}

export const Tidbits: React.FC<Props> = ({navigation}) => {
  const dispatch = useDispatch();
  const {colors} = useTheme();
  const loadingGet = useSelector(loadingGetSelector);
  const tidbits = useSelector(tidbitsSelector);
  const checkRemove = async () => {
    try {
      const v = await AsyncStorage.getItem('@expirs');
      if (v === null) {
        dispatch(logout());
      }
    } catch (e) {
      console.log('rerr');
    }
  };
  const _fetchTidbits = useCallback(async () => {
    dispatch(GetTidbits());
  }, [dispatch]);
  React.useEffect(() => {
    _fetchTidbits();
    checkRemove();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [_fetchTidbits]);
  return !loadingGet ? (
    <SafeAreaView style={styles.container}>
      <Header onPress={() => navigation.goBack()} />
      <TidbitList data={tidbits} />
    </SafeAreaView>
  ) : (
    <View style={{...styles.indicator, backgroundColor: colors.background}}>
      <RnSpinner size="xl" color="#75AECC" />
    </View>
  );
};
