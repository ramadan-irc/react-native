import React, { useCallback } from 'react';
import { View, ScrollView, SafeAreaView } from 'react-native';
import { Text, RnButton, RnInput, RnSpinner } from '@components';
// import R from 'reactotron-react-native';
import { useTranslation } from 'react-i18next';
import '@i18n';
import { Header, CitySelector } from './Lib';
import {
  setEdit,
  editSelector,
  EditProfile,
  newUserSelector,
  loadingGetSelector,
  loadingEditSelector,
  GetProfile,
} from '@store/Profile/profileSlice';
import { useTheme } from '@theme/ThemeProvider';
import { useSelector, useDispatch } from 'react-redux';
import FontType from '@theme/fonts';
import styles from './style';
import { SettingsParamsList } from '../../../Navigation/paramsList';
import { StackNavigationProp } from '@react-navigation/stack';
import { ReloadButton } from '@components/reloadButton/component';
interface Props {
  user: { username: String; email: String; age: Number; gender: string; location: string };
  navigation: StackNavigationProp<SettingsParamsList, 'Setting'>;
}

export const Profile: React.FC<Props> = ({ navigation }) => {
  const dispatch = useDispatch();
  const { colors } = useTheme();
  const { t } = useTranslation();
  const user = useSelector(newUserSelector);
  const loadingGet = useSelector(loadingGetSelector);
  const loadingEdit = useSelector(loadingEditSelector);
  const [username, setUsername] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [age, setAge] = React.useState(null);
  const [selectedGender, setSelectedGender] = React.useState(user?.gender);
  const [city, setCity] = React.useState('');
  const isEdit = useSelector(editSelector);
  const _fetchEditProfile = useCallback(async () => {
    await dispatch(
      EditProfile(
        username === '' ? user.username : username,
        email === '' ? user.email : email,
        city === '' ? user.location : city,
        Number(age) === 0 ? user.age : Number(age),
        selectedGender === '' ? user.gender : selectedGender,
      ),
    );

    dispatch(GetProfile());
    dispatch(setEdit(false));
  }, [
    username,
    email,
    user.username,
    user.email,
    city,
    age,
    selectedGender,
    dispatch,
    user.age,
    user.gender,
    user.location,
  ]);
  const _fetchGetProfile = useCallback(async () => {
    dispatch(GetProfile());
  }, [dispatch]);
  React.useEffect(() => {
    _fetchGetProfile();
  }, [_fetchGetProfile]);

  const renderContent = () => <ScrollView contentContainerStyle={styles.containerScroll}>
    <View style={styles.containerItems}>
      <View style={styles.Line1} />
      <Text
        size="xl"
        style={{ color: '#75AECC', fontFamily: FontType.Lie, paddingHorizontal: 10 }}>
        {t('profile:myProfile')}
      </Text>
      <View style={styles.Line2} />
    </View>
    <View style={styles.containerItems}>
      <Text
        size="m"
        style={{ color: colors.calendarText, fontFamily: FontType.Regular, paddingLeft: 20 }}>
        {t('profile:Name')}
      </Text>
      {!isEdit ? (
        <Text
          size="m"
          style={{ color: colors.calendarText, fontFamily: FontType.Bold, paddingLeft: 20 }}>
          {user?.username}
        </Text>
      ) : (
        <RnInput
          placeHolder={user.username}
          value={username}
          StyleText={styles.textInput}
          style={styles.input}
          onChangeText={(nextValue) => setUsername(nextValue)}
          Status="basic"
          Size="medium"
        />
      )}
    </View>
    <View style={styles.containerItems}>
      <Text
        size="m"
        style={{ color: colors.calendarText, fontFamily: FontType.Regular, paddingLeft: 20 }}>
        {t('profile:Email')}
      </Text>
      {!isEdit ? (
        <Text
          size="m"
          style={{ color: colors.calendarText, fontFamily: FontType.Bold, paddingLeft: 20 }}>
          {user?.email}
        </Text>
      ) : (
        <RnInput
          placeHolder={user.email}
          value={email}
          StyleText={styles.textInput}
          style={styles.input}
          onChangeText={(nextValue) => setEmail(nextValue)}
          Status="basic"
          Size="medium"
        />
      )}
    </View>
    <View style={styles.containerItems}>
      <Text
        size="m"
        style={{ color: colors.calendarText, fontFamily: FontType.Regular, paddingLeft: 20 }}>
        {t('profile:pass')}
      </Text>

      <Text
        size="m"
        style={{ color: colors.calendarText, fontFamily: FontType.Bold, paddingLeft: 20 }}>
        *******
      </Text>
    </View>
    <View style={styles.Line} />
    <View style={{ ...styles.containerItems, paddingTop: 20 }}>
      <Text
        size="m"
        style={{ color: colors.calendarText, fontFamily: FontType.Regular, paddingLeft: 20 }}>
        {t('profile:loc')}
      </Text>
      {!isEdit ? (
        <Text
          size="m"
          style={{ color: colors.calendarText, fontFamily: FontType.Bold, paddingLeft: 20 }}>
          {user?.location}
        </Text>
      ) : (
        <RnInput
          placeHolder={user.location}
          value={city}
          StyleText={styles.textInput}
          style={styles.input}
          onChangeText={(nextValue) => setCity(nextValue)}
          Status="basic"
          Size="medium"
        />
      )}
    </View>
    <View style={styles.containerItems}>
      <Text
        size="m"
        style={{ color: colors.calendarText, fontFamily: FontType.Regular, paddingLeft: 20 }}>
        {t('profile:Age')}
      </Text>

      {!isEdit ? (
        <Text
          size="m"
          style={{ color: colors.calendarText, fontFamily: FontType.Bold, paddingLeft: 20 }}>
          {user?.age}
        </Text>
      ) : (
        <RnInput
          value={age}
          keyboard="numeric"
          StyleText={styles.textInput}
          style={styles.input}
          onChangeText={(nextValue: any) => setAge(nextValue)}
          Status="basic"
          Size="medium"
        />
      )}
    </View>
    <View style={styles.containerItems}>
      <Text
        size="m"
        style={{ color: colors.calendarText, fontFamily: FontType.Regular, paddingLeft: 20 }}>
        {t('profile:Gender')}
      </Text>

      {!isEdit ? (
        <Text
          size="m"
          style={{ color: colors.calendarText, fontFamily: FontType.Bold, paddingLeft: 20 }}>
          {user?.gender}
        </Text>
      ) : (
        <CitySelector selectedGender={selectedGender} setSelectedGender={setSelectedGender} />
      )}
    </View>
    <View
      style={{
        justifyContent: 'space-around',
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 30,
      }}>
      <RnButton
        appearance="filled"
        children={() => (
          <View style={{ flex: 1 }}>
            <Text
              size="m"
              style={{ color: '#75AECC', textAlign: 'center', fontFamily: FontType.Bold }}>
              {t('profile:sett')}
            </Text>
          </View>
        )}
        style={styles.buttonback}
        size="small"
        onPress={() => navigation.navigate('Setting')}
      />
      {!isEdit ? (
        <RnButton
          appearance="filled"
          children={() => (
            <View style={{ flex: 1 }}>
              <Text
                size="m"
                style={{ color: '#75AECC', textAlign: 'center', fontFamily: FontType.Bold }}>
                {t('profile:Edit')}
              </Text>
            </View>
          )}
          style={styles.editButton}
          size="small"
          onPress={() => dispatch(setEdit(true))}
        />
      ) : (
        <RnButton
          appearance="filled"
          children={() =>
            !loadingEdit ? (
              <View style={{ flex: 1 }}>
                <Text
                  size="m"
                  style={{ color: '#75AECC', textAlign: 'center', fontFamily: FontType.Bold }}>
                  {t('profile:submit')}
                </Text>
              </View>
            ) : (
              <View style={[styles.indicatorButton]}>
                <RnSpinner size="l" color="#75AECC" />
              </View>
            )
          }
          style={styles.editButton}
          size="small"
          onPress={_fetchEditProfile}
        />
      )}
    </View>
  </ScrollView>
  const renderReload = () => <ReloadButton
    onPress={() => _fetchGetProfile()}
  />
  return <SafeAreaView
    style={{
      ...styles.container,
      backgroundColor: colors.background,
    }}>
    <Header />
    {(loadingGet) ? <View style={{
      justifyContent: 'center',
      alignItems: 'center',
      position: 'absolute',
      right: '50%',
      top: '70%'
    }}>
      <RnSpinner size="xl" color="#75AECC" />
    </View>
      : (!user || user?.id == undefined) ?
        renderReload()
        : renderContent()
    }
  </SafeAreaView>
};
