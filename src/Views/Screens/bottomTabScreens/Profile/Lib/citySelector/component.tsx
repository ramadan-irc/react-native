import React from 'react';
import {MenuItem, OverflowMenu, Button} from '@ui-kitten/components';
import styles from '../../style';
import {setCity} from '@store/Profile/profileSlice';
import {useDispatch} from 'react-redux';
import {Icon} from '@ui-kitten/components';
import {Text} from '@components';
import FontType from '@theme/fonts';
const CITY = [
  {value: 'Male', label: 'Male'},
  {value: 'Female', label: 'Female'},
];

interface Props {
  selectedGender: string;
  setSelectedGender: (s: string) => void;
}

export const CitySelector: React.FC<Props> = ({selectedGender, setSelectedGender}) => {
  const dispatch = useDispatch();
  const [visible, setVisible] = React.useState(false);
  const IconRnder = () => (
    <Icon style={{width: 24, height: 24}} fill={'#656B8D'} name="arrow-down" />
  );
  const renderToggleButton = () => (
    <Button
      accessoryRight={IconRnder}
      style={styles.Buttoncontainer}
      children={() => (
        <Text size="xs" style={{color: '#656B8D', fontFamily: FontType.Bold}}>
          {selectedGender}
        </Text>
      )}
      onPress={() => setVisible(true)}
    />
  );
  return (
    <OverflowMenu
      visible={visible}
      anchor={renderToggleButton}
      placement="bottom end"
      onBackdropPress={() => setVisible(false)}>
      {CITY.map((c, key) => {
        return (
          <MenuItem
            title={c.label}
            key={key}
            style={{backgroundColor: '#F2F2F2', marginBottom: 10}}
            onPress={() => {
              setSelectedGender(c.label);
              dispatch(
                setCity({
                  city: c.label,
                }),
              );
              setVisible(false);
            }}
          />
        );
      })}
    </OverflowMenu>
  );
};
