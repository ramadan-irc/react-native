import {StyleSheet} from 'react-native';
import {theme, wp, width} from '@theme/style';
import FontType from '@theme/fonts';
export default StyleSheet.create({
  header: {
    justifyContent: 'space-around',
    alignItems: 'flex-start',
    flexDirection: 'row',
    paddingVertical: 24,
  },
  containerScroll: {
    paddingVertical: theme.spacing.xl,
    justifyContent: 'flex-start',
  },
  container: {
    flex: 1,
  },
  buttonback: {
    borderRadius: theme.radius.mid,
    backgroundColor: '#C5E2ED',
    width: wp(40),

    marginTop: 28,
  },
  editButton: {
    borderRadius: theme.radius.mid,
    backgroundColor: '#C5E2ED',
    width: wp(40),
    marginTop: 28,
  },
  containerItems: {
    justifyContent: 'flex-start',
    flexDirection: 'row',
    alignItems: 'flex-start',
    paddingBottom: 20,
  },
  containerItem: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  Line1: {
    borderBottomColor: '#75AECC',
    borderBottomWidth: 1.5,
    width: width / 4,
    marginLeft: 25,
    top: 25,
  },
  Line: {
    borderBottomColor: '#E3E7EF',
    borderBottomWidth: 1,
    alignSelf: 'center',
    width: width / 1.1,
  },
  Line2: {
    borderBottomColor: '#75AECC',
    borderBottomWidth: 1.5,
    width: width / 4,
    marginRight: 20,
    top: 25,
  },
  containerButton: {
    alignItems: 'flex-end',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  textInput: {
    color: '#75AECC',
    fontFamily: FontType.Regular,
  },
  input: {
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    width: wp(90),
    bottom: 8,
  },
  Buttoncontainer: {
    width: 50 * 2,
    marginBottom: 10,
    backgroundColor: '#F2F2F2',
    borderRadius: 20,
    borderWidth: 1,
    bottom: 6,
    marginLeft: 20,
    borderColor: '#F2F2F2',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  indicator: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  indicatorButton: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
