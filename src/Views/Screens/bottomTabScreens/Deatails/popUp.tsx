import React from 'react';
import RNModal from 'react-native-modal';
import {Dimensions, Platform, StyleSheet, View, Text} from 'react-native';
import {useTheme} from '@theme/ThemeProvider';
type ModalProps = {
  isVisible: boolean;
  children: React.ReactNode;
  [x: string]: any;
  setHide?: () => void;
};

const deviceWidth: any = Dimensions.get('window').width;
const deviceHeight: any =
  Platform.OS === 'ios' ? Dimensions.get('window').height / 2 : Dimensions.get('window').height;

export const Modal = ({isVisible = false, children, ...props}: ModalProps) => {
  return (
    <RNModal
      propagateSwipe
      animationIn="fadeIn"
      animationOut="fadeOut"
      isVisible={isVisible}
      deviceWidth={deviceWidth}
      deviceHeight={deviceHeight}
      animationInTiming={1000 / 2}
      animationOutTiming={1000 / 2}
      backdropTransitionInTiming={800}
      backdropTransitionOutTiming={800}
      {...props}>
      {children}
    </RNModal>
  );
};
const ModalContainer = ({children}: {children: React.ReactNode}) => {
  const {colors} = useTheme();
  return <View style={{...styles.container, backgroundColor: colors.background}}>{children}</View>;
};

const ModalHeader = ({title}: {title: string}) => {
  const {colors} = useTheme();
  return (
    <View style={styles.header}>
      <Text style={{...styles.text, color: colors.duaaAr}}>{title}</Text>
    </View>
  );
};

const ModalBody = ({children}: {children?: React.ReactNode}) => (
  <View style={styles.body}>{children}</View>
);

const styles = StyleSheet.create({
  container: {
    borderRadius: 15,
  },
  header: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    paddingTop: 10,
    textAlign: 'center',
    fontSize: 20,
    margin: 5,
  },
  body: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingHorizontal: 15,
    alignItems: 'center',
    minHeight: 150,
  },
});
Modal.Header = ModalHeader;
Modal.Container = ModalContainer;
Modal.Body = ModalBody;
