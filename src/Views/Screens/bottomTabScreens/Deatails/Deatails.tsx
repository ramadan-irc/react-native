import React, { useCallback } from 'react';
import { View, SafeAreaView } from 'react-native';
import { useTheme } from '@theme/ThemeProvider';
import { RnSpinner, RnButton, Text } from '@components';
import styles from './style';
import { Header } from './Header';
import { Body } from './Body';
import { ReflicationsParamsList } from '../../../Navigation/paramsList';
// import R from 'reactotron-react-native';
import {
  itemSelector,
  EditRefliction,
  loadingDeleteSelector,
  deleteStateSelector,
  DeleteRefliction,
  loadingEditSelector,
  setEdit,
  setStatusDelete,
  setRefliction,
  loadingSetSelector,
  stautsEditSelector,
  setStatusEdit,
} from '@store/Reflictions/reflictionSlice';
import { Modal } from './popUp';
import { useDispatch, useSelector } from 'react-redux';
import { StackNavigationProp } from '@react-navigation/stack';
import { useTranslation } from 'react-i18next';
import Fonts from '@theme/fonts';

interface Props {
  navigation: StackNavigationProp<ReflicationsParamsList, 'Deatails'>;
}

export const Deatails: React.FC<Props> = ({ navigation }) => {
  const { colors } = useTheme();
  const dispatch = useDispatch();
  const [visible, setVisible] = React.useState(false);
  const { t } = useTranslation();
  const loading = useSelector(loadingEditSelector);
  const loadingGet = useSelector(loadingSetSelector);
  const loadingDelete = useSelector(loadingDeleteSelector);
  const statusDelete = useSelector(deleteStateSelector);
  const statusEdit = useSelector(stautsEditSelector);
  // const item = useSelector(itemSelectedSelector);
  const itemSelected = useSelector(itemSelector);
  const [body, setBody] = React.useState(itemSelected?.text);
  const renderLoader = () => {
    return loadingDelete ? (
      <View style={styles.loader}>
        <View style={styles.loaderContainer}>
          <RnSpinner size="xl" color="#7FCAAF" />
        </View>
      </View>
    ) : null;
  };
  const _fetchEdit = useCallback(async () => {
    await dispatch(EditRefliction(itemSelected.id, body, itemSelected.date));
    dispatch(setEdit(false));
  }, [body, dispatch, itemSelected.date, itemSelected.id]);
  const _fetchDelete = useCallback(async () => {
    await dispatch(DeleteRefliction(itemSelected.id));
    !!loadingDelete && setVisible(false);
  }, [dispatch, itemSelected.id, loadingDelete]);
  React.useEffect(() => {
    if (statusDelete === 'Reflection has been deleted') {
      navigation.navigate('Reflections');
    }
    return () => {
      dispatch(setStatusDelete(''));
    };
  }, [navigation, dispatch, statusDelete]);
  React.useEffect(() => {
    if (statusEdit === 'Reflection has been updated') {
      dispatch(setRefliction(itemSelected.id));
    }
    return () => {
      dispatch(setStatusEdit(''));
    };
  }, [dispatch, statusEdit, itemSelected.id]);
  return !loading || loadingGet ? (
    <SafeAreaView
      style={{
        ...styles.container,
        backgroundColor: colors.background,
      }}>
      {renderLoader()}
      <Header onPress={() => navigation.goBack()} />
      <Body onSubmit={_fetchEdit} body={body} setBody={setBody} onDelete={() => setVisible(true)} />
      <Modal isVisible={visible} setHide={() => setVisible(false)}>
        <Modal.Container>
          <Modal.Header title={t('homePage:DeleteSure')} />
          <Modal.Body>
            <RnButton
              appearance="filled"
              children={() =>
                !loadingDelete ? (
                  <View style={{ flex: 1 }}>
                    <Text
                      size="m"
                      style={{ textAlign: 'center', fontFamily: Fonts.Bold, color: colors.text }}>
                      {t('homePage:YES')}
                    </Text>
                  </View>
                ) : (
                  <View style={{
                    ...styles.inde,
                    position: 'absolute',

                  }}>
                    <RnSpinner size="l" color="#75AECC" />
                  </View>
                )
              }
              style={styles.buttonAlertYes}
              disabled={loading ? true : false}
              size="medium"
              onPress={_fetchDelete}
            />
            <RnButton
              appearance="filled"
              children={() =>
                !loadingDelete ? (
                  <View style={{ flex: 1 }}>
                    <Text
                      size="m"
                      style={{ textAlign: 'center', fontFamily: Fonts.Bold, color: '#E68D89' }}>
                      {t('homePage:NO')}
                    </Text>
                  </View>
                ) : null
              }
              style={styles.buttonAlert}
              disabled={loading ? true : false}
              size="medium"
              onPress={() => setVisible(false)}
            />
          </Modal.Body>
        </Modal.Container>
      </Modal>
    </SafeAreaView>
  ) : (
    <View style={[styles.indicatorButton]}>
      <RnSpinner size="xl" color="#75AECC" />
    </View>
  );
};
