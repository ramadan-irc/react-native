import React from 'react';
import {View, ScrollView, Share} from 'react-native';
import {Text, RnButton, RnInput} from '@components';
import {EditPen} from '@constants';
import styles from './style';
import {itemSelector, isEditSelector, setEdit} from '@store/Reflictions/reflictionSlice';
import {useSelector, useDispatch} from 'react-redux';
import {Icon, Button} from '@ui-kitten/components';
import {useTheme} from '@theme/ThemeProvider';
import {useTranslation} from 'react-i18next';

interface Props {
  body: string;
  setBody: (body: string) => void;
  onSubmit: () => void;
  onDelete: () => void;
}
const deleteIcon = (props: any) => <Icon {...props} name="trash-2" fill="#E68D89" />;
export const Body: React.FC<Props> = ({body, setBody, onSubmit, onDelete}) => {
  const item = useSelector(itemSelector);
  const isEdit = useSelector(isEditSelector);
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const {colors} = useTheme();
  const onShare = async () => {
    try {
      await Share.share({
        message: item?.text,
      });
    } catch (error) {
      console.log(error.message);
    }
  };
  return (
    <>
      <View style={styles.containerDate}>
        <Text size="xl" style={styles.dateText}>
          {item?.date}
        </Text>
        <View style={styles.Line} />
      </View>
      <ScrollView contentContainerStyle={styles.bodyTextContainer}>
        {!isEdit ? (
          <Text size="m" style={{...styles.bodyText, color: colors.duaaAr}}>
            {item?.text}
          </Text>
        ) : (
          <RnInput
            value={body}
            StyleText={{...styles.textInput, color: colors.duaaAr}}
            style={styles.input}
            onChangeText={(nextValue: any) => setBody(nextValue)}
            Status="basic"
            multiline={true}
            Size="medium"
          />
        )}

        <View style={{flexDirection: 'row', justifyContent: 'center'}}>
          {!isEdit ? (
            <RnButton
              appearance="outline"
              children={() => (
                <View style={{flex: 1}}>
                  <Text size="s" style={{...styles.shareText, color: '#75AECC'}}>
                    {t('homePage:share')}
                  </Text>
                </View>
              )}
              style={styles.button}
              size="medium"
              onPress={onShare}
            />
          ) : null}
        </View>
        {!isEdit ? (
          <EditPen
            width={25}
            height={25}
            style={{position: 'absolute', right: 25, bottom: 32}}
            onPress={() => dispatch(setEdit(true))}
          />
        ) : (
          <View style={{flexDirection: 'row', justifyContent: 'center'}}>
            <Button
              appearance="outline"
              onPress={onSubmit}
              children={() => (
                <View style={{flex: 1}}>
                  <Text size="s" style={{...styles.shareText, color: '#75AECC'}}>
                    {t('homePage:sub')}
                  </Text>
                </View>
              )}
              style={styles.button}
              size="medium"
            />
          </View>
        )}
        {!isEdit ? (
          <Button
            appearance="ghost"
            onPress={onDelete}
            style={{position: 'absolute', left: 20, bottom: 15}}
            accessoryRight={deleteIcon}
            size="giant"
          />
        ) : null}
      </ScrollView>
    </>
  );
};
