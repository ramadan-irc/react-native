import {StyleSheet} from 'react-native';
import FontType from '@theme/fonts';
import {wp, theme, width, height} from '@theme/style';
export default StyleSheet.create({
  header: {
    justifyContent: 'space-around',
    alignItems: 'flex-start',
    flexDirection: 'row',
    paddingVertical: 24,
  },
  textHeader: {
    fontFamily: FontType.Lie,
  },
  container: {
    flex: 1,
  },
  starIcon: {
    position: 'absolute',
    right: wp(6),
  },
  buttonPlus: {
    borderRadius: theme.radius.mid,
    backgroundColor: '#C5E2ED',
    width: wp(50),
  },
  buttonText: {
    color: '#75AECC',
    textAlign: 'center',
    fontFamily: FontType.Medium,
  },
  buttonContainer: {
    alignSelf: 'flex-start',
    paddingBottom: 18,
    paddingHorizontal: 20,
  },
  rectangle: {
    width: 130 * 2.6,
    height: 80,
    borderRadius: 20,
    backgroundColor: '#E9F2EE',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    alignSelf: 'center',
    marginVertical: 20,
  },
  rectangleText: {
    color: '#7FCAAF',
    fontFamily: FontType.Medium,
    lineHeight: 18,
    textAlign: 'left',
    paddingLeft: 20,
    width: wp(70),
  },
  containerDate: {
    paddingVertical: 20,
    alignItems: 'center',
  },
  dateText: {
    color: '#74AFCD',
    fontFamily: FontType.Lie,
  },
  Line: {
    borderBottomColor: '#E3E7EF',
    borderBottomWidth: 1,
    alignSelf: 'center',
    width: width / 1.1,
  },
  bodyText: {
    color: '#656B8D',
    fontFamily: FontType.Medium,
    lineHeight: 25,
    textAlign: 'left',
  },
  bodyTextContainer: {
    paddingVertical: theme.spacing.s,
    justifyContent: 'center',
    paddingStart: wp(10),
    paddingEnd: wp(4),
  },
  shareText: {
    textAlign: 'center',
    fontFamily: FontType.Bold,
  },
  button: {
    borderRadius: theme.radius.mid,
    marginVertical: 20,
    borderColor: '#75AECC',
    backgroundColor: 'transparent',
    width: wp(25),
  },
  buttonAlert: {
    borderRadius: theme.radius.xmin,
    borderColor: '#E68D89',
    backgroundColor: 'transparent',
    width: wp(20),
  },
  buttonAlertYes: {
    borderRadius: theme.radius.xmin,
    borderColor: '#75AECC',
    backgroundColor: 'transparent',
    width: wp(20),
  },
  textInput: {
    fontFamily: FontType.Bold,
    textAlign: 'left',
  },
  input: {
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    textAlign: 'center',
    bottom: 8,
  },
  indicatorButton: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  loader: {
    flex: 1,
    position: 'absolute',
    height: height,
    backgroundColor: 'transparent',
    width: width,
  },
  loaderContainer: {
    flex: 1,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inde: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
