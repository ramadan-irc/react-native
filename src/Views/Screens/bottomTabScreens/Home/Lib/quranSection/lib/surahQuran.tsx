import React, {Dispatch} from 'react';
import {View, Platform} from 'react-native';
import {Icon} from '@ui-kitten/components';
import {Text, Touchable} from '@components';
import styles from '../../../style';
import {useTheme} from '@theme/ThemeProvider';
import FontType from '@theme/fonts';
import {UpdateQuran, updateQuranLocal} from '@store/Sections/quranSlice';
import debounce from 'lodash.debounce';
import Toast from 'react-native-root-toast';

interface Props {
  trakerQuran: {
    juz: number;
    surah: number;
    ayah: number;
  };
  maxLimit: {
    juz: number;
    surah: number;
    ayah: number;
  };
  dispatch: Dispatch<any>;
  minLimit: {
    juz: number;
    surah: number;
    ayah: number;
  };
}

export const SurahQuran: React.FC<Props> = ({trakerQuran, maxLimit, minLimit, dispatch}) => {
  const {colors} = useTheme();
  const debouncedPlus = React.useCallback(
    debounce((quranTracker: {juz: any; surah: any; ayah: any}) => {
      dispatch(UpdateQuran(quranTracker?.juz, quranTracker?.surah + 1, quranTracker?.ayah));
    }, 1500),
    [],
  );
  const debouncedMinus = React.useCallback(
    debounce((quranTracker: {juz: any; surah: any; ayah: any}) => {
      dispatch(UpdateQuran(quranTracker?.juz, quranTracker?.surah - 1, quranTracker?.ayah));
    }, 1500),
    [],
  );

  const _handlePlusSuraa = () => {
    if (trakerQuran?.surah < maxLimit?.surah) {
      dispatch(
        updateQuranLocal({
          ...trakerQuran,
          surah: trakerQuran?.surah + 1,
        }),
      );
      debouncedPlus(trakerQuran);
    } else {
      Toast.show('You Reached Maximum', {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        textColor: '#FFFF',
        shadow: true,
        animation: true,
        textStyle: {fontFamily: FontType.Bold},
        hideOnPress: true,
        backgroundColor: '#96A782',
        delay: 0,
        opacity: 1,
        containerStyle: {
          marginBottom: 18,
          borderRadius: 10,
        },
      });
    }
  };
  const _handleMinusSuraa = () => {
    if (trakerQuran?.surah > minLimit?.surah) {
      dispatch(
        updateQuranLocal({
          ...trakerQuran,
          surah: trakerQuran?.surah - 1,
        }),
      );
      debouncedMinus(trakerQuran);
    } else {
      Toast.show('You Reached Minimum', {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        textColor: '#FFFF',
        shadow: true,
        animation: true,
        textStyle: {fontFamily: FontType.Bold},
        hideOnPress: true,
        backgroundColor: '#96A782',
        delay: 0,
        opacity: 1,
        containerStyle: {
          marginBottom: 18,
          borderRadius: 10,
        },
      });
    }
  };
  return (
    <View style={styles.quranShapes}>
      <Touchable onPress={() => _handlePlusSuraa()}>
        <Icon style={{width: 26, height: 26}} fill={'#96A782'} name="arrow-up" />
      </Touchable>
      <View style={styles.quranCounter}>
        <Text
          size="s"
          style={{
            color: colors.textSelctedpray,
            ...styles.typesQuranText,
          }}>
          SURAH
        </Text>
        <Text
          size="l"
          style={{
            color: colors.textSelctedpray,
            fontFamily: FontType.Bold,
            bottom: Platform.OS === 'ios' ? 0 : 4,
          }}>
          {trakerQuran?.surah}
        </Text>
      </View>
      <Touchable onPress={() => _handleMinusSuraa()}>
        <Icon style={{width: 26, height: 26}} fill={'#96A782'} name="arrow-down" />
      </Touchable>
    </View>
  );
};
