import React from 'react';
import {View} from 'react-native';
import * as Progress from 'react-native-progress';
import styles from '../../../style';
import {Text} from '@components';
interface Props {
  trakerQuran: {
    juz: number;
    surah: number;
    ayah: number;
  };
}
export const ProgressBarComponent: React.FC<Props> = ({trakerQuran}) => {
  return (
    <>
      <Progress.Bar
        progress={trakerQuran?.juz / 30}
        width={320}
        style={styles.progressQuran}
        borderRadius={20}
        borderColor="#96A782"
        height={36}
        color="#96A782"
      />
      <View style={styles.juz}>
        <Text size="s" style={styles.PQ}>
          JUZ 1
        </Text>
        <Text size="s" style={styles.PQ}>
          JUZ 7
        </Text>
        <Text size="s" style={styles.PQ}>
          JUZ 15
        </Text>
        <Text size="s" style={styles.PQ}>
          JUZ 23
        </Text>
        <Text size="s" style={styles.PQ}>
          JUZ 30
        </Text>
      </View>
    </>
  );
};
