import React from 'react';
import { View } from 'react-native';
import { Text, Touchable, RnInput } from '@components';
import '@i18n';
import { useTranslation } from 'react-i18next';
import styles from '../../../style';

interface Props {
  showEdit: boolean;
  setShowEdit: React.Dispatch<React.SetStateAction<boolean>>;
  dailyQuran: {
    id: number;
    userId: number;
    value: boolean;
    readAt: any;
    readTime: number;
  };
  setRead: React.Dispatch<any>;
  _onSubmitRead: () => void;
  read: any;
}

export const QuranTime: React.FC<Props> = ({
  setRead,
  setShowEdit,
  showEdit,
  _onSubmitRead,
  read,
  dailyQuran,
}) => {
  const { t } = useTranslation();
  return (
    <View style={styles.lineRead}>
      <Text size="s" style={{
        ...styles.textRead,
        paddingStart: '10%',
        textAlign: 'center',
        textDecorationLine: 'underline'
      }}>
        {t('homePage:tRead')}
      </Text>
      <Touchable onPress={() => setShowEdit(!showEdit)}>
        <View style={styles.borderTimeStyle}>
          {showEdit ? (
            <RnInput
              placeHolder={JSON.stringify(dailyQuran?.readTime)}
              value={read}
              autoFocus={true}
              keyboard="number-pad"
              placeHolderColor="#96A782"
              StyleText={styles.textRead}
              returnKeyType="done"
              style={styles.input}
              onSubmit={() => {
                _onSubmitRead();
                setShowEdit(!showEdit);
              }}
              onChangeText={(nextValue: any) => setRead(nextValue)}
              Status="basic"
              Size="medium"
            />
          ) : (
            <Text size="l" style={styles.MinsStyle}>
              {dailyQuran?.readTime}
            </Text>
          )}
          <Text size="l" style={styles.MinStyle}>
            mins
          </Text>
        </View>
      </Touchable>
    </View>
  );
};
