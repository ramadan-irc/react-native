import React from 'react';
import {useDispatch, useSelector, batch} from 'react-redux';
import {
  quranSelector,
  maxLimitSelector,
  minLimitSelector,
  UpdateRead,
  updateRead,
  dailyQuranSelector,
} from '@store/Sections/quranSlice';
import {View} from 'react-native';
import {Text} from '@components';
import styles from '../../style';
import {BookMark} from '@constants';
import {dateSelector} from '@store/Sections/praySlice';
import {useTranslation} from 'react-i18next';
import {QuranTime, JuzQuran, SurahQuran, AyahQuran, ProgressBarComponent} from './lib';

import '@i18n';
interface Props {}

export const QuranSection: React.FC<Props> = () => {
  const dispatch = useDispatch();
  const [showEdit, setShowEdit] = React.useState(false);
  const trakerQuran = useSelector(quranSelector);
  const {t} = useTranslation();
  const dailyQuran = useSelector(dailyQuranSelector);
  const maxLimit = useSelector(maxLimitSelector);
  const minLimit = useSelector(minLimitSelector);
  const value = useSelector(dateSelector);
  const [read, setRead] = React.useState(null);

  const _onSubmitRead = () => {
    batch(() => {
      dispatch(
        updateRead({
          ...dailyQuran,
          readTime: read,
        }),
      );
      dispatch(UpdateRead(read, value.format()));
    });
  };
  return (
    <View>
      <QuranTime
        setRead={setRead}
        showEdit={showEdit}
        setShowEdit={setShowEdit}
        dailyQuran={{...dailyQuran}}
        _onSubmitRead={_onSubmitRead}
        read={read}
      />
      <View style={styles.bookmarkedContainer}>
        <BookMark width="28" height="28" />
        <Text size="xl" style={styles.bookmarkedText}>
          {t('homePage:bMarked')}
        </Text>
      </View>
      <View style={styles.quranContainer}>
        <JuzQuran
          minLimit={{...minLimit}}
          maxLimit={{...maxLimit}}
          trakerQuran={{...trakerQuran}}
          dispatch={dispatch}
        />
        <SurahQuran
          minLimit={{...minLimit}}
          maxLimit={{...maxLimit}}
          trakerQuran={{...trakerQuran}}
          dispatch={dispatch}
        />
        <AyahQuran
          minLimit={{...minLimit}}
          maxLimit={{...maxLimit}}
          trakerQuran={{...trakerQuran}}
          dispatch={dispatch}
        />
      </View>
      <ProgressBarComponent trakerQuran={{...trakerQuran}} />
    </View>
  );
};
