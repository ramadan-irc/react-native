import {ActionCreatorWithPayload} from '@reduxjs/toolkit';
import {batch} from 'react-redux';
export const _handlePlusAyah: any = (
  trakerQuran: {
    juz: number;
    surah: number;
    ayah: number;
  },
  maxLimit: {
    juz: number;
    surah: number;
    ayah: number;
  },
  dispatch: (action: any) => any,
  UpdateQuran: any,
  updateQuranLocal: ActionCreatorWithPayload<any, string>,
) => {
  if (trakerQuran?.ayah < maxLimit?.ayah) {
    batch(() => {
      dispatch(
        updateQuranLocal({
          ayah: trakerQuran?.ayah + 1,
          juz: trakerQuran?.juz,
          surah: trakerQuran?.surah,
        }),
      );
      dispatch(UpdateQuran(trakerQuran?.juz, trakerQuran?.surah, trakerQuran?.ayah + 1));
    });
  }
};

export const _handleMinusAyah: any = (
  trakerQuran: {
    juz: number;
    surah: number;
    ayah: number;
  },
  minLimit: {
    juz: number;
    surah: number;
    ayah: number;
  },
  dispatch: (action: any) => any,
  UpdateQuran: any,
  updateQuranLocal: ActionCreatorWithPayload<any, string>,
) => {
  if (trakerQuran?.ayah > minLimit?.ayah) {
    batch(() => {
      dispatch(
        updateQuranLocal({
          ayah: trakerQuran?.ayah - 1,
          juz: trakerQuran?.juz,
          surah: trakerQuran?.surah,
        }),
      );
      dispatch(UpdateQuran(trakerQuran?.juz, trakerQuran?.surah, trakerQuran?.ayah - 1));
    });
  }
};

export const _handlePlusJuz: any = (
  trakerQuran: {
    juz: number;
    surah: number;
    ayah: number;
  },
  maxLimit: {
    juz: number;
    surah: number;
    ayah: number;
  },
  dispatch: (action: any) => any,
  UpdateQuran: any,
  updateQuranLocal: ActionCreatorWithPayload<any, string>,
) => {
  if (trakerQuran?.juz < maxLimit?.juz) {
    batch(() => {
      dispatch(
        updateQuranLocal({
          ayah: trakerQuran?.ayah,
          juz: trakerQuran?.juz + 1,
          surah: trakerQuran?.surah,
        }),
      );
      dispatch(UpdateQuran(trakerQuran?.juz + 1, trakerQuran?.surah, trakerQuran?.ayah));
    });
  }
};

export const _handleMinusJuz: any = (
  trakerQuran: {
    juz: number;
    surah: number;
    ayah: number;
  },
  minLimit: {
    juz: number;
    surah: number;
    ayah: number;
  },
  dispatch: (action: any) => any,
  UpdateQuran: any,
  updateQuranLocal: ActionCreatorWithPayload<any, string>,
) => {
  if (trakerQuran?.juz > minLimit?.juz) {
    batch(() => {
      dispatch(
        updateQuranLocal({
          ayah: trakerQuran?.ayah,
          juz: trakerQuran?.juz - 1,
          surah: trakerQuran?.surah,
        }),
      );
      dispatch(UpdateQuran(trakerQuran?.juz - 1, trakerQuran?.surah, trakerQuran?.ayah));
    });
  }
};

export const _handlePlusSuraa: any = (
  trakerQuran: {
    juz: number;
    surah: number;
    ayah: number;
  },
  maxLimit: {
    juz: number;
    surah: number;
    ayah: number;
  },
  dispatch: (action: any) => any,
  UpdateQuran: any,
  updateQuranLocal: ActionCreatorWithPayload<any, string>,
) => {
  if (trakerQuran?.surah < maxLimit?.surah) {
    batch(() => {
      dispatch(
        updateQuranLocal({
          ayah: trakerQuran?.ayah,
          juz: trakerQuran?.juz,
          surah: trakerQuran?.surah + 1,
        }),
      );
      dispatch(UpdateQuran(trakerQuran?.juz, trakerQuran?.surah + 1, trakerQuran?.ayah));
    });
  }
};

export const _handleMinusSuraa: any = (
  trakerQuran: {
    juz: number;
    surah: number;
    ayah: number;
  },
  minLimit: {
    juz: number;
    surah: number;
    ayah: number;
  },
  dispatch: (action: any) => any,
  UpdateQuran: any,
  updateQuranLocal: ActionCreatorWithPayload<any, string>,
) => {
  if (trakerQuran?.surah > minLimit?.surah) {
    batch(() => {
      dispatch(
        updateQuranLocal({
          ayah: trakerQuran?.ayah,
          juz: trakerQuran?.juz,
          surah: trakerQuran?.surah - 1,
        }),
      );
      dispatch(UpdateQuran(trakerQuran.juz, trakerQuran.surah - 1, trakerQuran.ayah));
    });
  }
};
