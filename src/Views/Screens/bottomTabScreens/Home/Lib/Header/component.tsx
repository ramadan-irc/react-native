import React from 'react';
import {GetDeed} from '@store/landingData/landingSlice';
import {View} from 'react-native';
import {dateSelector} from '@store/Sections/praySlice';
import {useTheme} from '@theme/ThemeProvider';
import moment from 'moment';
import {build, buildNames, buildNamesFr} from './build';
import styles from '../../style';
import {FAV, ISLAMICIco} from '@constants';
import {useDispatch, useSelector} from 'react-redux';
import '@i18n';
import {DeedComponent, DaysNames, DaysMonth, IconCallendar} from './components';

interface Props {
  onPressCalendar: () => void;
  onPressTidbit: () => void;
  onPressFav: () => void;
}

export const Header: React.FC<Props> = ({onPressCalendar, onPressTidbit, onPressFav}) => {
  const dispatch = useDispatch();
  const {colors} = useTheme();
  const date = useSelector(dateSelector);
  const [value] = React.useState<any>(moment());
  const [valueFr] = React.useState<any>(moment().clone().locale('fr'));
  const [days, setDays] = React.useState([]);
  const [daysName, setDaysName] = React.useState([]);
  const [daysNameFr, setDaysNameFr] = React.useState([]);
  const encodeDate = encodeURIComponent(date.format(''));

  React.useEffect(() => {
    dispatch(GetDeed(encodeDate));
    setDays(build(value));
    setDaysName(buildNames(value));
    setDaysNameFr(buildNamesFr(valueFr));
  }, [dispatch, value, date, encodeDate, valueFr]);
  return (
    <React.Fragment>
      <View
        style={{
          ...styles.headerContainer,
          backgroundColor: colors.header,
        }}>
        <ISLAMICIco width={70} height={70} style={{top: 12}} />
        <DeedComponent onPressTidbit={onPressTidbit} />
        <FAV width={30} height={30} style={styles.favIcon} onPress={onPressFav} />
      </View>
      <DaysNames daysName={daysName} daysNameFr={daysNameFr} />
      <View style={{...styles.chartContainer, backgroundColor: colors.calendar}}>
        <DaysMonth days={days} />
        <IconCallendar onPressCalendar={onPressCalendar} />
      </View>
    </React.Fragment>
  );
};
