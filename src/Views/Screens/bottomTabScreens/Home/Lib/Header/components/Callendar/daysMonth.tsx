import React from 'react';
import {View, ViewStyle, Platform} from 'react-native';
import {Text} from '@components';
import {callndarTypeSelector, adjTypeSelector} from '@store/User/userState';
import styles from '../../../../style';
import {PieChartComponent} from '@components';
import momentHijri from 'moment-hijri';
import {dataIndicatiorSelector, loadingSelector} from '@store/landingData/landingSlice';
import {useSelector} from 'react-redux';
import {useTheme} from '@theme/ThemeProvider';
interface Props {
  days: any[];
}
export const DaysMonth: React.FC<Props> = ({days}) => {
  const adjType = useSelector(adjTypeSelector);
  const typeCalendar = useSelector(callndarTypeSelector);
  const indicatior = useSelector(dataIndicatiorSelector);
  const {colors} = useTheme();
  const loadingIndicatior = useSelector(loadingSelector);
  const VIEWSTYLE = {
    justifyContent: 'space-around',
    flex: 1,
  } as ViewStyle;
  const PIESTYLE = {
    height: Platform.OS === 'ios' ? 50 : 48,
    width: 60,
    paddingLeft: 10,
  } as ViewStyle;

  return (
    <>
      {days.map((d, key) => {
        return (
          <View style={VIEWSTYLE} key={key}>
            {!loadingIndicatior ? (
              <PieChartComponent
                style={PIESTYLE}
                data={indicatior !== undefined ? indicatior[d.format('YYYY-MM-DD')] : null}
              />
            ) : (
              <PieChartComponent
                style={PIESTYLE}
                data={indicatior !== undefined ? indicatior[d.format('YYYY-MM-DD')] : null}
              />
            )}
            {typeCalendar === 'Gregorian' ? (
              <View style={styles.GeorgianDiv}>
                <Text
                  style={{
                    color: colors.calendarText,
                    paddingLeft: 5,
                    ...styles.day,
                  }}>
                  {typeCalendar === 'Gregorian'
                    ? d.format('D').toString()
                    : adjType === '0'
                    ? momentHijri(d).format('iD').toString()
                    : adjType === '+1'
                    ? momentHijri(d).subtract(1, 'd').format('iD').toString()
                    : momentHijri(d).add(1, 'd').format('iD').toString()}
                </Text>
              </View>
            ) : (
              <View style={styles.HijriDiv}>
                <Text
                  style={{
                    color: colors.calendarText,
                    paddingLeft: 5,
                    ...styles.day,
                  }}>
                  {typeCalendar === 'Gregorian'
                    ? d.format('D').toString()
                    : adjType === '0'
                    ? momentHijri(d).format('iD').toString()
                    : adjType === '+1'
                    ? momentHijri(d).subtract(1, 'd').format('iD').toString()
                    : momentHijri(d).add(1, 'd').format('iD').toString()}
                </Text>
              </View>
            )}
          </View>
        );
      })}
    </>
  );
};
