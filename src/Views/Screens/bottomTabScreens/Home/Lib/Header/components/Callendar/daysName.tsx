import React from 'react';
import {View} from 'react-native';
import {Text} from '@components';
import {useTranslation} from 'react-i18next';
import '@i18n';
import styles from '../../../../style';
import {useTheme} from '@theme/ThemeProvider';

interface Props {
  daysNameFr: any[];
  daysName: any[];
}

export const DaysNames: React.FC<Props> = ({daysName, daysNameFr}) => {
  const {i18n} = useTranslation();
  const selectedLngCode = i18n.language;
  const {colors} = useTheme();
  return (
    <View
      style={{
        ...styles.callendarHeader,
        backgroundColor: colors.calendar,
      }}>
      {selectedLngCode === 'fr'
        ? daysNameFr.map((d, k) => (
            <Text
              size="s"
              key={k}
              style={{
                ...styles.dayText,
                color: colors.calendarText,
                textTransform: 'uppercase',
              }}>
              {d.substring(0, d.length - 1)}
            </Text>
          ))
        : daysName.map((d, i) => (
            <Text
              size="s"
              key={i + '12'}
              style={{
                ...styles.dayText,
                color: colors.calendarText,
                textTransform: 'uppercase',
              }}>
              {d}
            </Text>
          ))}
    </View>
  );
};
