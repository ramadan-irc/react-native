import React from 'react';
import {View} from 'react-native';
import styles from '../../../style';
import {Touchable} from '@components';
import {CalendarIcon, WhiteC} from '@constants';
import {useTheme} from '@theme/ThemeProvider';
interface Props {
  onPressCalendar: () => void;
}

export const IconCallendar: React.FC<Props> = ({onPressCalendar}) => {
  const {colors, isDarkState} = useTheme();
  return (
    <View style={{...styles.backCalendarIcon, backgroundColor: colors.calendarIcon}}>
      <Touchable onPress={onPressCalendar}>
        {isDarkState ? (
          <WhiteC width={30} height={30} style={{top: 7}} />
        ) : (
          <CalendarIcon width={30} height={30} style={{top: 7}} />
        )}
      </Touchable>
    </View>
  );
};
