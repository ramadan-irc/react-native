import React from 'react';
import {DEADIco} from '@constants';
import {Text, Touchable} from '@components';
import {View} from 'react-native';
import {deedSelector, loadingDeedSelector} from '@store/landingData/landingSlice';
import {useSelector} from 'react-redux';
import {useTheme} from '@theme/ThemeProvider';
import {useTranslation} from 'react-i18next';
import '@i18n';
import styles from '../../../style';

interface Props {
  onPressTidbit: () => void;
}

export const DeedComponent: React.FC<Props> = ({onPressTidbit}) => {
  const deed: any = useSelector(deedSelector);
  const loading = useSelector(loadingDeedSelector);
  const {t, i18n} = useTranslation();
  const {colors} = useTheme();
  const selectedLngCode = i18n.language;

  return (
    <>
      {!loading ? (
        <Touchable onPress={onPressTidbit}>
          <View style={{...styles.rectangle, backgroundColor: colors.dead}}>
            <DEADIco width={50} height={50} />
            <Text
              size="xs"
              style={{
                ...styles.textDead,
                color: colors.white,
              }}>
              {deed?.textEnglish === undefined
                ? t('homePage:Nodeed')
                : selectedLngCode === 'en'
                ? deed?.textEnglish
                : deed?.textFrench}
            </Text>
          </View>
        </Touchable>
      ) : (
        <View style={{...styles.rectangle, backgroundColor: colors.dead}}>
          <DEADIco width={80} height={80} />
        </View>
      )}
    </>
  );
};
