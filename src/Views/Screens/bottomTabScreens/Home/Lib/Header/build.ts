export function build(value: any) {
  const date = value.clone().subtract(5, 'day');
  const days = [];
  for (var i = 0; i < 5; i++) {
    days.push(date.add(1, 'day').clone());
  }
  return days;
}

export function buildNames(value: any) {
  const date = value.clone().subtract(4, 'day');

  const daysName = [];
  for (var i = 0; i < 5; i++) {
    daysName.push(date.format('ddd'));
    date.add(1, 'day').clone();
  }
  return daysName;
}

export function buildNamesFr(value: any) {
  const date = value.clone().subtract(4, 'day');

  const daysName = [];
  for (var i = 0; i < 5; i++) {
    daysName.push(date.format('ddd'));
    date.add(1, 'day').clone();
  }
  return daysName;
}
