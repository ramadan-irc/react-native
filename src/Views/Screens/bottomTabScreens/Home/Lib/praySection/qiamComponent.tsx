import React from 'react';
import {View, Platform} from 'react-native';
import styles from '../../style';
import {useTheme} from '@theme/ThemeProvider';
import {Text, Touchable} from '@components';
import {Icon} from '@ui-kitten/components';
import FontType from '@theme/fonts';

interface Props {
  handlePlus: () => void;
  handleMinus: () => void;
  Value: {
    id: Number;
    name: string;
    type: string;
    value: number;
    prayAt: string;
  };
}

export const QiayamComponent: React.FC<Props> = ({handleMinus, handlePlus, Value}) => {
  const {colors} = useTheme();
  return (
    <View style={styles.qiyamShape}>
      <Touchable onPress={handlePlus}>
        <Icon style={{width: 26, height: 26}} fill={colors.gold} name="arrow-up" />
      </Touchable>
      <View style={{backgroundColor: colors.gold, width: 69, alignItems: 'center', height: 50}}>
        <Text
          size="s"
          style={{
            color: colors.textSelctedpray,
            ...styles.quiamText,
          }}>
          QIYAM
        </Text>
        <Text
          size="l"
          style={{
            color: colors.textSelctedpray,
            fontFamily: FontType.Bold,
            bottom: Platform.OS === 'ios' ? 0 : 4,
          }}>
          {Value?.value}
        </Text>
      </View>
      <Touchable onPress={handleMinus}>
        <Icon style={{width: 26, height: 26}} fill={colors.gold} name="arrow-down" />
      </Touchable>
    </View>
  );
};
