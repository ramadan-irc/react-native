import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  loadingGetSelector,
  qiyamSelector,
  tarawehSelector,
  CheckFardSalah,
  dateSelector,
  updateQiaymLocal,
} from '@store/Sections/praySlice';
import { PraysList } from '@components/Lists/prayerList';
import { SunahList } from '@components/Lists/sunahList';
import { SafeAreaView, View } from 'react-native';
import { RnSpinner } from '@components';
import styles from '../../style';
import { useTheme } from '@theme/ThemeProvider';
import * as Progress from 'react-native-progress';
import { TarawehSelector } from '../tarawehSelector';
import { NumberProgress } from './progressNumber';
import { QiayamComponent } from './qiamComponent';
import debounce from 'lodash.debounce';
interface Props {
  progressValue: any;
}

export const PraySection: React.FC<Props> = ({ progressValue }) => {
  const { colors } = useTheme();
  const dispatch = useDispatch();
  const value = useSelector(dateSelector);
  const loadingGet = useSelector(loadingGetSelector);
  const tarawehValues = useSelector(tarawehSelector);
  const qiyamValues = useSelector(qiyamSelector);

  const debouncedPlus = React.useCallback(
    debounce((qiyam: { id: any; value: any; type: any }) => {
      dispatch(
        CheckFardSalah(
          [
            {
              id: qiyam?.id,
              value: qiyam?.value + 1,
            },
          ],
          value.format(),
          qiyam?.type,
        ),
      );
    }, 1500),
    [],
  );
  const debouncedMinus = React.useCallback(
    debounce(
      (qiyam: { id: any; value: any; type: any }) =>
        // console.log(qiyam
        dispatch(
          CheckFardSalah(
            [
              {
                id: qiyam?.id,
                value: qiyam?.value - 1,
              },
            ],
            value.format(),
            qiyam?.type,
          ),
        ),
      1500,
    ),
    [],
  );

  const _handlePlus = React.useCallback(() => {
    dispatch(
      updateQiaymLocal({
        ...qiyamValues,
        value: qiyamValues?.value + 1,
      }),
    );
    debouncedPlus(qiyamValues);
  }, [debouncedPlus, dispatch, qiyamValues]);
  const _handleMinus = React.useCallback(() => {
    if (qiyamValues.value !== 0) {
      dispatch(
        updateQiaymLocal({
          ...qiyamValues,
          value: qiyamValues?.value - 1,
        }),
      );
      debouncedMinus(qiyamValues);
    }
  }, [debouncedMinus, dispatch, qiyamValues]);

  const renderTarawehAndQiyam = () => <View style={styles.praysectionContainer}>
    <View>
      <TarawehSelector tarawehValue={tarawehValues?.value} />
      <Progress.Bar
        progress={progressValue}
        width={260}
        borderRadius={20}
        borderColor={colors.gold}
        height={35}
        color={colors.gold}
      />
      <NumberProgress />
    </View>
    <QiayamComponent
      handleMinus={() => _handleMinus()}
      handlePlus={() => _handlePlus()}
      Value={{
        ...qiyamValues,
      }}
    />
  </View>
  return !loadingGet ? (
    <SafeAreaView>
      <PraysList />
      <SunahList />

      {renderTarawehAndQiyam()}
    </SafeAreaView>
  ) : (
    <View style={[styles.indicatorButton]}>
      <RnSpinner size="xl" color="#75AECC" />
    </View>
  );
};
{/* <SafeAreaView>
  <PraysList />
  <SunahList />
  {!loadingGet ? (
    renderQiyam()
  ) : (
    <View style={[styles.indicatorButton]}>
      <RnSpinner size="xl" color="#75AECC" />
    </View>
  )}
</SafeAreaView> */}