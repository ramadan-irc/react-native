import React from 'react';
import {View, ViewStyle} from 'react-native';
import {Text} from '@components';
export const NumberProgress = () => {
  const VIEWSTYLE = {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 5,
  } as ViewStyle;
  return (
    <View style={VIEWSTYLE}>
      <Text size="s" style={{color: '#D3D3D3'}}>
        0
      </Text>
      <Text size="s" style={{color: '#D3D3D3'}}>
        10
      </Text>
      <Text size="s" style={{color: '#D3D3D3'}}>
        20
      </Text>
    </View>
  );
};
