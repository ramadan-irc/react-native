import React, { useCallback } from 'react';
import { MenuItem, OverflowMenu, Button } from '@ui-kitten/components';
import styles from '../../style';
import { Icon } from '@ui-kitten/components';
import { Text } from '@components';
import tron from 'reactotron-react-native';
import {
  tarawehSelector,
  CheckFardSalah,
  dateSelector,
  updateTarawehL,
  loadingSalahSelector,
} from '@store/Sections/praySlice';
import { batch, useDispatch, useSelector } from 'react-redux';
import { useTheme } from '@theme/ThemeProvider';

interface Props {
  tarawehValue: Number;
}

export const TarawehSelector: React.FC<Props> = ({ tarawehValue }) => {
  const tarawehValues = useSelector(tarawehSelector);
  const TARAWEH = [
    { value: 0, id: tarawehValues?.id },
    { value: 2, id: tarawehValues?.id },
    { value: 4, id: tarawehValues?.id },
    { value: 6, id: tarawehValues?.id },
    { value: 8, id: tarawehValues?.id },
    { value: 10, id: tarawehValues?.id },
    { value: 12, id: tarawehValues?.id },
    { value: 14, id: tarawehValues?.id },
    { value: 16, id: tarawehValues?.id },
    { value: 18, id: tarawehValues?.id },
    { value: 20, id: tarawehValues?.id },
  ];
  const value = useSelector(dateSelector);
  const [visible, setVisible] = React.useState(false);
  const dispatch = useDispatch();
  const _fetchSetValue = useCallback(
    async (item) => {
      tron.logImportant('dsd', [item]);
      console.log('dsd', tarawehValues, [item])
      batch(async () => {
        dispatch(
          updateTarawehL({
            ...tarawehValues,
            value: item?.value,
          }),
        );
        dispatch(CheckFardSalah([item], value.format(), tarawehValues.type));
      });
    },
    [dispatch, tarawehValues, value],
  );
  const IconRnder = () => (
    <Icon style={{ width: 24, height: 24 }} fill={colors.gold} name="arrow-down" />
  );
  const { colors } = useTheme();
  const renderToggleButton = () => (
    <Button
      accessoryRight={IconRnder}
      style={styles.selectorTaraweh}
      children={() => (
        <>
          <Text
            size="xs"
            style={{
              color: colors.gold,
              ...styles.tarawehText,
            }}>
            NAFLS
          </Text>
          <Text size="l" style={{ color: colors.gold, ...styles.tarawehNum }}>
            {tarawehValue}
          </Text>
        </>
      )}
      onPress={() => setVisible(true)}
    />
  );
  return (
    <OverflowMenu
      visible={visible}
      anchor={renderToggleButton}
      placement="bottom end"
      style={{ width: 100 * 2.6, backgroundColor: '#F2F2F2', }}
      onBackdropPress={() => setVisible(false)}>
      {TARAWEH.map((c, key) => {
        return (
          <MenuItem
            title={() => (
              <Text size="l" style={{ ...styles.TaText, color: colors.gold, paddingHorizontal: 40 * 2.6 }}>
                {c.value}
              </Text>
            )}
            key={key}
            style={{ backgroundColor: '#F2F2F2', }}
            onPress={() => {
              _fetchSetValue(c);
              setVisible(false);
            }}
          />
        );
      })}
    </OverflowMenu>
  );
};
