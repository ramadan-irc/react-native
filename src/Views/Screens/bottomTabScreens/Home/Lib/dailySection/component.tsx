import React from 'react';
import {DailyList} from '@components/Lists/dailyCheckList';
export const DailySection: React.FC = () => {
  return (
    <>
      <DailyList />
    </>
  );
};
