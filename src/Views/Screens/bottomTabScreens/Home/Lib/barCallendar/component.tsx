import React from 'react';
import {dateSelector, setDate} from '@store/Sections/praySlice';
import {useDispatch, useSelector} from 'react-redux';
import {View} from 'react-native';
import {Text} from '@components';
import styles from '../../style';
import {useTheme} from '@theme/ThemeProvider';
import {Icon} from '@ui-kitten/components';
import FontType from '@theme/fonts';
import {CurrMonth, CurrDay, CurrFREN} from './lib';
import '@i18n';

interface Props {}

export const BarCallendar: React.FC<Props> = () => {
  const {colors} = useTheme();
  const dispatch = useDispatch();
  const value = useSelector(dateSelector);
  const currDName = () => {
    return value.format('D');
  };
  const nextMonth = () => {
    return value.clone().add(1, 'd');
  };
  const prevMonth = () => {
    return value.clone().subtract(1, 'd');
  };
  const thisDay = () => {
    return value.clone().isSame(new Date(), 'D');
  };
  return (
    <>
      <View style={{...styles.callenderShape, backgroundColor: colors.barCalendar}}>
        <Icon
          style={{width: 24, height: 24}}
          fill={colors.arrowIcon}
          name="arrow-left"
          onPress={() => dispatch(setDate(prevMonth()))}
        />
        <CurrMonth />
        <CurrDay />
        <View style={{...styles.barCalendar, backgroundColor: colors.barC}} />
        <CurrFREN />
        <Text
          size="s"
          style={{
            color: colors.barC,
            fontFamily: FontType.Medium,
            textTransform: 'uppercase',
          }}>
          {currDName()}
        </Text>
        {!thisDay() ? (
          <Icon
            style={{width: 24, height: 24}}
            fill={colors.arrowIcon}
            name="arrow-right"
            onPress={() => !thisDay() && dispatch(setDate(nextMonth()))}
          />
        ) : (
          <Icon
            style={{width: 24, height: 24}}
            fill={'#E3EEF4'}
            name="arrow-right"
            onPress={() => !thisDay() && dispatch(setDate(nextMonth()))}
          />
        )}
      </View>
    </>
  );
};
