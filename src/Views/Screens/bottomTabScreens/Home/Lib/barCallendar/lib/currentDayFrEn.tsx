import React from 'react';
import {Text} from '@components';
import {useTranslation} from 'react-i18next';
import {useSelector} from 'react-redux';
import {dateSelector} from '@store/Sections/praySlice';
import {useTheme} from '@theme/ThemeProvider';
import FontType from '@theme/fonts';

export const CurrFREN = () => {
  const {i18n} = useTranslation();
  const selectedLngCode = i18n.language;
  const value = useSelector(dateSelector);
  const {colors} = useTheme();
  const currDayName = () => {
    return value.format('ddd');
  };
  const currDayNameFr = () => {
    return value.clone().locale('fr').format('ddd');
  };
  const currMonthName = () => {
    return value.format('MMM');
  };
  const currMonthNameFr = () => {
    return value.clone().locale('fr').format('MMM');
  };
  return (
    <>
      {selectedLngCode === 'en' ? (
        <Text
          size="s"
          style={{
            color: colors.barC,
            left: 12,
            fontFamily: FontType.Medium,
            textTransform: 'uppercase',
          }}>
          {currDayName()}
        </Text>
      ) : (
        <Text
          size="s"
          style={{
            color: colors.barC,
            left: 12,
            fontFamily: FontType.Medium,
            textTransform: 'uppercase',
          }}>
          {currDayNameFr().substring(0, currMonthNameFr().length - 1)}
        </Text>
      )}

      {selectedLngCode === 'fr' ? (
        <Text
          size="s"
          style={{
            color: colors.barC,
            fontFamily: FontType.Medium,
            left: 6,
            textTransform: 'uppercase',
          }}>
          {currMonthNameFr().substring(0, currMonthNameFr().length - 1)}
        </Text>
      ) : (
        <Text
          size="s"
          style={{
            color: colors.barC,
            fontFamily: FontType.Medium,
            left: 6,
            textTransform: 'uppercase',
          }}>
          {currMonthName()}
        </Text>
      )}
    </>
  );
};
