import React from 'react';
import { dateSelector } from '@store/Sections/praySlice';
import { useSelector } from 'react-redux';
import { Text } from '@components';
import momentHijri from 'moment-hijri';
import { useTheme } from '@theme/ThemeProvider';
import FontType from '@theme/fonts';
import { callndarTypeSelector } from '@store/User/userState';

export const CurrMonth = () => {
  const { colors } = useTheme();
  const value = useSelector(dateSelector);
  const typeCalendar = useSelector(callndarTypeSelector);
  const currMonthNameHijri = () => {
    return momentHijri(value).format('iMMMM');
  };
  const currMonthNameMilady = () => {
    return value.format('MMMM');
  };
  return (
    <>

      <Text
        size="xl"
        style={{
          textAlign: 'left',
          color: colors.calendarText,
          fontFamily: FontType.Lie,
        }}>
        {typeCalendar === 'Gregorian' ? currMonthNameMilady() : currMonthNameHijri()}
      </Text>

    </>
  );
};
