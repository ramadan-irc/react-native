import React from 'react';
import { View } from 'react-native';
import { Text } from '@components';
import { useSelector } from 'react-redux';
import { useTheme } from '@theme/ThemeProvider';
import { useTranslation } from 'react-i18next';
import { dateSelector } from '@store/Sections/praySlice';
import { callndarTypeSelector } from '@store/User/userState';
import momentHijri from 'moment-hijri';
import FontType from '@theme/fonts';
import '@i18n';
export const CurrDay = () => {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const typeCalendar = useSelector(callndarTypeSelector);
  const value = useSelector(dateSelector);
  const currDayNameMilady = () => {
    return value.format('D');
  };
  const currDayNameHijri = () => {
    return momentHijri(value).format('iD');
  };
  return (
    <View style={{ flexDirection: 'row' }}>
      <Text
        size="m"
        style={{
          color: colors.white,
          fontFamily: FontType.Bold,
          marginRight: 4,
        }}>
        {t('homePage:Day')}
      </Text>

      <Text
        size="m"
        style={{
          color: colors.white,
          fontFamily: FontType.Bold,
        }}>

        {typeCalendar === 'Gregorian' ? currDayNameMilady() : currDayNameHijri()}
      </Text>

    </View >
  );
};
