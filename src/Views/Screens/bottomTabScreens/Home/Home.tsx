import React from 'react';
import { View, ScrollView, SafeAreaView, TouchableOpacity } from 'react-native';
import { useTheme } from '@theme/ThemeProvider';
import moment from 'moment';
import { GetTilte } from '@store/Reflictions/reflictionSlice';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { RnSpinner, SectionsList, } from '@components';
import { Header, PraySection, BarCallendar, QuranSection, DailySection } from './Lib';
import { stateReadSelector, GetQuranDaily, GetQuran, quranSelector, loadingDailyQuranSelector } from '@store/Sections/quranSlice';
import { GetDataIndicatior } from '@store/landingData/landingSlice';
import { useSelector, useDispatch, batch } from 'react-redux';
import { HomeParamsList } from '../../../Navigation/paramsList';
import { StackNavigationProp } from '@react-navigation/stack';
import { logout } from '@store/User/userAction';
import styles from './style';
import {
  dateSelector,
  GetFardList,
  tarawehSelector,
  sectionSelector,
  statusCheckSelector,
  setStatus,
  loadingSalahSelector,
  loadingGetSelector,
} from '@store/Sections/praySlice';
import { GetTasksList, loadingGetTasksSelector, statusDailySelector, tasksListSelector } from '@store/Sections/dailySlice';
import { theme } from '@theme/style';
import { ReloadButton } from '@components/reloadButton/component';
interface Props {
  navigation: StackNavigationProp<HomeParamsList, 'Home'>;
}

export const Home: React.FC<Props> = ({ navigation }) => {
  const { colors } = useTheme();
  const dispatch = useDispatch();
  const currentSection = useSelector(sectionSelector);
  const statusDaily = useSelector(statusDailySelector);
  const statusQuran = useSelector(stateReadSelector);
  const statusPray = useSelector(statusCheckSelector);
  const value = useSelector(dateSelector);
  const taraweh = useSelector(tarawehSelector);
  const loadingSalah = useSelector(loadingSalahSelector);
  const loadingDailyQuran = useSelector(loadingDailyQuranSelector);
  const loadingTasksList = useSelector(loadingGetTasksSelector);
  const loadingFardList = useSelector(loadingGetSelector);
  //
  const trakerQuran = useSelector(quranSelector);
  const tasks = useSelector(tasksListSelector);


  const [date] = React.useState<any>(moment());
  const encodeDate = encodeURIComponent(date.format(''));
  const checkRemove = async () => {
    try {
      const v = await AsyncStorage.getItem('@expirs');
      if (v === null) {
        dispatch(logout());
      }
    } catch (e) {
      console.log('rerr');
    }
  };
  React.useEffect(() => {
    batch(() => {
      dispatch(GetQuranDaily(value.format()));
      dispatch(GetTasksList(value.format()));
      dispatch(GetFardList(value.format()));
    });
  }, [dispatch, encodeDate, value]);
  React.useEffect(() => {
    batch(() => {
      dispatch(GetDataIndicatior(encodeDate));
      dispatch(setStatus(''));
      checkRemove();
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch, encodeDate, statusDaily, statusQuran, statusPray]);
  React.useEffect(() => {
    batch(() => {
      dispatch(GetQuran());
      dispatch(GetTilte(encodeDate));
    });
  }, [dispatch, encodeDate]);

  const renderContent = () => <ScrollView

    contentContainerStyle={styles.containerScroll}>
    <SectionsList />

    <View style={styles.sectionsContainer}>
      {currentSection.id === 1 || currentSection === '1' ? (
        <PraySection progressValue={taraweh?.value / 20} />
      ) : currentSection.id === 2 ? (
        <QuranSection />
      ) : (
        <DailySection />
      )}
    </View>
  </ScrollView>


  const renderReload = () => <ReloadButton
    onPress={() => batch(() => {
      dispatch(GetQuranDaily(value.format()));
      dispatch(GetTasksList(value.format()));
      dispatch(GetFardList(value.format()));
    })}
  />
  return (
    <SafeAreaView
      style={{
        ...styles.container,
        backgroundColor: colors.background,
      }}>
      <Header
        onPressCalendar={() => navigation.navigate('Calendar')}
        onPressTidbit={() => navigation.navigate('Tidbits')}
        onPressFav={() => navigation.navigate('Favorites')}
      />
      <View style={{ paddingTop: 22 }}>
        <BarCallendar />
      </View>
      {(loadingSalah) && <View style={{
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        right: '50%',
        top: '70%'

      }}>
        <RnSpinner size="xl" color="#75AECC" />
      </View>}

      {(loadingDailyQuran || loadingFardList || loadingTasksList) ? <View style={{
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        right: '50%',
        top: '70%'

      }}>
        <RnSpinner size="xl" color="#75AECC" />
      </View> :
        (!tasks || !taraweh || !trakerQuran ||
          (tasks && tasks.length === 0)
          // ||(taraweh && taraweh.length === 0)
        ) ?
          renderReload() :
          renderContent()

      }
    </SafeAreaView >
  );
};
