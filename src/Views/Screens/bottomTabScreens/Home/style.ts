import { StyleSheet, Platform } from 'react-native';
import { theme, wp, width, hp } from '@theme/style';
import FontType from '@theme/fonts';

// const deviceWidth = Dimensions.get('window').width;

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    justifyContent: 'space-around',
    alignItems: 'flex-start',
    flexDirection: 'row',
    paddingVertical: 12,
  },
  favIcon: {
    top: wp(9),
    right: wp(2),
    marginRight: 12,
  },
  textRead: {
    color: '#96A782',
    fontFamily: FontType.Bold,
    fontSize: 20,
    // paddingRight: 5,
    // bottom: 3,

  },
  MinStyle: {
    color: '#96A782',
    top: 7,
    right: 15,
    fontFamily: FontType.Bold,
  },
  MinsStyle: {
    color: '#96A782',
    top: 7,
    paddingRight: 20,
    textAlign: 'center',
    fontFamily: FontType.Bold,
  },
  borderTimeStyle: {
    borderLeftWidth: 0.5,
    borderTopWidth: 0.5,
    height: 35,
    borderRightWidth: 0.5,
    borderColor: '#96A782',
    flexDirection: 'row',
    width: width / 2,
    justifyContent: 'center',
  },
  bookmarkedContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 8,
  },
  progressQuran: {
    alignSelf: 'center',
    marginTop: 10,
    marginBottom: 5,
  },
  quranCounter: {
    backgroundColor: '#96A782',
    width: 69,
    alignItems: 'center',
    height: 50,
  },
  typesQuranText: {
    fontFamily: FontType.Bold,
    paddingTop: 10,
  },
  bookmarkedText: {
    color: '#96A782',
    fontFamily: FontType.Lie,
    textAlign: 'center',
  },
  quranContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  sectionsContainer: {
    paddingTop: 18,
    paddingBottom: 15,
  },
  chartContainer: {
    paddingBottom: 20,
    flexDirection: 'row',
    paddingHorizontal: 20,
  },
  praysectionContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'flex-start',
    paddingTop: 5,
    paddingBottom: 20,
  },
  tarawehText: {
    paddingLeft: 10,
    fontFamily: FontType.Medium,
  },
  tarawehNum: {
    position: 'absolute',
    right: wp(10),
    fontFamily: FontType.Bold,
  },
  lineRead: {
    borderColor: '#96A782',
    borderWidth: 1,
    bottom: 7,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    width: width / 1.2,
  },
  quiamText: {
    fontFamily: FontType.Bold,
    paddingTop: 10,
  },
  callendarHeader: {
    justifyContent: 'space-evenly',
    paddingTop: 20,
    paddingBottom: 10,
    flexDirection: 'row',
    paddingHorizontal: 24,
    left: 4,
  },
  dayText: {
    fontFamily: FontType.Bold,
    right: wp(6),
    textAlignVertical: 'center',
  },
  textDead: {
    fontFamily: FontType.Medium,
    textAlign: 'left',
    lineHeight: 20,
    width: wp(40),
    paddingTop: Platform.OS === 'ios' ? 15 : 5,
  },
  containerScroll: {
    paddingVertical: theme.spacing.m,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  rectangle: {
    width: wp(61),
    height: 110,
    borderRadius: 20,
    right: 10,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  square: {
    width: 50,
    height: 50,
  },
  selectorTaraweh: {
    width: 100 * 2.6,
    height: 25,
    backgroundColor: 'transparent',
    borderRadius: 20,
    borderWidth: 1,
    borderColor: '#DFBE84',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 18,
  },
  qiyamShape: {
    width: 70,
    height: 100,

    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 25,
    borderColor: '#DFBE84',
    backgroundColor: 'transparent',
  },
  quranShapes: {
    width: 70,
    height: 100,

    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 25,
    borderColor: '#96A782',
    backgroundColor: 'transparent',
  },
  callenderShape: {
    width: wp(90),
    height: 40,
    borderRadius: 20,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    alignSelf: 'center',
  },
  backCalendarIcon: {
    alignItems: 'center',
    width: 45,
    height: 45,
    borderRadius: 50,
    left: 10,
  },
  indicatorButton: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  barCalendar: {
    height: '60%',
    width: 1,
  },
  input: {
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    top: Platform.OS === 'ios' ? 0 : 3,
  },
  loader: {
    position: 'absolute',
    height: hp(50),
    top: 0.5,
    left: -17,
    backgroundColor: 'transparent',
    width: width,
    borderRadius: 10,
  },
  loaderContainer: {
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
  },
  day: {
    textAlign: 'center',
    fontFamily: FontType.Bold,
  },
  TaText: {
    fontFamily: FontType.Bold,
  },
  PQ: {
    color: '#D3D3D3',
    fontFamily: FontType.Bold,
  },
  juz: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 10,
  },
  GeorgianDiv: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  HijriDiv: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
