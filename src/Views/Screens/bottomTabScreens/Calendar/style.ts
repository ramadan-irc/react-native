import {StyleSheet, Platform} from 'react-native';
import {wp, width} from '@theme/style';
import FontType from '@theme/fonts';
export default StyleSheet.create({
  containerItems: {
    justifyContent: 'flex-start',
    flexDirection: 'row',
    alignItems: 'flex-start',
    paddingBottom: 20,
  },
  Line1: {
    borderBottomColor: '#75AECC',
    borderBottomWidth: 1.5,
    width: width / 4.5,
    left: 8,
    top: 25,
  },

  Line2: {
    borderBottomColor: '#75AECC',
    borderBottomWidth: 1.5,
    width: width / 4.5,
    top: 25,
    right: 8,
  },
  containerIcon: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    paddingVertical: 20,
  },
  dailyText: {
    color: '#75AECC',
    fontFamily: FontType.Bold,
    width: wp(35.7),
    paddingLeft: 15,
    bottom: Platform.OS === 'ios' ? 0 : 10,
    textAlign: 'center',
  },
  dailyText1: {
    color: '#75AECC',
    fontFamily: FontType.Bold,
    width: wp(35.7),
    paddingLeft: 15,
    bottom: Platform.OS === 'ios' ? 0 : 10,
    textAlign: 'center',
  },
  quranRead: {
    color: '#96A782',
    bottom: Platform.OS === 'ios' ? 0 : 10,
    fontFamily: FontType.Bold,
    paddingLeft: 8,
  },
  indicatorButton: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
