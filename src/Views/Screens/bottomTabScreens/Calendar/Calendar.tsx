import React from 'react';
import { ScrollView, SafeAreaView } from 'react-native';
import { Calendar } from '@components';
import { GetMonthProgress } from '@store/landingData/landingSlice';
import { useDispatch } from 'react-redux';
import { useTheme } from '@theme/ThemeProvider';
import MonthProgress from './monthProgress';
import { HomeParamsList } from '../../../Navigation/paramsList';
import { StackNavigationProp } from '@react-navigation/stack';
interface Props {
  navigation: StackNavigationProp<HomeParamsList, 'Calendar'>;
}

export const CalendarScreen: React.FC<Props> = ({ navigation }) => {
  const { colors } = useTheme();

  const dispatch = useDispatch();
  React.useEffect(() => {
    dispatch(GetMonthProgress());
  }, [dispatch]);
  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: colors.background,
      }}>
      <ScrollView contentContainerStyle={{ justifyContent: 'flex-start', alignItems: 'center' }}>
        <Calendar navigation={navigation} onBack={() => navigation.goBack()} />
        <MonthProgress />
      </ScrollView>
    </SafeAreaView>
  );
};
