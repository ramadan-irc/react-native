import React from 'react';
import { Text, RnSpinner } from '@components';
import { wp } from '@theme/style';
import { monthSelector, loadingMonthSelector } from '@store/landingData/landingSlice';
import { DailyCheckListIcon, QuranIcon, PraysIcon } from '@constants';
import { useSelector } from 'react-redux';
import { useTheme } from '@theme/ThemeProvider';
import { View, Text as RText, Platform } from 'react-native';
import styles from './style';
import { useTranslation } from 'react-i18next';
import '@i18n';
import FontType from '@theme/fonts';
interface Props { }
const MonthProgress: React.FC<Props> = () => {
  const { colors } = useTheme();
  const { t, i18n } = useTranslation();
  const loading = useSelector(loadingMonthSelector);
  const month = useSelector(monthSelector);
  const selectedLngCode = i18n.language;
  return !loading && month !== undefined ? (
    <View style={{ flex: 1 }}>
      <View style={styles.containerItems}>
        <View style={styles.Line1} />
        <Text
          size="xl"
          style={{ color: colors.duaaAr, fontFamily: FontType.Lie, paddingHorizontal: 10 }}>
          {t('homePage:Mprogrees')}
        </Text>
        <View style={styles.Line2} />
      </View>
      <View style={styles.containerIcon}>
        <View style={{ alignItems: 'center' }}>
          <PraysIcon width={80} height={80} />
          <RText
            style={{
              color: '#DFBE84',
              fontFamily: FontType.Bold,
              fontSize: 70,
              top: Platform.OS === 'ios' ? 8 : 0,
            }}>
            {month?.sunnahs}
          </RText>
          <Text
            size="m"
            style={{
              color: '#DFBE84',
              fontFamily: FontType.Bold,
              bottom: Platform.OS === 'ios' ? 0 : 10,
            }}>
            {t('homePage:sPrayed')}
          </Text>
        </View>
        <View style={{ alignItems: 'center' }}>
          <QuranIcon width={80} height={80} />
          <RText
            style={{
              color: '#96A782',
              fontFamily: FontType.Bold,
              fontSize: 70,
              top: Platform.OS === 'ios' ? 8 : 0,
            }}>
            {month?.quranTime}
          </RText>
          <Text
            size="m"
            style={{ ...styles.quranRead, width: selectedLngCode === 'en' ? wp(35) : wp(41) }}>
            {t('homePage:TminQuran')}
          </Text>
        </View>
      </View>
      <View style={styles.containerIcon}>
        <View style={{ alignItems: 'center' }}>
          <PraysIcon width={80} height={80} />
          <RText
            style={{
              color: '#DFBE84',
              fontFamily: FontType.Bold,
              fontSize: 70,
              top: Platform.OS === 'ios' ? 8 : 0,
            }}>
            {month?.nafls}
          </RText>
          <Text
            size="m"
            style={{
              color: '#DFBE84',
              fontFamily: FontType.Bold,
              bottom: Platform.OS === 'ios' ? 0 : 10,
            }}>
            {t('homePage:NPrayed')}
          </Text>
        </View>
        <View style={{ alignItems: 'center', left: 10 }}>
          <DailyCheckListIcon width={80} height={80} />
          <RText
            style={{
              color: '#75AECC',
              fontFamily: FontType.Bold,
              fontSize: 70,
              top: Platform.OS === 'ios' ? 8 : 0,
            }}>
            {month?.deedsAccomplished}
          </RText>
          <Text size="m" style={styles.dailyText}>
            {t('homePage:gdeedsf')}
          </Text>
          <Text size="m" style={styles.dailyText1}>
            {t('homePage:gdeedss')}
          </Text>
          <Text size="m" style={styles.dailyText1}>
            {t('homePage:gdeedst')}
          </Text>
        </View>
      </View>
    </View>
  ) : (
    <View style={{ ...styles.indicatorButton, }}>
      <RnSpinner size="xl" color="#75AECC" />
    </View>
  );
};
export default MonthProgress;
