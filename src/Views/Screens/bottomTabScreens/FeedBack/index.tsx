import React from 'react';
import {View, ScrollView, SafeAreaView} from 'react-native';
import {Header} from './header';
import {Body} from './bodyAdd';
import styles from './style';
import moment from 'moment';
import VersionNumber from 'react-native-version-number';
import {useDispatch, useSelector} from 'react-redux';
import {GiveFeedback, setStatusFeedback, StatusFeedbackSelector} from '@store/Profile/profileSlice';
import {useTheme} from '@theme/ThemeProvider';
import {SettingsParamsList} from '../../../Navigation/paramsList';
import {StackNavigationProp} from '@react-navigation/stack';

interface Props {
  navigation: StackNavigationProp<SettingsParamsList, 'feedBack'>;
}
export const ShareFeedback: React.FC<Props> = ({navigation}) => {
  const [body, setBody] = React.useState('');
  const date = moment().format('YYYY-MM-DD');
  const dispatch = useDispatch();
  const feedback = useSelector(StatusFeedbackSelector);
  const {colors} = useTheme();

  const _fetchFeedback = React.useCallback(async () => {
    dispatch(GiveFeedback(VersionNumber.appVersion, body, date));
  }, [dispatch, body, date]);
  React.useEffect(() => {
    if (feedback !== '') {
      navigation.navigate('Setting');
    }
    return () => {
      dispatch(setStatusFeedback(''));
    };
  }, [navigation, dispatch, feedback]);
  return (
    <SafeAreaView
      style={{
        ...styles.container,
        backgroundColor: colors.background,
      }}>
      <Header onPress={() => navigation.goBack()} />
      <ScrollView contentContainerStyle={styles.containerScroll}>
        <View style={{paddingVertical: 30}}>
          <Body body={body} setBody={setBody} onPress={_fetchFeedback} />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};
