import React from 'react';
import { MenuItem, OverflowMenu, Button } from '@ui-kitten/components';
import styles from '../../style';
import { setAdjType } from '@store/User/userAction';
import { adjTypeSelector } from '@store/User/userState';
import { useDispatch, useSelector } from 'react-redux';
import { Icon } from '@ui-kitten/components';
import { Text } from '@components';
import FontType from '@theme/fonts';
const Types = [
  { value: '0', label: '0' },
  { value: '+1', label: '+1' },
  { value: '-1', label: '-1' },
];

interface Props { }

export const AdjSelector: React.FC<Props> = () => {
  const dispatch = useDispatch();
  const [visible, setVisible] = React.useState(false);
  const typeAdj = useSelector(adjTypeSelector);
  const IconRnder = () => (
    <Icon style={{ width: 24, height: 24 }} fill={'#656B8D'} name="arrow-down" />
  );
  const renderToggleButton = () => (
    <Button
      accessoryRight={IconRnder}
      style={styles.Buttoncontainer}
      children={() => (
        <Text size="xs" style={{ color: '#656B8D', fontFamily: FontType.Bold }}>
          {typeAdj}
        </Text>
      )}
      onPress={() => setVisible(true)}
    />
  );
  return (
    <OverflowMenu
      visible={visible}
      anchor={renderToggleButton}
      placement="bottom end"
      style={{ width: 45 * 2, }}
      onBackdropPress={() => setVisible(false)}>
      {Types.map((c, key) => {
        return (
          <MenuItem
            title={c.label}
            key={key}
            style={{ backgroundColor: '#F2F2F2' }}
            onPress={() => {
              dispatch(setAdjType(c.value));
              setVisible(false);
            }}
          />
        );
      })}
    </OverflowMenu>
  );
};
