import React from 'react';
import { MenuItem, OverflowMenu, Button } from '@ui-kitten/components';
import styles from '../../style';
import { setCalendarType } from '@store/User/userAction';
import { callndarTypeSelector } from '@store/User/userState';
import { useDispatch, useSelector } from 'react-redux';
import { Icon } from '@ui-kitten/components';
import { useTranslation } from 'react-i18next';
import { Text } from '@components';
import FontType from '@theme/fonts';

const Types = [
  { value: 'Gregorian', label: 'Gregorian' },
  { value: 'Hijri', label: 'Hijri' },
];

interface Props { }

export const CalendarTypeSelector: React.FC<Props> = () => {
  const dispatch = useDispatch();
  const { i18n } = useTranslation();
  const selectedLngCode = i18n.language;
  const [visible, setVisible] = React.useState(false);
  const typeCalendar = useSelector(callndarTypeSelector);
  const IconRnder = () => (
    <Icon style={{ width: 24, height: 24 }} fill={'#656B8D'} name="arrow-down" />
  );
  const renderToggleButton = () => (
    <Button
      accessoryRight={IconRnder}
      style={styles.Buttoncontainer}
      children={() => (
        <Text size="xs" style={{ color: '#656B8D', fontFamily: FontType.Bold }}>
          {typeCalendar === 'Gregorian' && selectedLngCode === 'fr'
            ? 'Grégorien'
            : typeCalendar === 'Gregorian' && selectedLngCode === 'en'
              ? 'Gregorian'
              : typeCalendar}
        </Text>
      )}
      onPress={() => setVisible(true)}
    />
  );
  return (
    <OverflowMenu
      visible={visible}
      anchor={renderToggleButton}
      placement="bottom end"
      style={{ width: 100 }}
      onBackdropPress={() => setVisible(false)}>
      {Types.map((c, key) => {
        return (
          <MenuItem
            title={c.label}
            key={key}
            style={{ backgroundColor: '#F2F2F2', }}
            onPress={() => {
              dispatch(setCalendarType(c.value));
              setVisible(false);
            }}
          />
        );
      })}
    </OverflowMenu>
  );
};
