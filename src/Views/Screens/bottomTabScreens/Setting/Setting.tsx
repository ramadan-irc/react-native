import React, { useState } from 'react';
import { View, Switch, ScrollView, SafeAreaView, Modal } from 'react-native';
import { Text, RnButton, Touchable, RnSpinner } from '@components';
import { useTheme } from '@theme/ThemeProvider';
import { useDispatch, batch, useSelector } from 'react-redux';
import { Header, CalendarTypeSelector, AdjSelector } from './Lib';
import { LoginManager } from 'react-native-fbsdk';
import styles from './style';
import FontType from '@theme/fonts';
import { SetNotify } from '@store/User/userAction';
import { logout, setNotify } from '@store/User/userAction';
import { notifySelector, loadingNotifySelector } from '@store/User/userState';
import { setSectionReset } from '@store/Sections/praySlice';
import { useTranslation } from 'react-i18next';
import '@i18n';
import LanguageSelector from '@i18n/langugeSelector';
import { Divider, Icon } from '@ui-kitten/components';
import { SettingsParamsList } from '../../../Navigation/paramsList';
import { StackNavigationProp } from '@react-navigation/stack';
import { hp, wp } from '@theme/style';

interface Props {
  navigation: StackNavigationProp<SettingsParamsList, 'Setting'>;
}
export const Setting: React.FC<Props> = ({ navigation }) => {
  const { colors, isDarkState, setScheme } = useTheme();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const notify = useSelector(notifySelector);
  const loading = useSelector(loadingNotifySelector);
  const toggleScheme = () => {
    isDarkState ? setScheme('light') : setScheme('dark');
  };
  const [openLogoutModal, setOpenLogoutModal] = useState(false)
  const handleLogoutPress = () => {
    batch(() => {
      dispatch(logout());
      dispatch(setSectionReset());
      logoutWithFacebook();
    });
  }
  const renderLoader = () => {
    return loading ? (
      <View style={styles.loader}>
        <View style={styles.loaderContainer}>
          <RnSpinner size="xl" color="#656B8D" />
        </View>
      </View>
    ) : null;
  };
  const logoutWithFacebook = () => {
    LoginManager.logOut();
  };
  const toggleNotification = () => {
    console.log('pressed')
    batch(async () => {
      await dispatch(SetNotify(!notify));
      // dispatch(setNotify(!notify));
    });
  };
  const LogoutModal = () => <Modal animated
    animationType="fade"
    transparent={true}
    visible={openLogoutModal}
    onRequestClose={() => setOpenLogoutModal(false)}>
    <View style={{ height: '100%', width: '100%', justifyContent: 'flex-end', alignItems: 'center', backgroundColor: 'transparent' }}>
      <View style={{
        zIndex: 1000000000000,
        position: 'absolute',
        bottom: 0,
        height: '30%',
        width: wp(100),
        backgroundColor: '#fff',
        borderRadius: 0,
        borderTopEndRadius: 30,
        borderTopStartRadius: 30,
        paddingHorizontal: wp(5),
        elevation: 8,

      }}>
        <Divider style={{
          width: wp(10),
          alignSelf: 'center',
          marginVertical: hp(2),
          height: 1,
          backgroundColor: '#656B8D'
        }} />

        <View style={{
          flex: 1,
          justifyContent: 'center',

        }}>
          <Text
            size="xl"
            style={{
              color: colors.calendarText, fontFamily: FontType.Regular,
              paddingLeft: 20
              // textAlign: 'center'
            }}>
            {t('settings:checkLogoutText')}
          </Text>
          <View style={styles.containerButton}>

            <RnButton
              appearance="filled"
              children={() => (
                <View style={{ flex: 1 }}>
                  <Text
                    size="l"
                    style={{ color: colors.white, textAlign: 'center', fontFamily: FontType.Bold }}>
                    {t('settings:out')}
                  </Text>
                </View>
              )}
              style={styles.buttonSignout}
              size="large"
              onPress={handleLogoutPress}
            />
            <RnButton
              appearance="filled"
              children={() => (
                <View style={{ flex: 1 }}>
                  <Text
                    size="l"
                    style={{ color: '#75AECC', textAlign: 'center', fontFamily: FontType.Bold, textDecorationLine: 'underline' }}>
                    {t('settings:cancel')}
                  </Text>
                </View>
              )}
              style={{ ...styles.FEEDBACKbutton, backgroundColor: 'transparent', borderWidth: 0 }}
              size="large"
              onPress={() => setOpenLogoutModal(false)}
            />
          </View>
        </View>
      </View>
    </View>
  </Modal>
  return (
    <SafeAreaView
      style={{
        ...styles.container,
        backgroundColor: colors.background,
      }}>
      <Header />
      <ScrollView contentContainerStyle={styles.containerScroll}>
        <Touchable onPress={() => navigation.navigate('Profile')}>
          <View style={styles.containerItems}>
            <Text
              size="m"
              style={{ color: colors.calendarText, fontFamily: FontType.Regular, paddingLeft: 20 }}>
              {t('settings:myProfile')}
            </Text>
            <Icon
              style={{ width: 24, height: 24, right: 20 }}
              fill={colors.arrowIconSettings}
              name="arrow-right"
            />
          </View>
        </Touchable>
        <View style={styles.containerItems}>
          <Text
            size="m"
            style={{ color: colors.calendarText, fontFamily: FontType.Regular, paddingLeft: 20 }}>
            {t('settings:hgC')}
          </Text>
          <View style={{ marginRight: 10 }}>
            <CalendarTypeSelector />
          </View>
        </View>
        <View style={styles.containerItems}>
          <Text
            size="m"
            style={{ color: colors.calendarText, fontFamily: FontType.Regular, paddingLeft: 20 }}>
            {t('settings:dateAdj')}
          </Text>
          <View style={{ marginRight: 10 }}>
            <AdjSelector />
          </View>
        </View>
        <View style={styles.Line} />
        <View style={{ ...styles.containerItems, paddingTop: 20 }}>
          <Text
            size="m"
            style={{ color: colors.calendarText, fontFamily: FontType.Regular, paddingLeft: 20 }}>
            {t('settings:push')}
          </Text>

          <Switch
            value={notify}
            trackColor={{ false: '#F2F2F2', true: '#F2F2F2' }}
            thumbColor={notify ? '#656B8D' : '#D3D3D3'}
            onValueChange={toggleNotification}
            style={{ marginRight: 20 }}
          />
        </View>
        <View style={styles.containerItems}>
          <Text
            size="m"
            style={{ color: colors.calendarText, fontFamily: FontType.Regular, paddingLeft: 20 }}>
            {isDarkState ? t('settings:lMode') : t('settings:dMode')}
          </Text>

          <Switch
            value={isDarkState}
            trackColor={{ false: '#F2F2F2', true: '#F2F2F2' }}
            thumbColor={isDarkState ? '#656B8D' : '#D3D3D3'}
            onValueChange={toggleScheme}
            style={{ marginRight: 20 }}
          />
        </View>

        <View style={{ ...styles.containerItem, paddingBottom: 20 }}>
          <Text
            size="m"
            style={{ color: colors.calendarText, fontFamily: FontType.Regular, paddingLeft: 20 }}>
            {t('settings:lan')}
          </Text>
          <LanguageSelector />
        </View>
        <View style={styles.Line} />
        <Touchable onPress={() => navigation.navigate('appPolicy')}>
          <View style={{ ...styles.containerItems, paddingTop: 20 }}>
            <Text
              size="m"
              style={{ color: colors.calendarText, fontFamily: FontType.Regular, paddingLeft: 20 }}>
              {t('settings:plicy')}
            </Text>
            <Icon
              style={{ width: 24, height: 24, right: 20 }}
              fill={colors.arrowIconSettings}
              name="arrow-right"
            />
          </View>
        </Touchable>
        <Touchable onPress={() => navigation.navigate('termsConditions')}>
          <View style={styles.containerItems}>
            <Text
              size="m"
              style={{ color: colors.calendarText, fontFamily: FontType.Regular, paddingLeft: 20 }}>
              {t('settings:terms')}
            </Text>
            <Icon
              style={{ width: 24, height: 24, right: 20 }}
              fill={colors.arrowIconSettings}
              name="arrow-right"
            />
          </View>
        </Touchable>
        <View style={styles.containerButton}>
          <RnButton
            appearance="filled"
            children={() => (
              <View style={{ flex: 1 }}>
                <Text
                  size="s"
                  style={{ color: '#75AECC', textAlign: 'center', fontFamily: FontType.Bold }}>
                  {t('settings:feedback')}
                </Text>
              </View>
            )}
            style={styles.FEEDBACKbutton}
            size="large"
            onPress={() => navigation.navigate('feedBack')}
          />
          <RnButton
            appearance="filled"
            children={() => (
              <View style={{ flex: 1 }}>
                <Text
                  size="s"
                  style={{ color: colors.white, textAlign: 'center', fontFamily: FontType.Bold }}>
                  {t('settings:out')}
                </Text>
              </View>
            )}
            style={styles.buttonSignout}
            size="large"
            onPress={() => setOpenLogoutModal(true)}
          />
        </View>
        {renderLoader()}
        {LogoutModal()}
      </ScrollView>
    </SafeAreaView>
  );
};
