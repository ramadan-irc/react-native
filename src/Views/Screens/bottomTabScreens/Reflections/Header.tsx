import React from 'react';
import {View} from 'react-native';
import {IslamicWhite, StarRef, PenIcon} from '@constants';
import {Text} from '@components';
import styles from './style';
import {useTheme} from '@theme/ThemeProvider';
import {useTranslation} from 'react-i18next';
import '@i18n';
interface Props {}

export const Header: React.FC<Props> = () => {
  const {colors} = useTheme();
  const {t} = useTranslation();
  return (
    <View style={{...styles.header}}>
      <IslamicWhite width="80" height="80" style={{bottom: 10}} />
      <Text size="xl" style={{...styles.textHeader, color: colors.duaaAr}}>
        {t('homePage:myref')}
      </Text>
      <StarRef width="110" style={styles.starIcon} height="110" />
      <PenIcon width="58" height="58" />
    </View>
  );
};
