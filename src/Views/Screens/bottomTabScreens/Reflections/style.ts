import {StyleSheet} from 'react-native';
import FontType from '@theme/fonts';
import {wp, theme} from '@theme/style';
export default StyleSheet.create({
  header: {
    justifyContent: 'space-around',
    alignItems: 'flex-start',
    flexDirection: 'row',
    paddingVertical: 24,
  },
  textHeader: {
    fontFamily: FontType.Lie,
  },
  container: {
    flex: 1,
  },
  starIcon: {
    position: 'absolute',
    right: wp(6),
  },
  buttonPlus: {
    borderRadius: theme.radius.mid,
    backgroundColor: '#C5E2ED',
    borderColor: '#C5E2ED',
    width: wp(50),
  },
  buttonText: {
    color: '#75AECC',
    textAlign: 'center',
    fontFamily: FontType.Medium,
  },
  buttonContainer: {
    alignSelf: 'flex-start',
    paddingBottom: 18,
    paddingHorizontal: 20,
  },
  indicator: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
});
