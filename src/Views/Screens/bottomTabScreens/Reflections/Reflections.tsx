import React, { useCallback } from 'react';
import { View, SafeAreaView } from 'react-native';
import { Header } from './Header';
import { useTheme } from '@theme/ThemeProvider';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { RnButton, Text, RnSpinner } from '@components';
import { logout } from '@store/User/userAction';
import styles from './style';
import { ReflictionsList } from '@components/Lists/reflictionsList';
import { ReflicationsParamsList } from '../../../Navigation/paramsList';
import { StackNavigationProp } from '@react-navigation/stack';
import { useSelector, useDispatch } from 'react-redux';
import {
  GetReflictions,
  loadingGetSelector,
  reflictionsSelector,
} from '@store/Reflictions/reflictionSlice';
import { useTranslation } from 'react-i18next';
import '@i18n';
import { ReloadButton } from '@components/reloadButton/component';

interface Props {
  navigation: StackNavigationProp<ReflicationsParamsList, 'Reflections'>;
}
export const Reflections: React.FC<Props> = ({ navigation }) => {
  const { colors } = useTheme();
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const loading = useSelector(loadingGetSelector);
  const refliction = useSelector(reflictionsSelector);
  const _fetchRefliction = useCallback(async () => {
    dispatch(GetReflictions());
  }, [dispatch]);
  const checkRemove = async () => {
    try {
      const v = await AsyncStorage.getItem('@expirs');
      if (v === null) {
        dispatch(logout());
      }
    } catch (e) {
      console.log('rerr');
    }
  };
  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      _fetchRefliction();
      checkRemove();
    });
    return unsubscribe;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [_fetchRefliction, navigation]);

  const renderReload = () => <ReloadButton
    onPress={() => _fetchRefliction()}
  />
  const renderContent = () => <ReflictionsList data={refliction} />
  return (
    <SafeAreaView
      style={{
        ...styles.container,
        backgroundColor: colors.background,
      }}>
      <Header />
      <View style={styles.buttonContainer}>
        <RnButton
          appearance="filled"
          children={() => (
            <View style={{ flex: 1 }}>
              <Text
                size="s"
                style={{
                  ...styles.buttonText,
                }}>
                + {t('homePage:Newref')}
              </Text>
            </View>
          )}
          style={styles.buttonPlus}
          size="large"
          onPress={() => navigation.navigate('NewReflications')}
        />
      </View>

      {(loading) ? <View style={{
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        right: '50%',
        top: '70%'

      }}>
        <RnSpinner size="xl" color="#75AECC" />
      </View>
        : (!refliction || (refliction && refliction.length === 0)) ?
          renderReload()
          : renderContent()
      }
    </SafeAreaView>
  )
};
