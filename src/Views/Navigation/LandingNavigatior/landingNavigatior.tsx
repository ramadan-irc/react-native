import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {LandingPage} from '@screens/Auth';
import {useTheme} from '@theme/ThemeProvider';
import {LandingParamsList} from '../paramsList';
const AuthStack = createStackNavigator<LandingParamsList>();

export const LandingStackScreen = () => {
  const {colors} = useTheme();
  return (
    <AuthStack.Navigator
      headerMode="none"
      initialRouteName="Landing"
      screenOptions={{cardStyle: {backgroundColor: colors.background}}}>
      <AuthStack.Screen name="Landing" component={LandingPage} />
    </AuthStack.Navigator>
  );
};
