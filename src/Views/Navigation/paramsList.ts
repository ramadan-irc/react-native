import {StackNavigationProp} from '@react-navigation/stack';
import {DrawerNavigationProp} from '@react-navigation/drawer';
import {BottomTabNavigationProp} from '@react-navigation/bottom-tabs';

export type AuthParamsList = {
  Login: undefined;
  Register: undefined;
  Drawer: undefined;
  Pledge: undefined;
  ForgetPassword: undefined;
};
export type LandingParamsList = {
  Landing: undefined;
};
export type SettingsParamsList = {
  Setting: undefined;
  Profile: undefined;
  appPolicy: undefined;
  termsConditions: undefined;
  feedBack: undefined;
};
export type ReflicationsParamsList = {
  Reflections: undefined;
  Deatails: undefined;
  NewReflications: undefined;
};
export type HomeParamsList = {
  Home: undefined;
  Calendar: undefined;
  Tidbits: undefined;
  Favorites: undefined;
};

export type BottomTabList = {
  HomeStack: undefined;
  ReflectionsStack: undefined;
  Duaa: undefined;
  SettingsStack: undefined;
  Donate: undefined;
};

export type DrawerParamsList = {
  Inspiration: undefined;
  Favorites: undefined;
  Journal: undefined;
  App: undefined;
};
export type authNavigationProps<T extends keyof AuthParamsList> = {
  navigation: StackNavigationProp<AuthParamsList, T>;
};
export type LandingNavigationProps<T extends keyof LandingParamsList> = {
  navigation: StackNavigationProp<LandingParamsList, T>;
};
export type settingNavigationProps<T extends keyof SettingsParamsList> = {
  navigation: StackNavigationProp<SettingsParamsList, T>;
};
export type bottomNavigationProps<T extends keyof BottomTabList> = {
  navigation: BottomTabNavigationProp<BottomTabList, T>;
};
export type drawerNavigationProps<T extends keyof DrawerParamsList> = {
  navigation: DrawerNavigationProp<DrawerParamsList, T>;
};
