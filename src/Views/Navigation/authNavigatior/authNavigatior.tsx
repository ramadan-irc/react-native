import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Login, Register, ForgetPassword} from '@screens/Auth';
import {useTheme} from '@theme/ThemeProvider';
import {AuthParamsList} from '../paramsList';
const AuthStack = createStackNavigator<AuthParamsList>();

export const AuthStackScreen = () => {
  const {colors} = useTheme();

  return (
    <AuthStack.Navigator
      headerMode="none"
      initialRouteName="Login"
      screenOptions={{cardStyle: {backgroundColor: colors.background}}}>
      <AuthStack.Screen name="Login" component={Login} />
      <AuthStack.Screen name="Register" component={Register} />
      {/* <AuthStack.Screen name="Pledge" component={Pledge} /> */}
      <AuthStack.Screen name="ForgetPassword" component={ForgetPassword} />
    </AuthStack.Navigator>
  );
};
