import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {IsLangNavigator} from '../isLangNavigatior';
import {tabNavigatior} from '../bottomNavigatior';
import {authenticatedSelector} from '@store/User/userState';
import {useSelector} from 'react-redux';
const RootStack = createStackNavigator();

export const RootNavigator: React.FC = () => {
  const isAuthenticated = useSelector(authenticatedSelector);
  return (
    <RootStack.Navigator>
      {!isAuthenticated ? (
        <RootStack.Screen
          name="Auth"
          component={IsLangNavigator}
          options={{
            animationEnabled: false,
            headerShown: false,
          }}
        />
      ) : (
        <RootStack.Screen
          name="App"
          component={tabNavigatior}
          options={{
            animationEnabled: false,
            headerShown: false,
          }}
        />
      )}
    </RootStack.Navigator>
  );
};
