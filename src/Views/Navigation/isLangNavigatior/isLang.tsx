import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {AuthStackScreen} from '../authNavigatior';
import {LandingStackScreen} from '../LandingNavigatior';
import {isLangSelected} from '@store/User/userState';
import {useSelector} from 'react-redux';
const RootStack = createStackNavigator();

export const IsLangNavigator: React.FC = () => {
  const isLang = useSelector(isLangSelected);

  return (
    <RootStack.Navigator headerMode="none">
      {isLang ? (
        <RootStack.Screen
          name="Auth"
          component={AuthStackScreen}
          options={{
            animationEnabled: false,
          }}
        />
      ) : (
        <RootStack.Screen
          name="Landing"
          component={LandingStackScreen}
          options={{
            animationEnabled: false,
          }}
        />
      )}
    </RootStack.Navigator>
  );
};
