import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {DrawerParamsList} from '../paramsList';
import {Inspiration, Favorites, Journal} from '@screens/drawerScreens';
import {tabNavigatior} from '../bottomNavigatior';
const Drawer = createDrawerNavigator<DrawerParamsList>();
export const DrawerNav = () => {
  return (
    <Drawer.Navigator initialRouteName="App" keyboardDismissMode="on-drag" overlayColor="0">
      <Drawer.Screen name="App" component={tabNavigatior} />
      <Drawer.Screen name="Inspiration" component={Inspiration} />
      <Drawer.Screen name="Favorites" component={Favorites} />
      <Drawer.Screen name="Journal" component={Journal} />
    </Drawer.Navigator>
  );
};
