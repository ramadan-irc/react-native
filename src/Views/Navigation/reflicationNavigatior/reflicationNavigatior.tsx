import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Reflections, Deatails, NewReflections} from '@screens/bottomTabScreens';
import {useTheme} from '@theme/ThemeProvider';
import {ReflicationsParamsList} from '../paramsList';
const AuthStack = createStackNavigator<ReflicationsParamsList>();

export const ReflicationsStackScreen = () => {
  const {colors} = useTheme();
  return (
    <AuthStack.Navigator
      headerMode="none"
      initialRouteName="Reflections"
      screenOptions={{cardStyle: {backgroundColor: colors.background}}}>
      <AuthStack.Screen name="Reflections" component={Reflections} />
      <AuthStack.Screen name="Deatails" component={Deatails} />
      <AuthStack.Screen name="NewReflications" component={NewReflections} />
    </AuthStack.Navigator>
  );
};
