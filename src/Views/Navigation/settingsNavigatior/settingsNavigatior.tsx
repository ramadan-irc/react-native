import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Setting, Profile, AppPolicy, Terms, ShareFeedback} from '@screens/bottomTabScreens';
import {useTheme} from '@theme/ThemeProvider';
import {SettingsParamsList} from '../paramsList';
const AuthStack = createStackNavigator<SettingsParamsList>();

export const SettingStackScreen = () => {
  const {colors} = useTheme();
  return (
    <AuthStack.Navigator
      headerMode="none"
      initialRouteName="Setting"
      screenOptions={{cardStyle: {backgroundColor: colors.background}}}>
      <AuthStack.Screen name="Setting" component={Setting} />
      <AuthStack.Screen name="Profile" component={Profile} />
      <AuthStack.Screen name="appPolicy" component={AppPolicy} />
      <AuthStack.Screen name="termsConditions" component={Terms} />
      <AuthStack.Screen name="feedBack" component={ShareFeedback} />
    </AuthStack.Navigator>
  );
};
