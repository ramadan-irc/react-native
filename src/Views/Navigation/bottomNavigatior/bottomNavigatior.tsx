import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { View, StyleSheet, Platform } from 'react-native';
import { wp, hp } from '@theme/style';
import { BottomTabList } from '../paramsList';
import {
  HomeUn,
  DuuUnFill,
  RifUnFill,
  Dollar,
  SettingsUnFill,
  HomeFill,
  DuuFill,
  SettingsFill,
  RifFill,
} from '@constants';
import { Duaa, Donate } from '@screens/bottomTabScreens';
import { SettingStackScreen } from '../settingsNavigatior';
import { HomeStackScreen } from '../homeNavigatior';
import { ReflicationsStackScreen } from '../reflicationNavigatior';
import { useTheme } from '@theme/ThemeProvider';
import { SvgXml } from 'react-native-svg';
import { DuuFill_dark, DuuUnFill_dark, HomeFill_dark, HomeUn_dark, RifFill_dark, RifUnFill_dark, SettingsFill_dark, SettingsUnFill_dark } from '@constants/svg';

const Tabs = createBottomTabNavigator<BottomTabList>();

export const tabNavigatior = () => {
  const { colors, isDarkState } = useTheme();

  return (
    <Tabs.Navigator
      initialRouteName="HomeStack"
      backBehavior="history"
      lazy={true}
      tabBarOptions={{
        showLabel: false,
        keyboardHidesTabBar: true,

        style: {
          backgroundColor: colors.background,
        },
      }}
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused }) => {
          if (route.name === 'HomeStack' && focused) {
            return isDarkState ? <SvgXml xml={HomeFill_dark} width={30} height={30} /> : <HomeFill width={30} height={30} />;
          } else if (route.name === 'HomeStack') {
            return isDarkState ? <SvgXml xml={HomeUn_dark} width={20} height={20} /> : <HomeUn width={20} height={20} />;
          }
          if (route.name === 'Donate') {
            return (
              <View style={styles.containerCoinIco}>
                <Dollar width={25} height={25} />
              </View>
            );
          }
          if (route.name === 'Duaa' && focused) {
            return isDarkState ? <SvgXml xml={DuuFill_dark} width={30} height={30} /> : <DuuFill width={30} height={30} />;
          } else if (route.name === 'Duaa') {
            return isDarkState ? <SvgXml xml={DuuUnFill_dark} width={20} height={20} /> : <DuuUnFill width={20} height={20} />;
          }
          if (route.name === 'ReflectionsStack' && focused) {
            return isDarkState ? <SvgXml xml={RifFill_dark} width={30} height={30} /> : <RifFill width={30} height={30} />;
          } else if (route.name === 'ReflectionsStack') {
            return isDarkState ? <SvgXml xml={RifUnFill_dark} width={20} height={20} /> : <RifUnFill width={20} height={20} />;
          }
          if (route.name === 'SettingsStack' && focused) {
            return isDarkState ? <SvgXml xml={SettingsFill_dark} width={30} height={30} /> : <SettingsFill width={30} height={30} />;
          } else if (route.name === 'SettingsStack') {
            return isDarkState ? <SvgXml xml={SettingsUnFill_dark} width={20} height={20} /> : <SettingsUnFill width={20} height={20} />;
          }
        },
      })}>
      <Tabs.Screen name="ReflectionsStack" component={ReflicationsStackScreen} />
      <Tabs.Screen name="Duaa" component={Duaa} />
      <Tabs.Screen name="HomeStack" component={HomeStackScreen} />
      <Tabs.Screen name="SettingsStack" component={SettingStackScreen} />
      <Tabs.Screen name="Donate" component={Donate} />
    </Tabs.Navigator>
  );
};
const styles = StyleSheet.create({
  containerCoinIco: {
    backgroundColor: '#E29591',
    height: Platform.OS === 'ios' ? hp(5.5) : hp(6.5),
    width: wp(18),
    justifyContent: 'center',
    marginLeft: 12,
    alignItems: 'center',
  },
});
