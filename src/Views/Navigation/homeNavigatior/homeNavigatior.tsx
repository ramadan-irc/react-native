import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Home, CalendarScreen, Tidbits, Favorites} from '@screens/bottomTabScreens';
import {useTheme} from '@theme/ThemeProvider';
import {HomeParamsList} from '../paramsList';
const AuthStack = createStackNavigator<HomeParamsList>();

export const HomeStackScreen = () => {
  const {colors} = useTheme();
  return (
    <AuthStack.Navigator
      headerMode="none"
      initialRouteName="Home"
      screenOptions={{cardStyle: {backgroundColor: colors.background}}}>
      <AuthStack.Screen name="Home" component={Home} />
      <AuthStack.Screen name="Calendar" component={CalendarScreen} />
      <AuthStack.Screen name="Tidbits" component={Tidbits} />
      <AuthStack.Screen name="Favorites" component={Favorites} />
    </AuthStack.Navigator>
  );
};
