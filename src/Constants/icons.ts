import COIN from '@assets/icon/Coin.svg';
import HOME from '@assets/icon/Home.svg';
import SETTINGS from '@assets/icon/Settings.svg';
import DUAA from '@assets/icon/Duaa.svg';
import REFLECTION from '@assets/icon/Reflection.svg';
import Test from '@assets/icon/test.svg';
import DEADIco from '@assets/icon/deadofday.svg';
import ISLAMICIco from '@assets/icon/isalmicicon.svg';
import FAV from '@assets/icon/Fav.svg';
import CALLENDERIcon from '@assets/icon/callenderIcon.svg';
import BookMark from '@assets/icon/Bookmark.svg';
import LogoAction from '@assets/icon/LogoIcon.svg';
import BackIcon from '@assets/icon/backIcon.svg';
import CalendarIcon from '@assets/icon/calendarIcon.svg';
import HijriIcon from '@assets/icon/HijriIcon.svg';
import PraysIcon from '@assets/icon/Pray.svg';
import QuranIcon from '@assets/icon/Quran.svg';
import DailyCheckListIcon from '@assets/icon/CheckList.svg';
import StarsIcon from '@assets/icon/StartsIcon.svg';
import FavFill from '@assets/icon/FavFill.svg';
import FavUnFill from '@assets/icon/FavUnfill.svg';
import PenIcon from '@assets/icon/Pencel.svg';
import SemiCIcon from '@assets/icon/Semiclom.svg';
import DuasIcon from '@assets/icon/DuasIcon.svg';
import IslamicWhite from '@assets/icon/islamicWhite.svg';
import EditPen from '@assets/icon/editPencel.svg';
import StarRef from '@assets/icon/startRef.svg';
import ArrowRef from '@assets/icon/arrowref.svg';
import BackWhite from '@assets/icon/backWhite.svg';
import WhiteC from '@assets/icon/callendarWhite.svg';
import NewIslamic from '@assets/icon/NewIslamic.svg';
import MyDeeds from '@assets/icon/MyDeeds.svg';
import WhiteDeeds from '@assets/icon/WhiteDeeds.svg';
import Dollar from '@assets/icon/dolarNew.svg';
import HomeFill from '@assets/icon/homefill.svg';
import HomeUn from '@assets/icon/homeun.svg';
import SettingsFill from '@assets/icon/settingfill.svg';
import SettingsUnFill from '@assets/icon/settingun.svg';
import RifFill from '@assets/icon/riffill.svg';
import RifUnFill from '@assets/icon/rifun.svg';
import DuuFill from '@assets/icon/duaafill.svg';
import DuuUnFill from '@assets/icon/duaaun.svg';
import FlibStars from '@assets/icon/flibStar.svg';
import MyDeedsFr from '@assets/icon/DeedFr.svg';
import MyDeedsFrWh from '@assets/icon/frMyDeeds.svg';
import CaWhite from '@assets/icon/CAWhite.svg';
export {
  COIN,
  HOME,
  SETTINGS,
  DUAA,
  REFLECTION,
  Test,
  DEADIco,
  ISLAMICIco,
  FAV,
  CALLENDERIcon,
  BookMark,
  LogoAction,
  BackIcon,
  CalendarIcon,
  HijriIcon,
  PraysIcon,
  QuranIcon,
  DailyCheckListIcon,
  StarsIcon,
  FavFill,
  FavUnFill,
  PenIcon,
  SemiCIcon,
  DuasIcon,
  IslamicWhite,
  EditPen,
  StarRef,
  ArrowRef,
  BackWhite,
  WhiteC,
  NewIslamic,
  MyDeeds,
  WhiteDeeds,
  Dollar,
  HomeFill,
  HomeUn,
  SettingsFill,
  SettingsUnFill,
  RifFill,
  RifUnFill,
  DuuFill,
  DuuUnFill,
  FlibStars,
  MyDeedsFr,
  MyDeedsFrWh,
  CaWhite,
};
