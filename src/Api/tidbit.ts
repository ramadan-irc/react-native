import api from './api';
import { Alert } from 'react-native';
import Toast from 'react-native-root-toast';
import FontType from '@theme/fonts';
import MyToast from '@components/Toast/component';
export const getTidbit = async () => {
  const result: any = await api.get('/tidbit/mytidbits').then((r) => r.data);
  if (result === null) {
    throw MyToast('Sorry, network request failed')
    //throw Alert.alert('Sorry, network request failed');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};
export const getTidbitFav = async () => {
  const result: any = await api.get('/tidbit/favorite').then((r) => r.data);
  if (result === null) {
    throw MyToast('Sorry, network request failed')
    //throw Alert.alert('Sorry, network request failed');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};

export const addFavTidbit = async (id: any) => {
  const result: any = await api.post('/tidbit/favorite/add', { id }).then((r) => r.data);
  if (result === null) {
    throw MyToast('Sorry, network request failed')
    //throw Alert.alert('Sorry, network request failed');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};

export const removeFavTidbit = async (id: any) => {
  const result: any = await api.get('/tidbit/favorite/delete/' + id).then((r) => r.data);
  if (result === null) {
    throw MyToast('Sorry, network request failed')
    //throw Alert.alert('Sorry, network request failed');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};
