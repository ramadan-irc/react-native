import api from './api';
// import R from 'reactotron-react-native';
import { Alert } from 'react-native';
import Toast from 'react-native-root-toast';
import FontType from '@theme/fonts';
import MyToast from '@components/Toast/component';
export const loginUser = async (
  id: any,
  email: string,
  username: any,
  password: any,
  registrationToken: any,
  date: any,
  language: any,
) => {
  const result: any = await api
    .post('/user/login', {
      id,
      email,
      username,
      password,
      registrationToken,
      date,
      language,
    })
    .then((r) => r.data)
    .catch((e) => e.data);
  if (result === null) {
    throw MyToast('Sorry, network request failed')
    //throw Alert.alert('Sorry, network request failed');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};

export const registerUser = async (
  username: any,
  email: any,
  password: any,
  language: any,
  registrationToken: any,
  date: any,
) => {
  const result: any = await api
    .post('/user/register', {
      username,
      email,
      password,
      language,
      registrationToken,
      date,
    })
    .then((r) => r.data)
    .catch((e) => e.data);
  if (result === null) {
    throw MyToast('Sorry, network request failed')
    //throw Alert.alert('Sorry, network request failed');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};

export const recoverPassword = async (email: any) => {
  const result: any = await api
    .post('/user/forget-password', {
      email,
    })
    .then((r) => r.data);

  if (result === null) {
    throw MyToast('Sorry, network request failed')
    //throw Alert.alert('Sorry, network request failed');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};

export const setNotifyUser = async (value: any) => {
  const result: any = await api
    .post('/user/notify', { value })
    .then((r) => r.data)
    .catch((e) => e.data);
  if (result === null) {
    throw MyToast('Sorry, network request failed')
    //throw Alert.alert('Sorry, network request failed');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};
export const setLang = async (language: any) => {
  const result: any = await api
    .post('/user/language', { language })
    .then((r) => r.data)
    .catch((e) => e.data);
  if (result === null) {
    throw MyToast('Sorry, network request failed')
    //throw Alert.alert('Sorry, network request failed');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};
