import api from './api';
import { Alert } from 'react-native';
import Toast from 'react-native-root-toast';
import FontType from '@theme/fonts';
import MyToast from '@components/Toast/component';

export const getDeed = async (date: any) => {
  const result: any = await api
    .get('/deedoftheday?date=' + date)
    .then((r) => r.data)
    .catch((e) => console.log(e));
  if (result === null) {
    throw console.log('err');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};

export const getMonthProgress = async () => {
  const result: any = await api
    .get('/progress')
    .then((r) => r.data)
    .catch((e) => console.log(e.data));
  if (result === null) {
    throw MyToast('Sorry, network request failed')
    //throw Alert.alert('Sorry, network request failed');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};

export const getIndicatiorsData = async (date: any) => {
  const result: any = await api
    .get('/indicators/?date=' + date)
    .then((r) => r.data)
    .catch((e) => console.log(e));
  if (result === null) {
    throw console.log('err');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};
export const getRefreshToken = async (refreshToken: string) => {
  const result: any = await api
    .get(
      '/token',
      {},
      {
        headers: {
          refreshToken,
        },
      },
    )
    .catch((e) => console.log(e.data));
  if (result === null) {
    throw MyToast('Sorry, network request failed')
    //throw Alert.alert('Sorry, network request failed');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};
