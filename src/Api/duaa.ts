import api from './api';
import { Alert } from 'react-native';
import Toast from 'react-native-root-toast';
import FontType from '@theme/fonts';
import MyToast from '@components/Toast/component';
export const getDuaa = async () => {
  const result: any = await api
    .get('/dua/myduas')
    .then((r) => r.data)
    .catch((e) => e.data);
  if (result === null) {
    throw MyToast('Sorry, network request failed')
    //throw Alert.alert('Sorry, network request failed');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};
export const getDuaaFav = async () => {
  const result: any = await api
    .get('/dua/favorite')
    .then((r) => r.data)
    .catch((e) => e.data);
  if (result === null) {
    throw MyToast('Sorry, network request failed')
    //throw Alert.alert('Sorry, network request failed');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};

export const addFavDuaa = async (id: any) => {
  const result: any = await api
    .post('/dua/favorite/add', { id })
    .then((r) => r.data)
    .catch((e) => e.data);
  if (result === null) {
    throw MyToast('Sorry, network request failed')
    //throw Alert.alert('Sorry, network request failed');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};

export const removeFavDuaa = async (id: any) => {
  const result: any = await api
    .get('/dua/favorite/delete/' + id)
    .then((r) => r.data)
    .catch((e) => e.data);
  if (result === null) {
    throw MyToast('Sorry, network request failed')
    //throw Alert.alert('Sorry, network request failed');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};
