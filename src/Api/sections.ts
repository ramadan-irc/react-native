import api from './api';
import { Alert } from 'react-native';
import Toast from 'react-native-root-toast';
import FontType from '@theme/fonts';
import MyToast from '@components/Toast/component';
export const getPraySections = async (value: any) => {
  const result: any = await api
    .post('/prayer/myprayers', { value })
    .then((r) => r.data)
    .catch((e) => console.log(e));
  if (result === null) {
    throw MyToast('Sorry, network request failed')
    //throw Alert.alert('Sorry, network request failed');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};

export const checkPray = async (prayers: any, date: any) => {
  const result: any = await api.post('/prayer/check', { prayers, date }).then((r) => r.data);
  if (result === null) {
    throw Alert.alert('Sorry could not save your change, please check your network connection');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};
export const checkSalah = async (prayers: any, date: any) => {
  const result: any = await api
    .post('/prayer/check', { prayers, date })
    .then((r) => r.data)
    .catch((e) => e.data);
  if (result === null) {
    throw Alert.alert('Sorry could not save your change, please check your network connection');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};

export const getDailyTasks = async (value: any) => {
  const result: any = await api
    .post('/task/mytasks', { value })
    .then((r) => r.data)
    .catch((e) => console.log(e.data));
  if (result === null) {
    throw MyToast('Sorry, network request failed')
    //throw Alert.alert('Sorry, network request failed');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};

export const checkTask = async (tasks: any, date: any) => {
  const result: any = await api
    .post('/task/check', { tasks, date })
    .then((r) => r.data)
    .catch((e) => e.data);
  if (result === null) {
    throw Alert.alert('Sorry could not save your change, please check your network connection');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};

export const getDailyQuran = async (value: any) => {
  const result: any = await api
    .post('/quran/daily', { value })
    .then((r) => r.data)
    .catch((e) => console.log(e.data));
  if (result === null) {
    throw MyToast('Sorry, network request failed')
    //throw Alert.alert('Sorry, network request failed');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};
export const getQuranSection = async () => {
  const result: any = await api
    .get('/quran/tracker')
    .then((r) => r.data)
    .catch((e) => console.log(e.data));
  if (result === null) {
    throw console.log('err');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};

export const updateQuran = async (juz: number, surah: number, ayah: number) => {
  const result: any = await api
    .post('/quran/update', { juz, surah, ayah })
    .then((r) => r.data)
    .catch((e) => console.log(e.data));
  if (result === null) {
    throw Alert.alert('Sorry could not save your change, please check your network connection');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};
export const updateTimeRead = async (value: number, date: any) => {
  const result: any = await api
    .post('/quran/readtime', { value, date })
    .then((r) => r.data)
    .catch((e) => console.log(e.data));
  if (result === null) {
    throw Alert.alert('Sorry could not save your change, please check your network connection');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};
