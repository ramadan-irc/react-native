import api from './api';
import { Alert } from 'react-native';
import Toast from 'react-native-root-toast';
import FontType from '@theme/fonts';
import MyToast from '@components/Toast/component';
export const postFeedback = async (version: string, body: string, value: any) => {
  const result: any = await api
    .post('/feedback/add', { version, body, value })
    .then((r) => r.data)
    .catch((e) => e.data);
  if (result === null) {
    throw MyToast('Sorry, network request failed')
    //throw Alert.alert('Sorry, network request failed');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};
