import api from './api';
// import R from 'reactotron-react-native';
import { Alert } from 'react-native';
import Toast from 'react-native-root-toast';
import FontType from '@theme/fonts';
import MyToast from '@components/Toast/component';
export const editProfile = async (
  username: any,
  email: string,
  location: any,
  age: any,
  gender: any,
) => {
  const result: any = await api
    .post('/user/profile', {
      username,
      email,
      location,
      age,
      gender,
    })
    .then((r) => r.data)
    .catch((e) => e.data);
  if (result === null) {
    throw MyToast('Sorry, network request failed')
    //throw Alert.alert('Sorry, network request failed');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};

export const getProfile = async () => {
  const result: any = await api.get('/user/profile').then((r) => r.data);
  if (result === null) {
    throw MyToast('Sorry, network request failed')
    //throw Alert.alert('Sorry, network request failed');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};
