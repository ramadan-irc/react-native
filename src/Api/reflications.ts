import api from './api';
import { Alert } from 'react-native';
import Toast from 'react-native-root-toast';
import FontType from '@theme/fonts';
import MyToast from '@components/Toast/component';

export const getReflictions = async () => {
  const result: any = await api
    .get('/reflection')
    .then((r) => r.data)
    .catch((e) => e.data.message);
  if (result === null) {
    throw MyToast('Sorry, network request failed')
    //throw Alert.alert('Sorry, network request failed');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};

export const addRefliction = async (text: string) => {
  const result: any = await api
    .post('/reflection/add', { text })
    .then((r) => r.data)
    .catch((e) => e.data.message);
  if (result === null) {
    throw MyToast('Sorry, network request failed')
    //throw Alert.alert('Sorry, network request failed');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};

export const editRefliction = async (id: any, text: string, date: any) => {
  const result: any = await api
    .post('/reflection/update', { id, text, date })
    .then((r) => r.data)
    .catch((e) => e.data.message);
  if (result === null) {
    throw MyToast('Sorry, network request failed')
    //throw Alert.alert('Sorry, network request failed');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};
export const deleteRefliction = async (id: any) => {
  const result: any = await api
    .get('/reflection/delete/' + id)
    .then((r) => r.data)
    .catch((e) => e.data.message);
  if (result === null) {
    throw MyToast('Sorry, network request failed')
    //throw Alert.alert('Sorry, network request failed');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};
export const Setrefliction = async (id: any) => {
  const result: any = await api
    .get('/reflection/' + id)
    .then((r) => r.data)
    .catch((e) => e.data.message);
  if (result === null) {
    throw MyToast('Sorry, network request failed')
    //throw Alert.alert('Sorry, network request failed');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};

export const getTilte = async (date: any) => {
  const result: any = await api
    .get('/title?date=' + date)
    .then((r) => r.data)
    .catch((e) => e.data.message);
  if (result === null) {
    throw console.log('err');
  } else if (result.message) {
    throw result.message;
  }
  return result;
};
