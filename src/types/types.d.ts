declare module 'url';
declare module 'react-native-fbsdk';
declare module 'react-native-svg-charts';
declare module 'native-dates';
declare module 'react-native-social-share';
declare module 'react-native-version-check';
// declare module 'react-native-push-notification';
declare module '*.svg' {
  import React from 'react';
  import {SvgProps} from 'react-native-svg';
  const content: React.FC<
    SvgProps & {
      fillSecondary?: string;
    }
  >;
  export default content;
}
declare module 'lodash.debounce';
declare module 'react-native-popup-dialog';
