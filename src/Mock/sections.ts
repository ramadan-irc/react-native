const sections = [
  {
    id: 1,
    name: 'Pray',
    image: 'https://www.ircanada.info/public/images/Pray.svg',
  },
  {
    id: 2,
    name: 'Quran',
    image: 'https://www.ircanada.info/public/images/NewQuran.svg',
  },
  {
    id: 3,
    name: 'CheckList',
    image: 'https://www.ircanada.info/public/images/check.svg',
  },
];
export default sections;
