const Prays = [
  {
    id: 1,
    name: 'FAJR',
    selected: false,
  },
  {
    id: 2,
    name: 'DHUHR',
    selected: false,
  },
  {
    id: 3,
    name: 'ASR',
    selected: false,
  },
  {
    id: 4,
    name: 'MAGHRIB',
    selected: false,
  },
  {
    id: 5,
    name: 'ISHA',
    selected: false,
  },
];
const Sunah = [
  {
    id: 6,
    name: 'SUNNAH',
    selected: false,
  },
  {
    id: 7,
    name: 'SUNNAH',
    selected: false,
  },
  {
    id: 8,
    name: 'SUNNAH',
    selected: false,
  },
  {
    id: 9,
    name: 'SUNNAH',
    selected: false,
  },
  {
    id: 10,
    name: 'SUNNAH',
    selected: false,
  },
];
const Duas = [
  {
    id: 6,
    nameAr:
      'أشْهَدُ أن لا إلهَ إلا الله نَسْتَغْفِرُ الله نسأَلُكَ الجنَّةَ ونَعُوذُ بِكَ مِنْ النَّار',
    nameenAr: 'dsdsdsdskdaldjskjdaskjsakhfhfhdajkfhsdasdasldkaskdjh',
    nameEn:
      'I testify that there is nothing worthy of worship other than Allah and we seek the forgiveness of Allah. We ask You for Paradise and take refuge in You from the Fire.',
  },
  {
    id: 7,
    nameAr:
      'أشْهَدُ أن لا إلهَ إلا الله نَسْتَغْفِرُ الله نسأَلُكَ الجنَّةَ ونَعُوذُ بِكَ مِنْ النَّار',
    nameenAr: 'dsdsdsdskdaldjskjdaskjsakhfhfhdajkfhsdasdasldkaskdjh',
    nameEn:
      'I testify that there is nothing worthy of worship other than Allah and we seek the forgiveness of Allah. We ask You for Paradise and take refuge in You from the Fire.',
  },
  {
    id: 8,
    nameAr:
      'أشْهَدُ أن لا إلهَ إلا الله نَسْتَغْفِرُ الله نسأَلُكَ الجنَّةَ ونَعُوذُ بِكَ مِنْ النَّار',
    nameenAr: 'dsdsdsdskdaldjskjdaskjsakhfhfhdajkfhsdasdasldkaskdjh',
    nameEn:
      'I testify that there is nothing worthy of worship other than Allah and we seek the forgiveness of Allah. We ask You for Paradise and take refuge in You from the Fire.',
  },
  {
    id: 9,
    nameAr:
      'أشْهَدُ أن لا إلهَ إلا الله نَسْتَغْفِرُ الله نسأَلُكَ الجنَّةَ ونَعُوذُ بِكَ مِنْ النَّار',
    nameenAr: 'dsdsdsdskdaldjskjdaskjsakhfhfhdajkfhsdasdasldkaskdjh',
    nameEn:
      'I testify that there is nothing worthy of worship other than Allah and we seek the forgiveness of Allah. We ask You for Paradise and take refuge in You from the Fire.',
  },
  {
    id: 10,
    nameAr:
      'أشْهَدُ أن لا إلهَ إلا الله نَسْتَغْفِرُ الله نسأَلُكَ الجنَّةَ ونَعُوذُ بِكَ مِنْ النَّار',
    nameenAr: 'dsdsdsdskdaldjskjdaskjsakhfhfhdajkfhsdasdasldkaskdjh',
    nameEn:
      'I testify that there is nothing worthy of worship other than Allah and we seek the forgiveness of Allah. We ask You for Paradise and take refuge in You from the Fire.',
  },
  {
    id: 11,
    nameAr:
      'أشْهَدُ أن لا إلهَ إلا الله نَسْتَغْفِرُ الله نسأَلُكَ الجنَّةَ ونَعُوذُ بِكَ مِنْ النَّار',
    nameenAr: 'dsdsdsdskdaldjskjdaskjsakhfhfhdajkfhsdasdasldkaskdjh',
    nameEn:
      'I testify that there is nothing worthy of worship other than Allah and we seek the forgiveness of Allah. We ask You for Paradise and take refuge in You from the Fire.',
  },
  {
    id: 12,
    nameAr:
      'أشْهَدُ أن لا إلهَ إلا الله نَسْتَغْفِرُ الله نسأَلُكَ الجنَّةَ ونَعُوذُ بِكَ مِنْ النَّار',
    nameenAr: 'dsdsdsdskdaldjskjdaskjsakhfhfhdajkfhsdasdasldkaskdjh',
    nameEn:
      'I testify that there is nothing worthy of worship other than Allah and we seek the forgiveness of Allah. We ask You for Paradise and take refuge in You from the Fire.',
  },
  {
    id: 13,
    nameAr:
      'أشْهَدُ أن لا إلهَ إلا الله نَسْتَغْفِرُ الله نسأَلُكَ الجنَّةَ ونَعُوذُ بِكَ مِنْ النَّار',
    nameenAr: 'dsdsdsdskdaldjskjdaskjsakhfhfhdajkfhsdasdasldkaskdjh',
    nameEn:
      'I testify that there is nothing worthy of worship other than Allah and we seek the forgiveness of Allah. We ask You for Paradise and take refuge in You from the Fire.',
  },
];
const DAYES = [
  {
    id: 6,
    name: 'MON',
  },
  {
    id: 7,
    name: 'TUE',
  },
  {
    id: 8,
    name: 'WED',
  },
  {
    id: 9,
    name: 'THU',
  },
  {
    id: 10,
    name: 'FRI',
  },
  {
    id: 11,
    name: '',
  },
];
const CheckList = [
  {
    id: 6,
    name: 'Smiled at someone',
    image: 'http://157.230.215.132/public/images/Smile.svg',
    selected: false,
  },
  {
    id: 7,
    name: 'Gave charity for someone',
    image: 'http://157.230.215.132/public/images/selctedCharity.svg',
    selected: false,
  },
  {
    id: 8,
    name: 'Learned something new',
    image: 'http://157.230.215.132/public/images/Learn.svg',
    selected: false,
  },
  {
    id: 9,
    name: 'Feed a hungry person',
    image: 'http://157.230.215.132/public/images/Fed.svg',
    selected: false,
  },
  {
    id: 10,
    name: 'Prayed in congregation',
    image: 'http://157.230.215.132/public/images/People.svg',
    selected: false,
  },
  {
    id: 11,
    name: 'Read my daily adhkaar',
    image: 'http://157.230.215.132/public/images/Adkhar.svg',
    selected: false,
  },
  {
    id: 12,
    name: 'Helped someone out',
    image: 'http://157.230.215.132/public/images/Help.svg',
    selected: false,
  },
  {
    id: 13,
    name: 'Asked for forgiveness',
    image: 'http://157.230.215.132/public/images/Ask.svg',
    selected: false,
  },
];
const Reflictions = [
  {
    id: 6,
    date: 'January 1, 2021',
    description:
      'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Dui molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.Lorem ipsum dolor sit a',
  },
  {
    id: 7,
    date: 'January 1, 2021',
    description:
      'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Dui molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.Lorem ipsum dolor sit a',
  },
  {
    id: 8,
    date: 'January 1, 2021',
    description:
      'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Dui molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.Lorem ipsum dolor sit a',
  },
  {
    id: 9,
    date: 'January 1, 2021',
    description:
      'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Dui molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.Lorem ipsum dolor sit a',
  },
  {
    id: 10,
    date: 'January 1, 2021',
    description:
      'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Dui molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.Lorem ipsum dolor sit a',
  },
  {
    id: 11,
    date: 'January 1, 2021',
    description:
      'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Dui molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.Lorem ipsum dolor sit a',
  },
  {
    id: 12,
    date: 'January 1, 2021',
    description:
      'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Dui molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.Lorem ipsum dolor sit a',
  },
  {
    id: 13,
    date: 'January 1, 2021',
    description:
      'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Dui molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.Lorem ipsum dolor sit a',
  },
];
const data = [
  {
    key: 1,
    amount: 50,
    svg: {fill: '#96A782'},
  },
  {
    key: 2,
    amount: 50,
    svg: {fill: '#75AECC'},
  },
  {
    key: 3,
    amount: 10,
    svg: {fill: '#DFBE84'},
  },
  {
    key: 4,
    amount: 10,
    svg: {fill: '#DFBE84'},
  },
  {
    key: 5,
    amount: 10,
    svg: {fill: '#DFBE84'},
  },
  {
    key: 6,
    amount: 10,
    svg: {fill: '#DFBE84'},
  },
  {
    key: 7,
    amount: 10,
    svg: {fill: '#DFBE84'},
  },
];
export default {Prays, Sunah, CheckList, DAYES, Duas, Reflictions, data};
