import homePage from './homePage';
import Pledge from './Pledge';
import Privacy from './privacy';
import loginPage from './loginPage';
import Landing from './Landing';
import signUp from './signup';
import settings from './settings';
import profile from './profile';
import Terms from './Terms';
export default {
  homePage,
  Pledge,
  Privacy,
  loginPage,
  Landing,
  signUp,
  settings,
  profile,
  Terms,
};
