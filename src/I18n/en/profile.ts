export default {
  myProfile: 'My Profile',
  Name: 'Name',
  Email: 'Email',
  pass: 'Password',
  loc: 'Location',
  Age: 'Age',
  Gender: 'Gender',
  Edit: 'EDIT',
  submit: 'SUBMIT',
  sett: 'SETTINGS',
};
