export default {
  first: 'In the name of Allah, the most Gracious, the most Merciful',
  second: 'In this app you can track',
  third: 'THE',
  R: 'Ramadan',
  sc: 'PLEDGE OF INTENTION',
  I: 'I,',
  fourth:
    'solemnly make this pledge to my Lord and Cherisher, Allah the Almighty, seeking His assistance and acceptance. Every single action that I do will depend on the quality of my intention. Therefore, I testify that I will always purify and renew my intention and seek for His Forgiveness solely to earn His pleasures.',
  five:
    'I promise I will not compare myself to anyone. I believe that everyone is at a different level and success is when I am better than I was yesterday. This Ramadan, I will bring myself to a better level by improving my character. I intend to always repent, forgive others, stop doing bad habits and replace it with good habits.',
  six:
    'I agree to keep track of my daily action, to practice self-control, and to motivate myself to do more everyday with the right intention. I agree to continue doing with what I have learned and practiced this month after Ramadan. May Allah grant me guidance and strength to make this Ramadan productive.',
  seven: 'May He make it easy for me to turn to Him completely and perpetually.',
  Ameen: 'Ameen.',
  CONTINUE: 'CONTINUE',
  goals: 'As well as tidbits of the day, reflections, gems, and goals!',
  Pr: 'PRAYERS',
  Qr: 'QURAN',
  gH: 'GOOD HABITS',
};
