export default {
  myProfile: 'My Profile',
  hgC: 'Hijri/Gregorian Calendar',
  dateAdj: 'Hijri Date Adjustment',
  push: 'Push Notification',
  Dmode: 'Dark Mode',
  plicy: 'Privacy Policy',
  terms: 'Terms & Conditions',
  feedback: 'SHARE FEEDBACK',
  out: 'SIGN OUT',
  lan: 'Language',
  dMode: 'Dark Mode',
  lMode: 'Light Mode',
  checkLogoutText: 'Are you sure?',
  cancel: 'Cancel'
};
