export default {
  Email: 'EMAIL',
  Password: 'PASSWORD',
  Login: 'LOGIN',
  Signinwith: 'Or sign in with',
  SigninwithGoogle: 'Sign in with Google',
  SigninwithFacebook: 'Sign in with Facebook',
  SigninwithApple: 'Sign in with Apple',
  Resetpassword: 'Reset Password',
  Signup: 'SIGN UP',
  or: 'or',
  recover: 'RECOVER PASSWORD',
};
