import React from 'react';
import { StyleSheet } from 'react-native';
import { MenuItem, OverflowMenu, Button } from '@ui-kitten/components';
import FontType from '@theme/fonts';
import { SetLang } from '@store/User/userAction';
import { useDispatch } from 'react-redux';
import { Text } from '@components';
import { Icon } from '@ui-kitten/components';
import { useTranslation } from 'react-i18next';
const LANGS = [
  { lngCode: 'en', label: 'English', value: 'English' },
  { lngCode: 'fr', label: 'Français', value: 'French' },
];
const LanguageSelector = () => {
  const { i18n } = useTranslation();
  const dispatch = useDispatch();
  const selectedLngCode = i18n.language;
  const [visible, setVisible] = React.useState(false);
  const [selectedTitle, setSelectedTitle] = React.useState(
    selectedLngCode === 'en' ? 'English' : 'Français',
  );
  const setLng = (lngCode: string) => i18n.changeLanguage(lngCode);

  const IconRnder = () => (
    <Icon style={{ width: 24, height: 24 }} fill={'#656B8D'} name="arrow-down" />
  );
  const renderToggleButton = () => (
    <Button
      accessoryRight={IconRnder}
      style={styles.container}
      children={() => (
        <Text size="xs" style={{ color: '#656B8D', fontFamily: FontType.Bold, paddingRight: 5 }}>
          {selectedTitle}
        </Text>
      )}
      onPress={() => setVisible(true)}
    />
  );
  return (
    <OverflowMenu
      visible={visible}
      anchor={renderToggleButton}
      placement="bottom end"
      style={{ width: 45 * 2, }}
      onBackdropPress={() => setVisible(false)}>
      {LANGS.map((l) => {
        return (
          <MenuItem
            title={l.label}
            key={l.lngCode}
            style={{ backgroundColor: '#F2F2F2' }}
            onPress={() => {
              setLng(l.lngCode);
              setSelectedTitle(l.label);
              setVisible(false);
              dispatch(SetLang(l.value));
            }}
          />
        );
      })}
    </OverflowMenu>
  );
};

export default LanguageSelector;

const styles = StyleSheet.create({
  container: {
    width: 40 * 2,
    height: 30,
    backgroundColor: '#F2F2F2',
    borderRadius: 20,
    borderWidth: 1,
    right: 20,
    borderColor: '#F2F2F2',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  titleContainer: {
    alignItems: 'center',
    paddingBottom: 20,
  },
  select: {
    fontSize: 20,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 5,
    paddingVertical: 5,
  },
  selectedRow: {
    borderRadius: 50,
    borderWidth: 2,
  },
  selectedText: {
    fontSize: 15,
  },
  text: {
    fontSize: 15,
  },
});
