export default {
  Name: 'VOTRE NOM',
  Email: 'COURRIEL',
  Password: 'MOT DE PASSE',
  conf: 'CONFIRMEZ LE MOT DE PASSE',
  Sign: 'INSCRIVEZ-VOUS',
  Login: 'CONNEXION',
  emailReq: "L'e-mail est obligatoire",
  emailVal: "L'email n'est pas valide",
};
