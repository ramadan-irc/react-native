export default {
  TermsC: 'Conditions d’utilisation',
  firstTT:
    'Dernière mise à jour : 5 mars 2021 Veuillez lire attentivement ces conditions d’utilisation (« termes », « conditions d’utilisation ») avant d’utiliser l’application mobile « Mes bonnes actions » (le « service ») opérée par Islamic Relief Canada (« nous » ou « notre »). Votre accès et utilisation du service est conditionnée par votre consentement et votre adhésion à ces termes. Ces termes s’appliquent à tous les visiteurs, utilisateurs et à tous ceux qui accèdent ou utilisent le service. En accédant ou en utilisant le service, vous acceptez d’être tenu par ces conditions d’utilisation. Si vous n’êtes pas en accord avec une partie des conditions, vous ne pouvez pas accéder au service. ',
  changesTerms: '- Changements :',
  twoTerms:
    'Nous nous réservons le droit à notre seule discrétion de modifier ou de remplacer ces termes à tout moment. Si une révision est importante, nous essaierons de fournir un préavis d’au moins 30 jours avant l’entrée en vigueur de toute nouvelle condition. Ce qui constitue un changement important sera déterminé à notre seule discrétion.',
  contactUsT: '- Nous contacter :',
  threeTerms:
    'Pour toute question concernant ces termes, veuillez nous contacter par courriel à app@islamicreliefcanada.org ou par téléphone au 1 (855) 377-4673.',
  settingsTerms: 'PARAMÈTRES',
};
