export default {
  Email: 'COURRIEL',
  Password: 'MOT DE PASSE',
  Login: 'CONNEXION',
  Signinwith: 'Ou connexion avec',
  SigninwithGoogle: 'Connexion avec Google',
  SigninwithFacebook: 'Connexion avec Facebook',
  SigninwithApple: 'Connexion avec Apple',
  Resetpassword: 'Réinitialiser le mot de passe',
  Signup: 'INSCRIVEZ VOUS',
  or: 'ou',
  recover: 'RÉINITIALISER',
};
