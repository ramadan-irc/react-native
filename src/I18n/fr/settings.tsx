export default {
  myProfile: 'Mon Profil',
  hgC: 'Calendrier Hijri/Grégorien',
  dateAdj: 'Ajustement de la date Hijri',
  push: 'Notifcation',
  Dmode: 'Mode Sombre',
  plicy: 'Politique de Confidentialité',
  terms: 'Termes et Conditions',
  feedback: 'PARTAGEZ VOS SUGGESTIONS',
  out: 'DÉCONNEXION',
  lan: 'Langue',
  dMode: 'Mode Sombre',
  lMode: 'Mode Lumière',
  checkLogoutText: 'Es-tu sûr?',
  cancel: 'Annuler'
};
