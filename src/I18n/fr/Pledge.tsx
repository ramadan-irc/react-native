export default {
  first: 'Au nom d’Allah, le Tout Miséricordieux, le Très Miséricordieux',
  second: 'Dans cette application, vous pouvez suivre',
  v: 'L’ENGAGEMENT D’INTENTION DU',
  R: 'Ramadan',
  I: 'Je,',
  fourth:
    'prends solennellement cet engagement envers mon Seigneur et Adoré, Allah le Très Haut, implorant son aide et son acceptation. Chacune des actions que j’accomplirai dépendra de la qualité de mon intention. Par conséquent, j’atteste que je purifierai et renouvellerai constamment mon intention et implorerai Sa miséricorde dans l’unique but de Lui plaire.',
  five:
    'Je promets de ne me comparer à personne. Je crois que chacun est à son propre niveau et que mon succès est plutôt atteint si je suis meilleur qu’hier. Ce Ramadan, j’atteindrai un niveau supérieur en améliorant mon caractère. J’ai l’intention de toujours me repentir, de pardonner aux autres et d’abandonner mes mauvaises habitudes pour les remplacer par de bonnes habitudes.',
  six:
    'Je conviens de prendre note de mes actions quotidiennes, de me maîtriser et de me motiver à faire plus chaque jour avec la bonne intention. Je conviens de continuer, après le Ramadan, de faire ce que j’ai appris et ce à quoi je me suis exercé durant ce mois. Puisse Allah m’accorder la droiture et la force nécessaires pour un Ramadan productif.',
  seven: 'Puisse-t-Il me faciliter la tâche de me tourner complètement et à perpétuité vers Lui.',
  Ameen: 'Amine.',
  CONTINUE: 'CONTINUEZ',
  goals: 'De même que des conseils du jour, des réflexions, des citations et des objectifs!',
  Pr: 'PRIÈRES',
  Qr: 'CORAN',
  gH: 'BONNES HABITUDES',
};
