export default {
  myProfile: 'Mon Profil',
  Name: 'Votre Nom',
  Email: 'Courriel',
  pass: 'Mot De Passe',
  loc: 'Lieu',
  Age: 'Âge',
  Gender: 'Le sexe',
  Edit: 'MODIFIER',
  submit: 'SOUMETTRE',
  sett: 'PARAMÈTRES',
};
