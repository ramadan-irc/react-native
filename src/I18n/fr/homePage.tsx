export default {
  welcome: 'Bienvenue Comment vas tu',
  setting: 'réglage',
  Tidbits: 'Conseils',
  Favourites: 'Favoris',
  myref: 'Mes réflexions',
  Newref: 'NOUVELLE RÉFLEXION',
  sub: 'SOUMETTRE',
  share: 'PARTAGER',
  sharefeed: 'PartagerRetour',
  TReflection: 'Réflexion d’aujourd’hui',
  tap: 'Appuyez pour commencer à écrire',
  tRead: 'TEMPS DE LECTURE',
  bMarked: 'Marque-Page',
  TminQuran: 'MINUTES TOTAL DE LECTURE DU CORAN',
  sPrayed: 'SUNNAS PRIE',
  gdeedsf: 'BONNES ACTIONS',
  gdeedss: 'ACCOMPLIES',
  gdeedst: 'CE MOIS',
  NPrayed: 'NAFL PRIE',
  Mprogrees: 'Progression Mensuelle',
  title:
    'Un espace pour noter les réflexions, les leçons, les moments de gratitude et les prières.',
  titleFB:
    "Laissez-nous savoir comment vous aimer l'application et si vous avez des soucis, des questions ou des commentaires.  Cliquez sur Soumettre et quelqu'un de l'équipe d’Islamic Relief vous contactera!",
  Donate: 'FAIRE UN DON',
  duaa: 'DUAS',
  daily: 'Quotidiens',
  Day: 'Jour',
  nFD: "Vous n'avez pas encore ajouté de duaa à votre liste de favoris",
  nFT: "Vous n'avez pas encore ajouté de friandises à votre liste de favoris",
  Nodeed: "pas d'action pour la journée",
  DeleteSure: 'Voulez-vous vraiment supprimer ce reflet?',
  YES: 'Oui',
  NO: 'Non',
};
