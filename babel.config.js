module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      require.resolve('babel-plugin-module-resolver'),
      {
        cwd: 'babelrc',
        extensions: ['.ts', '.tsx', '.js', '.ios.js', '.android.js'],
        alias: {
          '@Src': './src',
          '@theme': ['./src/Views/Theme'],
          '@screens': ['./src/Views/Screens'],
          '@constants': ['./src/Constants'],
          '@mock': ['./src/Mock'],
          '@components': ['./src/Views/Components'],
          '@store': './src/Store',
          '@api': './src/Api',
          '@config': './src/Config',
          '@assets': ['./src/Assets'],
          '@i18n': ['./src/I18n'],
          '@navigatiors': ['./src/Views/Navigation'],
        },
      },
    ],
    'jest-hoist',
  ],
};
