import 'react-native-gesture-handler';
import React from 'react';
import R from 'reactotron-react-native';
import VersionCheck from 'react-native-version-check';
import { StatusBar, Alert, Linking } from 'react-native';
import { setRegisterToken } from '@store/User/userAction';
import messaging from '@react-native-firebase/messaging';
import { useDispatch } from 'react-redux';
import { ThemeProvider } from './src/Views/Theme/ThemeProvider';
import { RootSiblingParent } from 'react-native-root-siblings';
import AppNavigator from './src/Views/Navigation';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { enableScreens } from 'react-native-screens';
import SplashScreen from 'react-native-splash-screen';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import { ApplicationProvider, IconRegistry } from '@ui-kitten/components';
import { store, persistor } from './src/Store/index';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { useTheme } from '@theme/ThemeProvider';
import { SafeAreaProvider, initialWindowMetrics } from 'react-native-safe-area-context';
import { mapping, dark, light } from '@eva-design/eva';
import { BackHandler } from 'react-native';

enableScreens();
const App = () => {
  SplashScreen.hide();

  const { isDarkState, colors } = useTheme();
  const dispatch = useDispatch();
  messaging().setBackgroundMessageHandler(async (remoteMessage) => {
    console.log('Message handled in the background!', remoteMessage);
  });
  React.useEffect(() => {
    checkVersion();
    getFcmToken();
    const unsubscribe = messaging().onMessage(async (remoteMessage) => {
      R.log('remote dfd', remoteMessage.notification.body);
      Alert.alert(
        remoteMessage.notification.title,
        remoteMessage.notification.body,
        [{ text: 'Ok', onPress: () => console.log('ok pressed') }],
        { cancelable: false },
      );
    });

    return unsubscribe;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const checkVersion = async () => {
    try {
      let updateNeeded = await VersionCheck.needUpdate();
      if (updateNeeded && updateNeeded.isNeeded) {
        Alert.alert(
          'please Update',
          'You will have to update your app to the latest version to continue using.',
          [
            {
              text: 'Update',
              onPress: () => {
                BackHandler.exitApp();
                Linking.openURL(updateNeeded.storeUrl);
              },
            },
          ],
          { cancelable: false },
        );
      } else {
        console.log('YPU WIM');
      }
    } catch (error) {
      console.log('error', error);
    }
  };
  const _setFcmToken = async (key: any) => {
    try {
      await AsyncStorage.setItem('@fcmToken', key);
    } catch (error) {
      R.log(error);
    }
  };
  const getFcmToken = async () => {
    let fcmToken = await AsyncStorage.getItem('@fcmToken');
    if (!fcmToken) {
      try {
        const fcmTokenss = await messaging().getToken();
        dispatch(setRegisterToken(fcmTokenss));
        _setFcmToken(fcmTokenss);
        R.log('hay token', fcmTokenss);
      } catch (e) {
        R.logImportant('firebase error', e.message);
      }
    } else {
      R.log('Your Firebase Token is:', fcmToken);
      await AsyncStorage.setItem('@fcmToken', fcmToken);
    }
  };
  // PushNotification.configure({
  //   onRegister: function (token: any) {
  //     console.log('TOKEN:', token);
  //   },
  //   onNotification: function (notification: any) {
  //     console.log('NOTIFICATION:', notification);
  //     // notification.finish(PushNotificationIOS.FetchResult.NoData);
  //   },
  //   onRegistrationError: function (err: any) {
  //     console.error(err.message, err);
  //   },
  //   permissions: {
  //     alert: true,
  //     badge: true,
  //     sound: true,
  //   },
  //   popInitialNotification: true,
  //   requestPermissions: true,
  // });
  // const testSchdule = () => {
  //   PushNotification.localNotificationSchedule({
  //     message: 'My Notification hello',
  //     date: moment('2021-04-05').set('hour', 20).set('minute', 42).toDate(),
  //   });
  // };
  // const testSchdule1 = () => {
  //   PushNotification.localNotificationSchedule({
  //     message: 'My Notification ee',
  //     date: moment().day('Sunday').toDate(),
  //   });
  // };
  return (
    <React.Fragment>
      <SafeAreaProvider initialMetrics={initialWindowMetrics}>
        <ThemeProvider>
          <StatusBar
            barStyle={isDarkState ? 'light-content' : 'dark-content'}
            backgroundColor={isDarkState ? '#4B535B' : '#656B8D'}//{colors.statusBar}
            animated={true}
          />
          <IconRegistry icons={EvaIconsPack} />
          <ApplicationProvider mapping={mapping} theme={isDarkState ? dark : light}>
            <RootSiblingParent>
              <AppNavigator />
            </RootSiblingParent>
          </ApplicationProvider>
        </ThemeProvider>
      </SafeAreaProvider>
    </React.Fragment>
  );
};
export default () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <App />
      </PersistGate>
    </Provider>
  );
};
